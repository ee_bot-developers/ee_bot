# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from random import randint
import numpy as np
from quizUtils import Units
from typing import Union, Tuple, Optional, Sequence
import inspect

__very_easy = {}


def load_very_easy():
    # Open text file holding questions and filenames
    with open("veryEasyQuestions.txt", 'r', encoding='utf-8') as f:
        # Read file in with each line as an item
        lines = f.read().splitlines()
    # iterate through list and format each line
    for line in lines:
        Line = line.split(';')
        __very_easy[Line[0]] = {'question': Line[1], 'file': None if not Line[2] else f'Veasy/{Line[2]}'}


def freeVeryEasy():
    # Clear global list
    __very_easy.clear()


# Variants of question 1
def _Q1_1(R, V):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R, V)
    ans = V / R
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_2(R1, R2, R3, V):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, V)
    R = (R1 * R2) / (R1 + R2)
    R = (R * R3) / (R + R3)
    ans = V / R
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_3(R, I):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R, I)
    I = I / 1000
    ans = I * R
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_4(R1, R2, R3, I):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, I)
    R = (R1 * R2) / (R1 + R2)
    R = (R * R3) / (R + R3)
    I = I / 1000
    ans = R * I
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_5(V, I):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, I)
    I = I / 1000
    ans = V / I
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_6(V, I, R2, R3):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, I, R2, R3)
    tmp1 = (V / R2) * 1000
    tmp2 = (V / R3) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V / I
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_7(V, I, R1, R3):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, I, R1, R3)
    tmp1 = (V / R1) * 1000
    tmp2 = (V / R3) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V / I
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q1_8(V, I, R1, R2):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, I, R1, R2)
    tmp1 = (V / R1) * 1000
    tmp2 = (V / R2) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V / I
    margin = 0.01
    return Question, qDict['file'], ans, margin


# Variants of question 2
def _Q2_1(V, R1, R2, R3, R4, R5, version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:-1]}{version}']
    Question = qDict['question'] % (V, R1, R2, R3, R4, R5)
    X = np.array([[((1 / R1) + (1 / R3) + (1 / R5)), -1 / R3], [-1 / R3, ((1 / R2) + (1 / R3) + (1 / R4))]])
    Y = np.array([V / R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V - nodeVoltage[0])
    elif version == 2:
        ans = abs(nodeVoltage[1])
    elif version == 3:
        ans = abs(nodeVoltage[0] - nodeVoltage[1])
    elif version == 4:
        ans = abs(nodeVoltage[1])
    elif version == 5:
        ans = abs(nodeVoltage[0])
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


def _Q2_2(V, R1, R2, R3, R4, R5, R6, version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:-1]}{version}']
    Question = qDict['question'] % (V, R1, R2, R3, R4, R5, R6)
    X = np.array(
        [[((1 / R1) + (1 / R2) + (1 / R5)), -1 / R2, -1 / R5], [-1 / R2, ((1 / R2) + (1 / R3) + (1 / R4)), -1 / R4],
         [-1 / R5, -1 / R4, ((1 / R4) + (1 / R5) + (1 / R6))]])
    Y = np.array([V / R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V - nodeVoltage[0])
    elif version == 2:
        ans = abs(nodeVoltage[0] - nodeVoltage[1])
    elif version == 3:
        ans = abs(nodeVoltage[1])
    elif version == 4:
        ans = abs(nodeVoltage[1] - nodeVoltage[2])
    elif version == 5:
        ans = abs(nodeVoltage[0] - nodeVoltage[2])
    elif version == 6:
        ans = abs(nodeVoltage[2])
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


def _Q2_3(V1, V2, R1, R2, R3, R4, R5, R6, version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:-1]}{version}']
    Question = qDict['question'] % (V1, V2, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1 / R1) + (1 / R2) + (1 / R4) + (1 / R5)), ((-1 / R2) + (-1 / R4))],
                  [((-1 / R2) + (-1 / R4)), ((1 / R2) + (1 / R3) + (1 / R4))]])
    Y = np.array([((V1 / R1) + (V2 / R5)), 0])
    nodeVoltages = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V1 - nodeVoltages[0])
    elif version == 2:
        ans = abs(nodeVoltages[0] - nodeVoltages[1])
    elif version == 3:
        ans = abs(nodeVoltages[1])
    elif version == 4:
        ans = abs(nodeVoltages[0] - nodeVoltages[1])
    elif version == 5:
        ans = abs(V2 - nodeVoltages[0])
    elif version == 6:
        ans = abs(V2)
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


# Variants of question 3
def _Q3_1(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    tmp1 = R8 + R9 + R10
    tmp2 = (R7 * tmp1) / (R7 + tmp1)
    tmp1 = (R6 * tmp2) / (R6 + tmp2)
    tmp2 = R4 + R5 + tmp1
    tmp1 = (R3 * tmp2) / (R3 + tmp2)
    ans = R1 + R2 + tmp1
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q3_2(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    tmp1 = R9 + R10
    tmp1 = (tmp1 * R8) / (tmp1 + R8)
    tmp1 += R6 + R7
    tmp2 = R1 + R2 + R3
    tmp2 = (tmp2 * R4) / (tmp2 + R4)
    tmp2 += R5
    ans = (tmp1 * tmp2) / (tmp1 + tmp2)
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q3_3(R1, R2, R3, R4, R5, R6, R7):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, R4, R5, R6, R7)
    Ra = (R4 * R5) / (R4 + R5 + R7)
    Rb = (R5 * R7) / (R4 + R5 + R7)
    Rc = (R4 * R7) / (R4 + R5 + R7)
    tmp = ((R3 + Ra) * (R6 + Rb)) / (R3 + Ra + R6 + Rb)
    ans = R1 + tmp + Rc + R2
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q3_4(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14)
    Ra = (R4 * R5) / (R4 + R5 + R7)
    Rb = (R5 * R7) / (R4 + R5 + R7)
    Rc = (R4 * R7) / (R4 + R5 + R7)
    tmp = ((R3 + Ra) * (R6 + Rb)) / (R3 + Ra + R6 + Rb)
    tmp = ((tmp + Rc) * (R8 + R9)) / (tmp + R8 + R9 + Rc)
    ans = R1 + tmp + R2
    margin = 0.01
    return Question, qDict['file'], ans, margin


# question 4 private function
def _Q4(version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:]}_{version}']
    Question = qDict['question']
    ans = None
    if version == 1:
        ans = 'NAND'
    elif version == 2:
        ans = 'AND'
    elif version == 3:
        ans = 'NOR'
    elif version == 4:
        ans = 'OR'
    elif version == 5:
        ans = 'XOR'
    elif version == 6:
        ans = 'XNOR'
    margin = 0
    return Question, qDict['file'], ans, margin


# variants of question 5
def _Q5_1(I, R1, R2):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (I, R1, R2)
    I /= 1000
    ans = (R1 + R2) * I
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q5_2(I, R1, R2):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (I, R1, R2)
    Req = (R1 * R2) / (R1 + R2)
    I /= 1000
    ans = Req * I
    margin = 0.1
    return Question, qDict['file'], ans, margin


# variants of question 6
def _Q6_1(V, R1, R2, R3, R4, R5):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, R1, R2, R3, R4, R5)
    X = np.array([[((1 / R1) + (1 / R3) + (1 / R5)), -1 / R3], [-1 / R3, ((1 / R2) + (1 / R3) + (1 / R4))]])
    Y = np.array([V / R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V - nodeVoltage[0]), 2) / R1
    ans += pow((nodeVoltage[0] - nodeVoltage[1]), 2) / R3
    ans += pow(nodeVoltage[0], 2) / R5
    ans += pow(nodeVoltage[1], 2) / R2
    ans += pow(nodeVoltage[1], 2) / R4
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


def _Q6_2(V, R1, R2, R3, R4, R5, R6):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V, R1, R2, R3, R4, R5, R6)
    X = np.array(
        [[((1 / R1) + (1 / R2) + (1 / R5)), -1 / R2, -1 / R5], [-1 / R2, ((1 / R2) + (1 / R3) + (1 / R4)), -1 / R4],
         [-1 / R5, -1 / R4, ((1 / R4) + (1 / R5) + (1 / R6))]])
    Y = np.array([V / R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V - nodeVoltage[0]), 2) / R1
    ans += pow((nodeVoltage[0] - nodeVoltage[1]), 2) / R2
    ans += pow((nodeVoltage[0] - nodeVoltage[2]), 2) / R5
    ans += pow(nodeVoltage[1], 2) / R3
    ans += pow(nodeVoltage[2], 2) / R6
    ans += pow((nodeVoltage[1] - nodeVoltage[2]), 2) / R4
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


def _Q6_3(V1, V2, R1, R2, R3, R4, R5, R6):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (V1, V2, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1 / R1) + (1 / R2) + (1 / R4) + (1 / R5)), ((-1 / R2) + (-1 / R4))],
                  [((-1 / R2) + (-1 / R4)), ((1 / R2) + (1 / R3) + (1 / R4))]])
    Y = np.array([((V1 / R1) + (V2 / R5)), 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V1 - nodeVoltage[0]), 2) / R1
    ans += pow((nodeVoltage[0] - nodeVoltage[1]), 2) / R2
    ans += pow(nodeVoltage[1], 2) / R3
    ans += pow((nodeVoltage[0] - nodeVoltage[1]), 2) / R4
    ans += pow((nodeVoltage[0] - V2), 2) / R5
    ans += pow(V2, 2) / R6
    pV = -1 * ((V1 - nodeVoltage[0]) / R1) * V1
    if pV > 0:
        ans += pV
    pV = -1 * (((V2 - nodeVoltage[0]) / R5) + (V2 / R6)) * V2
    if pV > 0:
        ans += pV
    margin = 0.01
    ans = float(ans)
    return Question, qDict['file'], ans, margin


# private function for question 7
def _Q7(A, version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:]}_{version}']
    Question = qDict['question'] % A
    ans = A
    margin = 0
    return Question, qDict['file'], ans, margin


# private function for question 9
def _Q9(A, version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:]}_{version}']
    Question = qDict['question'] % (A, A)
    if version == 1:
        ans = ['cos(%dt)' % A, 'cos%dt' % A, 'cos[%dt]' % A, 'cos{%dt}' % A]
    else:
        ans = ['cos(%dn)' % A, 'cos%dn' % A, 'cos[%dn]' % A, 'cos{%dn}' % A]
    margin = 0
    return Question, qDict['file'], ans, margin


# private function for question 10
def _Q10(version):
    qDict = __very_easy[f'{inspect.stack()[0].function[1:]}_{version}']
    Question = qDict['question']
    if version == 1:
        ans = ['ohm', 'ohms', 'Ω']
    elif version == 2:
        ans = ['farad', 'farads', 'F']
    elif version == 3:
        ans = ['henry', 'henries', 'henrys', 'H']
    else:
        raise ValueError('Invalid version for very easy question 10')
    margin = 0
    return Question, qDict['file'], ans, margin


# variants of question 11
def _Q11_1(C1, C2, C3, C4, C5, C6):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (C1, C2, C3, C4, C5, C6)
    tmp = C3 + C5
    tmp = (tmp * C4) / (tmp + C4)
    tmp += C6
    ans = pow((1 / C1) + (1 / tmp) + (1 / C2), -1)
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q11_2(C1, C2, C3, C4, C5, C6, C7, C8, C9):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (C1, C2, C3, C4, C5, C6, C7, C8, C9)
    tmp = (C8 * C9) / (C8 + C9)
    tmp += C7
    tmp += ((C3 + C4 + C6) * C5) / (C3 + C4 + C5 + C6)
    ans = pow((1 / C1) + (1 / tmp) + (1 / C2), -1)
    margin = 0.01
    return Question, qDict['file'], ans, margin


# variants of question 12
def _Q12_1(L1, L2, L3, L4, L5, L6, L7):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (L1, L2, L3, L4, L5, L6, L7)
    tmp = (L6 * L7) / (L6 + L7)
    tmp += L4 + L5
    tmp = (L3 * tmp) / (L3 + tmp)
    tmp = (L2 * tmp) / (L2 + tmp)
    ans = tmp + L1
    margin = 0.01
    return Question, qDict['file'], ans, margin


def _Q12_2(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10):
    qDict = __very_easy[inspect.stack()[0].function[1:]]
    Question = qDict['question'] % (L1, L2, L3, L4, L5, L6, L7, L8, L9, L10)
    tmp = L9 + L10
    tmp = (tmp * L8) / (tmp + L8)
    tmp = (tmp * L7) / (tmp + L7)
    tmp += L4 + L5 + L6
    tmp = (tmp * L3) / (tmp + L3)
    ans = L1 + L2 + tmp
    margin = 0.01
    return Question, qDict['file'], ans, margin


# Public function for question 1
def Q1() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 1
    version = randint(1, 8)
    # Generate question
    if version == 1:
        V = randint(1, 25)
        R = randint(1, 1000)
        Question, file, ans, margin = _Q1_1(R, V)
        units = Units.AMPS
    elif version == 2:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        V = randint(1, 25)
        Question, file, ans, margin = _Q1_2(R1, R2, R3, V)
        units = Units.AMPS
    elif version == 3:
        I = randint(1, 5000)
        R = randint(1, 1000)
        Question, file, ans, margin = _Q1_3(R, I)
        units = Units.VOLTS
    elif version == 4:
        I = randint(1, 5000)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _Q1_4(R1, R2, R3, I)
        units = Units.VOLTS
    elif version == 5:
        I = randint(1, 5000)
        V = randint(1, 25)
        Question, file, ans, margin = _Q1_5(V, I)
        units = Units.OHMS
    elif version == 6:
        I = randint(1, 5000)
        V = randint(1, 25)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _Q1_6(V, I, R2, R3)
        units = Units.OHMS
    elif version == 7:
        I = randint(1, 5000)
        V = randint(1, 25)
        R1 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _Q1_7(V, I, R1, R3)
        units = Units.OHMS
    elif version == 8:
        I = randint(1, 5000)
        V = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        Question, file, ans, margin = _Q1_8(V, I, R1, R2)
        units = Units.OHMS
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')

    # Return question data
    return Question, file, ans, margin, units


# Public function for question 2
def Q2() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 2
    version = randint(1, 17)
    # Generate question
    if version in range(1, 6):
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        Question, file, ans, margin = _Q2_1(V, R1, R2, R3, R4, R5, version)
    elif version in range(6, 12):
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        Question, file, ans, margin = _Q2_2(V, R1, R2, R3, R4, R5, R6, version)
    elif version in range(12, 18):
        V1 = randint(-25, 25)
        if V1 == 0:
            V1 += 1
        V2 = randint(-25, 25)
        if V2 == 0:
            V2 -= 1
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        Question, file, ans, margin = _Q2_3(V1, V2, R1, R2, R3, R4, R5, R6, version)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')

    # Return question data
    return Question, file, ans, margin, Units.VOLTS


# Public function for question 3
def Q3() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 3
    version = randint(1, 4)
    # Generate question
    if version == 1:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _Q3_1(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    elif version == 2:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _Q3_2(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    elif version == 3:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        question, file, ans, margin = _Q3_3(R1, R2, R3, R4, R5, R6, R7)
    elif version == 4:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        R11 = randint(1, 1000)
        R12 = randint(1, 1000)
        R13 = randint(1, 1000)
        R14 = randint(1, 1000)
        question, file, ans, margin = _Q3_4(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')
    # Return question data
    return question, file, ans, margin, Units.OHMS


# Public function for question 4
def Q4() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    version = randint(1, 6)
    question, file, ans, margin = _Q4(version)
    return question, file, ans, margin, Units.NO_UNITS


# Public function for question 5
def Q5() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 5
    version = randint(1, 2)
    # Generate question
    I = randint(1, 1000)
    R1 = randint(1, 10000)
    R2 = randint(1, 10000)
    if version == 1:
        question, file, ans, margin = _Q5_1(I, R1, R2)
    elif version == 2:
        question, file, ans, margin = _Q5_2(I, R1, R2)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')
    # Return question data
    return question, file, ans, margin, Units.VOLTS


# Public function for question 6
def Q6() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 6
    version = randint(1, 3)
    # Generate question
    if version == 1:
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        question, file, ans, margin = _Q6_1(V, R1, R2, R3, R4, R5)
    elif version == 2:
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        question, file, ans, margin = _Q6_2(V, R1, R2, R3, R4, R5, R6)
    elif version == 3:
        V1 = randint(1, 25)
        V2 = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        question, file, ans, margin = _Q6_3(V1, V2, R1, R2, R3, R4, R5, R6)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')
    # Return question data
    return question, file, ans, margin, Units.WATTS


# Public function for question 7
def Q7() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 6
    version = randint(1, 2)
    # Generate question
    A = randint(-100000000000, 100000000000)
    if A == 0:
        A = 1
    question, file, ans, margin = _Q7(A, version)
    # Return question data
    return question, file, ans, margin, Units.NO_UNITS


def Q8() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    qData = __very_easy[f'{inspect.stack()[0].function}_1']
    # retrieve question
    Question = qData['question']
    # calculate answer
    ans = 16
    # calculate margin
    margin = 0
    # return question, file, answer, margin, and units
    return Question, qData['file'], ans, margin, Units.NO_UNITS


# Public function for question 9
def Q9() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 9
    version = randint(1, 2)
    # Generate question
    A = randint(-100000, 100000)
    if A == 0:
        A = 1
    question, file, ans, margin = _Q9(A, version)
    # Return question data
    return question, file, ans, margin, Units.NO_UNITS


# Public function for question 10
def Q10() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 10
    version = randint(1, 3)
    # Generate question
    question, file, ans, margin = _Q10(version)
    # Return question data
    return question, file, ans, margin, Units.NO_UNITS


# Public function for question 11
def Q11() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 11
    version = randint(1, 2)
    # Generate question
    if version == 1:
        C1 = float(randint(1, 100)) / 10
        C2 = float(randint(1, 100)) / 10
        C3 = float(randint(1, 100)) / 10
        C4 = float(randint(1, 100)) / 10
        C5 = float(randint(1, 100)) / 10
        C6 = float(randint(1, 100)) / 10
        question, file, ans, margin = _Q11_1(C1, C2, C3, C4, C5, C6)
    elif version == 2:
        C1 = float(randint(1, 100)) / 10
        C2 = float(randint(1, 100)) / 10
        C3 = float(randint(1, 100)) / 10
        C4 = float(randint(1, 100)) / 10
        C5 = float(randint(1, 100)) / 10
        C6 = float(randint(1, 100)) / 10
        C7 = float(randint(1, 100)) / 10
        C8 = float(randint(1, 100)) / 10
        C9 = float(randint(1, 100)) / 10
        question, file, ans, margin = _Q11_2(C1, C2, C3, C4, C5, C6, C7, C8, C9)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')
    # Return question data
    return question, file, ans, margin, Units.FARADS


# Public function for question 12
def Q12() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    # randomly choose variant of question 12
    version = randint(1, 2)
    # Generate question
    if version == 1:
        L1 = float(randint(1, 10000)) / 1000
        L2 = float(randint(1, 10000)) / 1000
        L3 = float(randint(1, 10000)) / 1000
        L4 = float(randint(1, 10000)) / 1000
        L5 = float(randint(1, 10000)) / 1000
        L6 = float(randint(1, 10000)) / 1000
        L7 = float(randint(1, 10000)) / 1000
        question, file, ans, margin = _Q12_1(L1, L2, L3, L4, L5, L6, L7)
    elif version == 2:
        L1 = float(randint(1, 10000)) / 1000
        L2 = float(randint(1, 10000)) / 1000
        L3 = float(randint(1, 10000)) / 1000
        L4 = float(randint(1, 10000)) / 1000
        L5 = float(randint(1, 10000)) / 1000
        L6 = float(randint(1, 10000)) / 1000
        L7 = float(randint(1, 10000)) / 1000
        L8 = float(randint(1, 10000)) / 1000
        L9 = float(randint(1, 10000)) / 1000
        L10 = float(randint(1, 10000)) / 1000
        question, file, ans, margin = _Q12_2(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10)
    else:
        raise RuntimeError(f'Very Easy {__name__} invalid question version: {version}')
    # Return question data
    return question, file, ans, margin, Units.HENRYS


# Question 13 function
def Q13() -> Tuple[str, Optional[str], Union[complex, str, int, float, Sequence[str]], float, Units]:
    qDict = __very_easy[f'{inspect.stack()[0].function}_1']
    # Retrieve question
    Question = qDict['question']
    # Calculate answer
    ans = 2000
    # calculate margin
    margin = 0
    # return question, file, answer, answer margin, and units
    return Question, qDict['file'], ans, margin, Units.HERTZ


Questions = {
    1: Q1,
    2: Q2,
    3: Q3,
    4: Q4,
    5: Q5,
    6: Q6,
    7: Q7,
    8: Q8,
    9: Q9,
    10: Q10,
    11: Q11,
    12: Q12,
    13: Q13
}
