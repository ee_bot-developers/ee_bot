# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from subprocess import Popen, PIPE
import datetime
import tempfile
import os
from typing import Sequence, Optional, Tuple
import io


class Color(object):
    """
    Data class that represents a color.
    :param[R]  The red component of the color (0-255)
    :param[G]  The green component of the color (0-255)
    :param[B]  The blue component of the color (0-255)
    """
    def __init__(self, R: int, G: int, B: int):
        self._red = R & 0xFF
        self._green = G & 0xFF
        self._blue = B & 0xFF

    # Normalizes the color so each entry is between 0.0 and 1.0
    @property
    def rgb(self) -> Tuple[float, float, float]:
        return self._red / 0xFF, self._green / 0xFF, self._blue / 0xFF

    # Returns the RGB color
    @property
    def RGB(self) -> Tuple[int, int, int]:
        return self._red, self._green, self._blue


def _create_tex(formula: str, packages: Sequence[str]) -> str:
    """
    Adds the formula to a complete latex document.

    :param formula: The latex code to be rendered
    :param packages: The extra packages needed and any other information that needs to go in the preamble
    :return: The complete latex document contents
    """
    tex_elements = [r'\documentclass[preview,36pt]{standalone}', r'\usepackage{amsmath}', r'\usepackage{xcolor}']
    if packages:
        tex_elements += [f'\\usepackage{{{package}}}' for package in packages]
    tex_elements += [r'\definecolor{discord-grey}{RGB}{51,54,61}', r'\definecolor{discord-white}{RGB}{255,255,255}',
                     r'\begin{document}']
    tex_elements += [f'\\colorbox{{discord-grey}}{{\\textcolor{{discord-white}}{{{formula}}}}}']
    tex_elements += [r'\end{document}']
    return '\n'.join(tex_elements)


def _render_image(filename: str, tex: str, tex_fn: str, background: Color, dpi: int,
                  verbose: bool) -> tempfile.TemporaryDirectory:
    """
    Compiles the latex code and renders the image in a temporary directory.

    :param filename: The unique filename generated for the job
    :param tex: The tex code that was generated.
    :param tex_fn: The tex filename to be written to
    :param background: The background color of the image to be generated
    :param dpi: The requested resolution of the image
    :param verbose: flag to print the compilation process. Useful for debugging
    :return: The temporary directory where the image was supposed to be generated in
    """
    tmp = tempfile.TemporaryDirectory(dir=os.getcwd())
    cwd = os.getcwd()
    os.chdir(tmp.name)
    with open(tex_fn, 'w') as tex_file:
        tex_file.write(tex)

    process = Popen(f'latex -interaction=batchmode {filename}.tex', shell=True, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()
    process.wait()

    if verbose:
        print(out)
        print('ERROR: ')
        print(err)

    process = Popen(f'dvipng -T tight -D {dpi} {filename}.dvi', shell=True,
                    stderr=PIPE, stdout=PIPE)
    out, err = process.communicate()
    process.wait()

    if verbose:
        print(out)
        print('ERROR: ')
        print(err)

    os.chdir(cwd)
    return tmp


def _read_image(directory) -> Optional[io.BytesIO]:
    """
    Reads the image and puts it into a bytes buffer

    :param directory: The directory where the file is located in
    :return: image stored in BytesIO object upon success, None upon failure
    """
    file_list = os.listdir(directory)
    for filename in file_list:
        file_path = os.path.join(directory, filename)
        if file_path.endswith('png'):
            if os.path.isfile(file_path):
                with open(file_path, 'rb') as f:
                    buf = io.BytesIO(f.read())
                    return buf
    return None


def render_latex(formula: str, dpi: int = 600, packages: Optional[Sequence[str]] = None,
                 verbose: bool = False) -> Tuple[Optional[io.BytesIO], str]:
    """
    Renders the latex in a png image

    :param formula: The latex string to be rendered
    :param dpi: The requested resolution. Default: 600
    :param packages: Additional packages needed for rendering. Default: None
    :param verbose: Print compilation output and conversion output. Default: False
    :return: Tuple of the image stream and the generated file name upon success. None and an empty string upon failure
    """
    unique_name = str(datetime.datetime.now().timestamp())
    tex = _create_tex(formula, packages)
    tex_fn = f'{unique_name}.tex'

    background = Color(51, 54, 61)

    tmp = _render_image(unique_name, tex, tex_fn, background, dpi, verbose)

    buf = _read_image(tmp.name)

    tmp.cleanup()
    return buf, f'{unique_name}.png' if buf is not None else ''
