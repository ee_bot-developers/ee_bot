# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from PIL import Image, ImageFont, ImageDraw
import io
import numpy as np
from typing import Tuple, Union, Optional

_STATUS_COLOR = {
    discord.Status.idle.value: (250, 165, 27),
    discord.Status.do_not_disturb.value: (240, 72, 72),
    discord.Status.invisible.value: (116, 127, 141),
    discord.Status.offline.value: (116, 127, 141),
    discord.Status.online.value: (68, 179, 127)
}


def _drawProgressBar(d: ImageDraw.ImageDraw,
                     x: int,
                     y: int,
                     w: int,
                     h: int,
                     progress: float,
                     bg: Union[str, Tuple[int, int, int, int]] = 'black',
                     fg: Union[str, Tuple[int, int, int, int]] = 'red') -> ImageDraw.ImageDraw:
    # Draw background
    d.ellipse((x + w, y, x + h + w, y + h), fill=bg)
    d.ellipse((x, y, x + h, y + h), fill=bg)
    d.rectangle((x + (h / 2), y, x + w + (h / 2), y + h), fill=bg)

    # Draw progress
    w *= progress
    d.ellipse((x + w, y, x + h + w, y + h), fill=fg)
    d.ellipse((x, y, x + h, y + h), fill=fg)
    d.rectangle((x + (h / 2), y, x + w + (h / 2), y + h), fill=fg)

    return d


def _drawAvatar(avatar: Image.Image,
                status: Tuple[int, int, int],
                r: int,
                status_r: int,
                x: int,
                y: int,
                outline: Union[str, Tuple[int, int, int, int]]) -> Image.Image:
    npImage = np.array(avatar)
    h, w, _ = npImage.shape

    alpha = Image.new('L', (h, w), 0)
    draw = ImageDraw.Draw(alpha)
    draw.pieslice(((0, 0), (h, w)), 0, 360, fill=255)

    npAlpha = np.array(alpha)

    npImage = np.dstack((npImage[:, :, :3], npAlpha))

    newImg = Image.fromarray(npImage)

    newImg = newImg.resize((r, r))
    d = ImageDraw.Draw(newImg)
    upperLeft = (x - status_r, y - status_r)
    lowerRight = (x + status_r, y + status_r)
    d.ellipse((upperLeft, lowerRight), fill=status, width=2, outline=outline)

    return newImg


def _drawBorder(d: ImageDraw.ImageDraw,
                img_size: Tuple[int, int],
                width: int,
                radius: Optional[float],
                fg: Union[str, Tuple[int, int, int, int]]) -> ImageDraw.ImageDraw:
    border = (width, width, img_size[0] - width, img_size[1] - width)
    if radius is not None:
        d.rounded_rectangle(border, radius, fill=fg)
    else:
        d.rectangle(border, fill=fg)
    return d


def _drawText(d: ImageDraw.ImageDraw,
              x: int,
              y: int,
              font: str,
              fontSize: int,
              text: str,
              fill: Union[str, Tuple[int, int, int, int]],
              anchor: str = 'la') -> Tuple[ImageDraw.ImageDraw, int]:
    fnt = ImageFont.truetype(font, fontSize)
    d.text((x, y), text, font=fnt, fill=fill, anchor=anchor)
    width, _ = d.textsize(text, font=fnt)
    return d, width


def createRankImage(display_name: str,
                    display_avatar: io.BytesIO,
                    status: discord.Status,
                    currentLevel: int,
                    progress: float,
                    leaderboardPos: int,
                    currentXP: int,
                    requiredXP: int,
                    border_color: Union[str, Tuple[int, int, int, int]],
                    image_background: Union[str, Tuple[int, int, int, int]],
                    progressColor: Union[str, Tuple[int, int, int, int]],
                    progressBackground: Union[str, Tuple[int, int, int, int]],
                    text_color: Union[str, Tuple[int, int, int, int]]) -> io.BytesIO:
    # Image parameters
    image_size = (984, 232)
    avatar_r = 160
    status_r = 20
    border_width = 20
    foreground_radius = 18
    progressXAnchor = 232
    progressYAnchor = 160
    progressWidth = 645
    progressHeight = 40
    usernameXAnchor = 260
    usernameYAnchor = 90
    usernameFontSize = 45
    currentLevelXAnchor = 930
    currentLevelYAnchor = 86
    currentLevelFontSize = 70
    levelFontSize = 30
    levelSpacing = 5
    requiredXpXAnchor = 930
    xpYAnchor = 140
    xpFontSize = 28
    font = './images/fonts/segoeui.ttf'

    # Construct base image
    img = Image.open(display_avatar).convert('RGBA')
    out = Image.new('RGBA', image_size, border_color)

    # Start image drawing
    d = ImageDraw.ImageDraw(out)
    d = _drawBorder(d, image_size, border_width, foreground_radius, image_background)

    # Create new avatar image and overlay it onto image that is getting constructed
    avatar = _drawAvatar(img, _STATUS_COLOR[status.value], avatar_r, status_r, avatar_r - status_r, avatar_r - status_r,
                         image_background)
    x, y = out.size
    offset = (32, (y - avatar_r) // 2)
    out.alpha_composite(avatar, offset)

    d = _drawProgressBar(d, progressXAnchor, progressYAnchor, progressWidth, progressHeight, progress / 100,
                         fg=progressColor, bg=progressBackground)

    d, _ = _drawText(d, usernameXAnchor, usernameYAnchor, font, usernameFontSize, display_name,
                     fill=text_color)

    d, width = _drawText(d, currentLevelXAnchor, currentLevelYAnchor, font, currentLevelFontSize, str(currentLevel),
                         progressColor, 'rb')

    levelXAnchor = currentLevelXAnchor - width - levelSpacing

    d, width = _drawText(d, levelXAnchor, currentLevelYAnchor, font, levelFontSize, 'LEVEL', progressColor, 'rb')

    leaderboardPosXAnchor = levelXAnchor - width - (2 * levelSpacing)

    d, width = _drawText(d, leaderboardPosXAnchor, currentLevelYAnchor, font, currentLevelFontSize,
                         f'#{leaderboardPos}', text_color, 'rb')

    rankPosXAnchor = leaderboardPosXAnchor - width - (2 * levelSpacing)

    d, _ = _drawText(d, rankPosXAnchor, currentLevelYAnchor, font, levelFontSize, 'RANK', text_color, 'rb')

    d, width = _drawText(d, requiredXpXAnchor, xpYAnchor, font, xpFontSize, f' / {requiredXP} XP', progressBackground,
                         'rb')

    currentXpXAnchor = requiredXpXAnchor - width

    _drawText(d, currentXpXAnchor, xpYAnchor, font, xpFontSize, str(currentXP), text_color, 'rb')

    buf = io.BytesIO()
    out.save(buf, format='png', quality=96)
    buf.seek(0)
    return buf
