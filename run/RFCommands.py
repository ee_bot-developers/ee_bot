# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import discord
from discord.ext import commands
from discord import app_commands, File
from typing import Optional
from EE_Bot_DB import EE_DB, cogEnum
from subprocess import Popen, PIPE
import threading
import asyncio
import os
import io
from random import randint


rfExists = os.path.isdir('RF')


class RF(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db: EE_DB = bot.db
        self.MASTER = bot.MASTER
        return

    async def cog_load(self) -> None:
        print('RF Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        print('RF Cog Unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction):
        if ctx.guild_id is None:
            return True
        if not await self.db.checkCog(ctx.guild_id, cogEnum.RF):
            await ctx.response.send_message('This command has been disabled')
            return False
        return True

    @staticmethod
    def runShellCommand(args, mutex, outputDict):
        process = Popen(args, stderr=PIPE, stdout=PIPE, universal_newlines=True)
        output, error = process.communicate()

        mutex.acquire()
        outputDict['output'] = output
        outputDict['error'] = error
        outputDict['done'] = True
        mutex.release()
        return

    async def runCommand(self,
                         ctx: discord.Interaction,
                         cmd: str,
                         args: list,
                         outputFile: bool = False,
                         file: Optional[discord.Attachment] = None):
        if not await self._isEnabled(ctx):
            return

        if file is not None:
            await file.save(fp=f'./RF/{file.filename}')

        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        RF_thread = threading.Thread(target=self.runShellCommand, args=(args, mutex, outputDict))
        RF_thread.start()

        await ctx.response.defer()

        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']

        if outputFile:
            outBuffer = io.StringIO(output)
            errorBuffer = io.StringIO(error)
            files = [File(outBuffer, f'{cmd}-out.txt'), File(errorBuffer, f'{cmd}-error.log')]
            await ctx.followup.send(files=files)
        else:
            msg = ''
            if output:
                msg += f'```{output}```'
            if error:
                msg += f'```diff\n- {error}```'
            await ctx.followup.send(msg)

        if file is not None:
            L = ['rm', '-f', file.filename]
            process = Popen(L)
            process.communicate()

        return

    @app_commands.command(description='Convert Watts to dBm')
    @app_commands.describe(
        power='The power in Watts'
    )
    async def dbm(self,
                  ctx: discord.Interaction,
                  power: str):
        L = ['./RF/ee432.out', 'dbm', power]
        await self.runCommand(ctx, 'dbm', L)
        return

    @app_commands.command(description='Convert dms coordinates to degrees coordinates')
    @app_commands.describe(
        degrees='degrees part of dms coordinates',
        minutes='minutes part of dms coordinates',
        seconds='seconds part of dms coordinates'
    )
    async def dms_to_deg(self,
                         ctx: discord.Interaction,
                         degrees: int,
                         minutes: int,
                         seconds: int):
        L = ["RF/ee432.out", "dms_to_deg", str(degrees), str(minutes), str(seconds)]
        await self.runCommand(ctx, 'dms_to_deg', L)
        return

    @app_commands.command(description='Convert degrees coordinates to dms coordinates')
    @app_commands.describe(
        degrees='The coordinate in degrees.'
    )
    async def deg_to_dms(self,
                         ctx: discord.Interaction,
                         degrees: float):
        L = ["RF/ee432.out", "deg_to_dms", str(degrees)]
        await self.runCommand(ctx, 'deg_to_dms', L)
        return

    @app_commands.command(description='Generate a rayleigh random variable.')
    async def rayrand(self, ctx: discord.Interaction):
        L = ["RF/ee432.out", "rayrand", str(randint(1, 1000))]
        await self.runCommand(ctx, 'rayrand', L)
        return

    @app_commands.command(description='Calculates the diffraction gain given topography data.')
    @app_commands.describe(
        landscape_data='The file that contains the topography data. See /help diffraction_gain for file format.',
        ht='The height of the transmitter antenna.',
        hr='The height of the receiver antenna.',
        wavelength='The wavelength of the signal. (c/f)'
    )
    async def diffraction_gain(self,
                               ctx: discord.Interaction,
                               landscape_data: discord.Attachment,
                               ht: float,
                               hr: float,
                               wavelength: float):
        L = ["./RF-interface.out", "defractGain", landscape_data.filename, str(ht), str(hr), str(wavelength)]
        await self.runCommand(ctx, 'defractGain', L, file=landscape_data)
        return

    @app_commands.command(description='Calculate carrier capacity')
    @app_commands.describe(
        channels='Number of channels available',
        pb='The blocking probability'
    )
    async def erlangs(self,
                      ctx: discord.Interaction,
                      channels: int,
                      pb: float):
        L = ["RF/ee432.out", "erlangs", str(channels), str(pb)]
        await self.runCommand(ctx, 'erlangs', L)
        return

    @app_commands.command(description='Generate a table of erlangs.')
    @app_commands.describe(
        min_channels='Lower channel limit, must be used with max channels. Default: 10',
        max_channels='Upper channel limit, must be used with min channels. Default: 100',
        pb='Block probability. Default: 0.01'
    )
    async def erlangs_table(self,
                            ctx: discord.Interaction,
                            min_channels: Optional[int],
                            max_channels: Optional[int],
                            pb: Optional[int]):
        min_max = 0
        L = ["RF/ee432.out", "erlang_table"]
        if min_channels is not None:
            min_max = 1
        if max_channels is not None:
            min_max |= 2
        if min_max != 0 and min_max != 3:
            await ctx.response.send_message('Error: min_channels and max_channels must both be used!')
            return
        if min_max:
            L.append('--range')
            L.append(str(min_channels))
            L.append(str(max_channels))
        if pb is not None:
            L.append('--pb')
            L.append(str(pb))
        await self.runCommand(ctx, 'erlang_table', L, True)
        return

    @app_commands.command(description='Calculates the exposure radius for controlled and uncontrolled exposure.')
    @app_commands.describe(
        frequency='The frequency of the basestation.',
        gain='The gain of the base station.',
        power='The transmitted power of the base station.'
    )
    async def exposure_radius(self,
                              ctx: discord.Interaction,
                              frequency: float,
                              gain: float,
                              power: float):
        L = ["RF/ee432.out", "exposure-radius", str(frequency), str(gain), str(power)]
        await self.runCommand(ctx, 'exposure-radius', L)
        return

    @app_commands.command(description='Generates a table of different cluster size combinations.')
    @app_commands.describe(
        max_size='The maximum cluster size'
    )
    async def cluster_sizes(self,
                            ctx: discord.Interaction,
                            max_size: int):
        L = ["RF/ee432.out", "cluster-size", str(max_size)]
        await self.runCommand(ctx, 'cluster-size', L, True)
        return

    @app_commands.command(description='Calculates the output of the Q function')
    @app_commands.describe(
        x='Input of Q function'
    )
    async def q(self,
                ctx: discord.Interaction,
                x: float):
        L = ["RF/ee432.out", "Q", str(x)]
        await self.runCommand(ctx, 'Q', L)
        return

    @app_commands.command(description='Calculates the output of the inverse Q function.')
    @app_commands.describe(
        x='Input of inverse Q function'
    )
    async def qinv(self,
                   ctx: discord.Interaction,
                   x: float):
        L = ["RF/ee432.out", "Qinv", str(x)]
        await self.runCommand(ctx, 'Qinv', L)
        return


async def setup(bot):
    if rfExists:
        await bot.add_cog(RF(bot=bot))
