# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import app_commands, Embed
from discord.ext import commands, tasks
from typing import Optional, Literal, Dict
from EE_Bot_DB import EE_DB, channelType, cogEnum
from datetime import datetime, timedelta, time
from enum import IntEnum
import pytz
import asyncio
import pickle as pk
import io
from collections import deque
from globals import embedColor, launchFlag


# Autocomplete choices for times of execution and timezones
dayChoices = [app_commands.Choice(name=str(num), value=num) for num in range(1, 32)]
minuteChoices = [app_commands.Choice(name=str(num), value=num) for num in range(60)]
hourChoices = [app_commands.Choice(name=str(num), value=num) for num in range(24)]
timezoneChoices = [app_commands.Choice(name=tz, value=tz) for tz in pytz.common_timezones]
UTC_MIDNIGHT = time(hour=0, minute=0, second=0, tzinfo=pytz.utc)


class taskEnum(IntEnum):
    """Enumerated type for tasks"""
    NONE = 0
    ARCHIVE = 1
    CLEAR_ROLES = 2


class taskData:
    """Class that contains the task data"""
    def __init__(self, taskType: taskEnum, channel: Optional[discord.TextChannel], role: Optional[discord.Role]):
        self._type = taskType
        self._channel = None
        self._role = None
        self.MessageReferences: Dict[int, int] = {}
        if self._type is taskType.ARCHIVE:
            if channel is None:
                raise ValueError('Channel must be provided for archive tasks!')
            self._channel = channel.id

        if self._type is taskType.CLEAR_ROLES:
            if role is not None:
                self._role = role.id

        self._complete = False
        return

    def __eq__(self, other):
        if type(other) != type(self):
            return False
        return self._type == other.taskType and self._role == other.role and self._channel == other.channel

    def __bool__(self):
        return self._complete

    @property
    def channel(self):
        return self._channel

    @property
    def taskType(self):
        return self._type

    @property
    def role(self):
        return self._role

    def markComplete(self):
        self._complete = True


class taskQueue:
    """A custom queue to contain the tasks"""
    def __init__(self, guild: int):
        self._guild = guild
        self._tasks: deque = deque(maxlen=20)
        self._locked = False
        self._dateExecution = None
        self._running = False
        return

    def __int__(self):
        return self._guild

    def __len__(self):
        return len(self._tasks)

    def __iter__(self) -> taskData:
        for x in self._tasks:
            yield x

    def __getitem__(self, item):
        return self._tasks[item]

    def __delitem__(self, key):
        del self._tasks[key]

    def __contains__(self, item):
        return item in self._tasks

    def __bool__(self):
        return self._locked

    def __eq__(self, other):
        return other == self._guild

    def clear(self):
        if not self._locked:
            self._tasks.clear()

    def newArchive(self, channel: discord.TextChannel):
        data = taskData(taskEnum.ARCHIVE, channel, None)
        if data not in self._tasks:
            self._tasks.append(data)
            return 'Created new archive task!'
        return 'Archive tasks cannot be duplicated!'

    def newClearRoles(self, role: Optional[discord.Role] = None):
        data = taskData(taskEnum.CLEAR_ROLES, None, role)
        if data not in self._tasks:
            self._tasks.append(data)
            return 'Created a new clear role task!'
        return 'Clear role tasks cannot be duplicated!'

    @property
    def locked(self) -> bool:
        return self._locked

    @property
    def date(self) -> Optional[datetime]:
        return self._dateExecution

    @property
    def guild(self) -> int:
        return self._guild

    @property
    def isRunning(self) -> bool:
        return self._running

    def setRunning(self, running: bool) -> None:
        self._running = running

    def unlock(self):
        self._locked = False
        self._dateExecution = None
        return

    def scheduleTasks(self, month: int, day: int):
        if self._locked:
            return 'Tasks are already scheduled!'
        now = datetime.now()
        try:
            if now.month == 12 and now.day > 1 and month < 12:
                dt = datetime(now.year + 1, month, day)
            else:
                dt = datetime(now.year, month, day)
        except ValueError:
            return 'Invalid date given!'

        # If same day, schedule for next day
        if (dt - now).total_seconds() > -86400:
            if (dt - now).total_seconds() < 0:
                dt += timedelta(days=1)

        # Check if date has already passed
        if (dt - now).total_seconds() < 0:
            return 'Date has already passed!'

        if (dt - now).total_seconds() > 2592000:
            return 'Cannot schedule a task that is more than 30 days away!'

        if not self._tasks:
            return 'No tasks given!'

        self._locked = True
        self._dateExecution = dt
        return 'Scheduling tasks...'

    def get_task(self) -> taskData:
        return self._tasks[0]

    def markTaskComplete(self) -> None:
        self._tasks.popleft()


class Announcement:
    """Custom container class to hold announcement data"""
    def __init__(self, guild: int, Time: str, timezone: str, msg: str, channel_id: int = -1):
        self._msg = msg
        self._guild = guild
        self._channel = channel_id
        local = pytz.timezone(timezone)
        now = datetime.now()
        native = datetime.strptime(f'{Time} {now: %m-%d-%Y}', '%H:%M %m-%d-%Y')
        local_dt = local.localize(native, is_dst=None)
        self._utc_time = local_dt.astimezone(pytz.utc)
        # Recalculate if time has already passed today and move to next day if it has
        if (self._utc_time - datetime.now(pytz.utc)).total_seconds() < 0:
            self._utc_time += timedelta(days=1)
        return

    def __int__(self):
        return self._guild

    def __str__(self):
        return self._msg

    def __eq__(self, other):
        return self._guild == other

    @property
    def time(self):
        return self._utc_time

    @property
    def channel(self):
        return self._channel


@app_commands.guild_only()
class Schedule(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db: EE_DB = bot.db
        self.MASTER = bot.MASTER
        self._taskQueues: Dict[int, taskQueue] = {}
        self._maxLength = 20
        self._runningTasks: Dict[int, asyncio.Task] = {}
        self._changed = False
        self._cancelMsg = 'Tasks cancelled'
        self._DEBUG = not launchFlag
        self._announcements: Dict[int, Announcement] = {}
        self._announcement_tasks: Dict[int, asyncio.Task] = {}
        self._announcements_changed = False
        self._announcement_cancel_msg = 'Announcement cancelled'
        self._termDict = {
            1: 'Spring',
            2: 'Spring',
            3: 'Spring',
            4: 'Spring',
            5: 'Spring',
            6: 'Summer',
            7: 'Summer',
            8: 'Summer',
            9: 'Fall',
            10: 'Fall',
            11: 'Fall',
            12: 'Fall'
        }
        return

    async def cog_load(self) -> None:
        await self.loadQueues(launchFlag)
        self.saveQueues.start(launchFlag)
        print('Schedule Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        await self.killQueues(launchFlag)
        print('Schedule Cog Unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction) -> bool:
        if not await self.db.checkCog(ctx.guild_id, cogEnum.Schedule):
            await ctx.response.send_message('This command has been disabled!', ephemeral=True)
            return False
        if not ctx.user.guild_permissions.administrator:
            await ctx.response.send_message('You do not have permission to do this!', ephemeral=True)
            return False
        if ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log)) is None:
            await ctx.response.send_message('Please run /configure first!', ephemeral=True)
            return False
        return True

    @tasks.loop(time=UTC_MIDNIGHT)
    async def startTasks(self):
        now = datetime.now()
        for guild_id in self._taskQueues:
            if guild_id in self._runningTasks:
                continue
            if self._taskQueues[guild_id].date is None:
                continue
            elif self._taskQueues[guild_id].date <= now:
                task = asyncio.create_task(self.runTasks(guild_id))
                self._runningTasks[guild_id] = task
                self._taskQueues[guild_id].setRunning(True)

    @app_commands.command(description='Create a task')
    @app_commands.describe(
        task='Task type',
        channel='Channel that get archived',
        role='Role that gets cleared'
    )
    async def create_task(self,
                          ctx: discord.Interaction,
                          task: Literal['archive', 'clear role(s)'],
                          channel: Optional[discord.TextChannel],
                          role: Optional[discord.Role]):
        if not await self._isEnabled(ctx):
            return

        if ctx.guild_id not in self._taskQueues:
            self._taskQueues[ctx.guild_id] = taskQueue(ctx.guild_id)

        if len(self._taskQueues[ctx.guild_id]) >= self._maxLength:
            await ctx.response.send_message('You cannot insert anymore tasks into the queue!', ephemeral=True)
            return

        if task == 'archive':
            if channel is None:
                await ctx.response.send_message('Channel is needed for archive task!', ephemeral=True)
                return

            if not bool(self._taskQueues[ctx.guild_id]):
                await ctx.response.send_message(self._taskQueues[ctx.guild_id].newArchive(channel), ephemeral=True)
                self._changed = True
                return
            await ctx.response.send_message('Unable to add archive task! The queue is locked. To unlock the queue, '
                                            'run the /cancel_tasks command.', ephemeral=True)
            return
        elif task == 'clear role(s)':
            if not bool(self._taskQueues[ctx.guild_id]):
                await ctx.response.send_message(self._taskQueues[ctx.guild_id].newClearRoles(role), ephemeral=True)
                self._changed = True
                return
            await ctx.response.send_message('Unable to add clear roles task! The queue is locked. To unlock the '
                                            'queue, run the /cancel_tasks command.', ephemeral=True)
            return
        await ctx.response.send_message('Creation failed', ephemeral=True)
        return

    @app_commands.command(description='View the task queue')
    async def view_tasks(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return

        if ctx.guild_id not in self._taskQueues:
            embed = Embed(title="Task Queue", description="There are no tasks in the queue.", color=embedColor)
            await ctx.response.send_message(embed=embed, ephemeral=True)
            return

        await ctx.response.defer()

        embed = Embed(title="Task Queue", color=embedColor)
        i = 1
        for x in self._taskQueues[ctx.guild_id]:
            if x.taskType == taskEnum.ARCHIVE:
                param = f'Archive <#{x.channel}>'
            elif x.taskType == taskEnum.CLEAR_ROLES:
                if x.role is None:
                    param = 'Clear roles'
                else:
                    param = f'Clear roles <@&{x.role}>'
            else:
                param = 'None'
            name = f'{i}'
            embed.add_field(name=name, value=param, inline=False)
            i += 1
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        await ctx.followup.send(embed=embed)
        return

    @app_commands.command(description='Removes a task from the task queue by ID')
    @app_commands.describe(
        task_id='The task id given by /view_tasks'
    )
    async def remove_task(self,
                          ctx: discord.Interaction,
                          task_id: int):
        if not await self._isEnabled(ctx):
            return

        if ctx.guild_id not in self._taskQueues:
            await ctx.response.send_message('No tasks in the queue!', ephemeral=True)
            return

        # Check if id is valid
        if task_id < 1 or task_id > len(self._taskQueues[ctx.guild_id]):
            await ctx.response.send_message('Task not found!', ephemeral=True)
            return

        if bool(self._taskQueues[ctx.guild_id]):
            await ctx.response.send_message('Unable to remove task. The queue is locked. To unlock the queue, '
                                            'run the /cancel_tasks command.', ephemeral=True)
            return

        # Delete item or entire queue given the queue size
        if len(self._taskQueues[ctx.guild_id]) != 1:
            del self._taskQueues[ctx.guild_id][task_id - 1]
        else:
            del self._taskQueues[ctx.guild_id]
        await ctx.response.send_message('Task removed!', ephemeral=True)
        self._changed = True
        return

    @app_commands.command(description='Clears the task queue')
    async def clear_tasks(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return

        if ctx.guild_id not in self._taskQueues:
            await ctx.response.send_message('No tasks in the queue!', ephemeral=True)
            return

        if bool(self._taskQueues[ctx.guild_id]):
            await ctx.response.send_message('Unable to remove task. The queue is locked. To unlock the queue, '
                                            'run the /cancel_tasks command.', ephemeral=True)
            return

        del self._taskQueues[ctx.guild_id]
        await ctx.response.send_message('Cleared task queue')
        self._changed = True
        return

    @app_commands.command(description='Schedules a date (UTC midnight) for the task queue to execute.')
    @app_commands.describe(
        month='Month of execution',
        day='Day of execution',
        announce='Announce this to the server?',
        announcement_channel='The channel the announcement will be made in'
    )
    async def schedule_tasks(self,
                             ctx: discord.Interaction,
                             month: Literal[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                             day: int,
                             announce: Literal['yes', 'no'],
                             announcement_channel: Optional[discord.TextChannel] = None):
        if not await self._isEnabled(ctx):
            return
        # TODO: Create a modal for this once there is support for selection menus
        if ctx.guild_id not in self._taskQueues:
            await ctx.response.send_message('Task queue is empty!', ephemeral=True)
            return

        if self._taskQueues[ctx.guild_id].locked:
            await ctx.response.send_message('Tasks are already scheduled!', ephemeral=True)
            return

        response = self._taskQueues[ctx.guild_id].scheduleTasks(month, day)

        if not self._taskQueues[ctx.guild_id].locked:
            await ctx.response.send_message(response, ephemeral=True)
            return

        if announce == 'yes' and announcement_channel is None:
            await ctx.response.send_message('Error: Announcement channel must be specified when selecting yes!',
                                            ephemeral=True)
            return
        await ctx.response.send_message(response, ephemeral=True)

        if announce == 'yes':
            date = self._taskQueues[ctx.guild_id].date
            suffix = {1: 'st', 2: 'nd', 3: 'rd'}.get(date.day % 10, 'th') if date.day not in (11, 12, 13) else 'th'
            embed = discord.Embed(title='Tasks scheduled for execution',
                                  description=f"@everyone, the following tasks have been scheduled for execution on "
                                              f"{date.strftime('%B')} {date.day}{suffix}\n\u200b", color=embedColor)
            for x in self._taskQueues[ctx.guild_id]:
                if x.taskType == taskEnum.ARCHIVE:
                    taskType = 'Archive'
                    param = f'<#{x.channel}>'
                elif x.taskType == taskEnum.CLEAR_ROLES:
                    taskType = 'CLear Roles'
                    if x.role is None:
                        param = '\u200b'
                    else:
                        param = f'<@&{x.role}>'
                else:
                    continue
                embed.add_field(name=taskType, value=param, inline=False)
            embed.set_thumbnail(url=self.bot.user.display_avatar.url)
            await announcement_channel.send(embed=embed)
        return

    @schedule_tasks.autocomplete('day')
    async def schedule_tasks_day_autocomplete(self, _, current: str):
        return [day for day in dayChoices if day.name.startswith(current)][:25]

    @app_commands.command(description='Cancel the scheduled tasks')
    async def cancel_tasks(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        if ctx.guild_id not in self._taskQueues:
            await ctx.response.send_message('Task Queue empty!', ephemeral=True)
            return

        if not self._taskQueues[ctx.guild_id].locked:
            await ctx.response.send_message('Tasks were not scheduled!', ephemeral=True)
            return

        await ctx.response.defer()

        self._taskQueues[ctx.guild_id].unlock()

        if ctx.guild_id in self._runningTasks:
            self._runningTasks[ctx.guild_id].cancel()
            del self._runningTasks[ctx.guild_id]
            self._taskQueues[ctx.guild_id].setRunning(False)
        self._changed = True
        await ctx.followup.send('Tasks cancelled!', ephemeral=True)
        return

    @app_commands.command(description='Schedules an announcement for the server.')
    async def schedule_announcement(self, ctx: discord.Interaction,
                                    channel: discord.TextChannel,
                                    message: str,
                                    hour: int,
                                    minute: int,
                                    timezone: Optional[str] = 'UTC'):
        if not await self._isEnabled(ctx):
            return
        # TODO: Create a modal for this command once discord implements the proper stuff
        if ctx.guild_id in self._announcements:
            await ctx.response.send_message('This server already has an announcement scheduled', ephemeral=True)
            return
        if hour not in range(24) or minute not in range(60):
            await ctx.response.send_message('Invalid input time', ephemeral=True)
            return
        if timezone not in pytz.common_timezones:
            await ctx.response.send_message('Invalid timezone', ephemeral=True)
            return
        self._announcements[ctx.guild_id] = Announcement(ctx.guild_id, f'{hour}:{minute}', timezone, message,
                                                         channel.id)
        self._announcement_tasks[ctx.guild_id] = asyncio.create_task(self.runAnnouncement(
            self._announcements[ctx.guild_id]))
        Time = time(hour, minute).strftime('%H:%M')
        await ctx.response.send_message(f'Announcement scheduled for {Time}', ephemeral=True)
        return

    @schedule_announcement.autocomplete('hour')
    async def autocomplete_hour(self, _, current: str):
        return [hour for hour in hourChoices if hour.name.startswith(current)]

    @schedule_announcement.autocomplete('minute')
    async def autocomplete_minute(self, _, current: str):
        return [minute for minute in minuteChoices if minute.name.startswith(current)][:25]

    @schedule_announcement.autocomplete('timezone')
    async def autocomplete_timezone(self, _, current: str):
        return [timezone for timezone in timezoneChoices if timezone.name.startswith(current)][:25]

    @app_commands.command(description='Cancels a scheduled announcement')
    async def cancel_announcement(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return

        if ctx.guild_id not in self._announcements:
            await ctx.response.send_message('There are no scheduled announcements!', ephemeral=True)
            return

        self._announcement_tasks[ctx.guild_id].cancel()
        del self._announcement_tasks[ctx.guild_id]
        del self._announcements[ctx.guild_id]

        await ctx.response.send_message('Announcement cancelled', ephemeral=True)
        self._announcements_changed = True
        return

    async def runTasks(self, guild: int, message: str = 'Starting tasks...'):
        guild = self.bot.get_guild(guild)
        log = guild.get_channel(await self.db.getLoggingChannel(guild.id, channelType.log))
        if log is None:
            self._taskQueues[guild.id].unlock()
            del self._runningTasks[guild.id]
            return

        try:
            await log.send(message)

            # Run tasks specified by task queue
            for task in self._taskQueues[guild.id]:
                if task.taskType == taskEnum.ARCHIVE:
                    await self.runArchive(guild, task.channel, task)
                elif task.taskType == taskEnum.CLEAR_ROLES:
                    role = None
                    if task.role is not None:
                        role = guild.get_role(task.role)
                    await self.runClearRoles(guild, role)
                self._taskQueues[guild.id].markTaskComplete()
                self._changed = True

            await log.send('Finished running tasks')

            del self._taskQueues[guild.id]
            del self._runningTasks[guild.id]
            self._changed = True

        except asyncio.CancelledError:
            self._changed = True
            await log.send(self._cancelMsg)
            raise

    async def runArchive(self, guild: discord.Guild, channel: int, task: taskData):
        channel = guild.get_channel(channel)
        while not await self.bot.ArchiveTasks(guild.id, True):
            await asyncio.sleep(0.5)
        log = guild.get_channel(await self.db.getLoggingChannel(guild.id, channelType.log))

        await log.send(f'Archiving {channel.name}')

        newChannel = await channel.clone()
        await newChannel.edit(position=channel.position)
        archiveChannelName = f'{channel.name}-archive'

        category = discord.utils.get(guild.categories, name='Archive')

        if category is None:
            category = await guild.create_category('Archive')
            await category.set_permissions(guild.default_role, add_reactions=False, attach_files=False,
                                           manage_channels=False, manage_permissions=False, read_message_history=True,
                                           read_messages=False, send_messages=False, send_tts_messages=False,
                                           use_application_commands=False)

        archiveChannel = discord.utils.get(category.text_channels, name=archiveChannelName)
        prefix = await self.db.getChannelPrefix(guild.id)

        if archiveChannel is None:
            position = 0
            end = True
            channels = category.text_channels
            try:
                channel_code = int(channel.name.lstrip(prefix))
                for x in channels:
                    try:
                        if int(x.name.lstrip(prefix).rstrip('-archive')) > channel_code:
                            position = x.position
                            end = False
                            break
                    except ValueError:
                        await log.send(f'<#{x.id}> channel name does not conform to naming standards of this server!')
            except ValueError:
                await log.send('Non-standard prefix detected. Please manually move the channel to the desired '
                               'position within the archive category')
            archiveChannel = await category.create_text_channel(archiveChannelName)

            if not end:
                await archiveChannel.edit(position=position)

        # Get roles of channel being archived and update their permissions
        roles = channel.changed_roles
        overwrite = discord.PermissionOverwrite(
            send_messages=False,
            read_messages=False,
            read_message_history=False
        )
        for x in roles:
            await channel.set_permissions(x, overwrite=overwrite)

        # Get roles from archive channel, save their permissions for later, and update them
        roles = archiveChannel.changed_roles
        role_permissions = []
        for x in roles:
            role_permissions.append([x, archiveChannel.overwrites_for(x)])
            await archiveChannel.set_permissions(x, overwrite=overwrite)

        # Create archive header
        date = datetime.today()
        month = date.month
        year = date.year
        term = f'{self._termDict[month]} {year}'

        if not task.MessageReferences:
            await archiveChannel.send(f'__**Start of the {term} semester**__')

        MsgRef = None
        refDeleted = False

        async for message in channel.history(limit=None, oldest_first=True):
            # If got interrupted during archive process, iterate to correct position
            if message.id in task.MessageReferences.values():
                continue
            embed = discord.Embed(timestamp=message.created_at, color=embedColor)
            embed.set_author(name=message.author.display_name, icon_url=message.author.display_avatar)

            if message.reference is not None:
                if message.type == discord.MessageType.pins_add:
                    try:
                        MsgRef = await archiveChannel.fetch_message(
                            task.MessageReferences[message.reference.message_id])
                        await MsgRef.pin()
                    except KeyError:
                        pass
                    continue
                elif message.reference.guild_id == message.guild.id and \
                        (message.reference.channel_id == message.channel.id):
                    try:
                        MsgRef = await archiveChannel.fetch_message(
                            task.MessageReferences[message.reference.message_id])
                    except KeyError:
                        refDeleted = True
                else:
                    MsgRef = message.reference

            files = []
            if message.attachments:
                for f in message.attachments:
                    file = await f.to_file(spoiler=f.is_spoiler())
                    files.append(file)

            if message.content:
                embed.description = message.content

            if refDeleted:
                MsgRef = await archiveChannel.send('Placeholder, to be deleted')

            try:
                newMsg = await archiveChannel.send(embed=embed, files=files, reference=MsgRef,
                                                   stickers=message.stickers)
            except discord.HTTPException as err:
                newMsg = await archiveChannel.send(content=str(err), embed=embed, reference=MsgRef)

            if refDeleted:
                await MsgRef.delete()

            task.MessageReferences[message.id] = newMsg.id
            MsgRef = None
            refDeleted = False

            await asyncio.sleep(3)

        await archiveChannel.send(f'__**End of the {term} semester**__')

        # Reset permissions from earlier
        for x in role_permissions:
            await archiveChannel.set_permissions(x[0], overwrite=x[1])

        # Delete the old channel
        await channel.delete()

        await log.send(f'Finished archiving {channel.name}')

        # Release the archive flag
        await self.bot.ArchiveTasks(guild.id, False)
        return

    async def runClearRoles(self, guild: discord.Guild, role: discord.Role):
        while not await self.bot.PurgeTasks(guild.id, True):
            await asyncio.sleep(0.5)
        log = guild.get_channel(await self.db.getLoggingChannel(guild.id, channelType.log))

        await log.send('Starting role purge...')

        if role is not None:
            members = role.members
            for member in members:
                await member.remove_roles(role)
        else:
            members = guild.members
            for member in members:
                roles = member.roles
                roles = tuple([x for x in roles if str(x).isdigit()])
                if roles:
                    await member.remove_roles(*roles, atomic=True)

        await log.send('Finished purging roles...')
        await self.bot.PurgeTasks(guild.id, False)
        return

    async def runAnnouncement(self, announcement: Announcement):
        guild = self.bot.get_guild(int(announcement))
        log = guild.get_channel(await self.db.getLoggingChannel(guild.id, channelType.log))
        seconds = (announcement.time - datetime.now(pytz.utc)).total_seconds()
        try:
            await asyncio.sleep(seconds)
            channel = guild.get_channel(announcement.channel)
            if channel is None:
                await log.send('Error: Unable to find channel for announcements.')
                return
            await channel.send(str(announcement))
            del self._announcements[int(announcement)]
            del self._announcement_tasks[int(announcement)]
            self._announcements_changed = True
        except asyncio.CancelledError:
            await log.send(self._announcement_cancel_msg)
            return

    @tasks.loop(minutes=5)
    async def saveQueues(self, launched: bool):
        if self._changed and launched:
            taskQ = io.BytesIO()
            pk.dump(self._taskQueues, taskQ)
            taskQ.seek(0)
            await self.db.dumpPickleData('taskQueues.pk', taskQ.read())
            self._changed = False
        if self._announcements_changed and launched:
            announcements = io.BytesIO()
            pk.dump(self._announcements, announcements)
            announcements.seek(0)
            await self.db.dumpPickleData('announcements.pk', announcements.read())
            self._announcements_changed = False

    async def killQueues(self, launched: bool):
        self._cancelMsg = 'Tasks cancelled due to bot maintenance.'
        self._announcement_cancel_msg = 'Announcement cancelled due to bot maintenance. The announcement will be ' \
                                        'rescheduled when the bot is back online.'

        if self._runningTasks:
            for guild in self._runningTasks:
                self._runningTasks[guild].cancel()
        if self._announcement_tasks:
            for guild in self._announcement_tasks:
                self._announcement_tasks[guild].cancel()
        if launched:
            taskQ = io.BytesIO()
            pk.dump(self._taskQueues, taskQ)
            taskQ.seek(0)
            await self.db.dumpPickleData('taskQueues.pk', taskQ.read())
            announcements = io.BytesIO()
            pk.dump(self._announcements, announcements)
            announcements.seek(0)
            await self.db.dumpPickleData('announcements.pk', announcements.read())

    async def loadQueues(self, launched: bool):
        if not launched:
            return
        pickleData = await self.db.loadPickleData('taskQueues.pk')
        if pickleData is not None:
            Buf = io.BytesIO(pickleData)
            Buf.seek(0)
            self._taskQueues = pk.load(Buf)

        if self._taskQueues:
            for taskQ in self._taskQueues:
                if taskQ.isRunning:
                    self._runningTasks[taskQ.guild] = asyncio.create_task(self.runTasks(taskQ.guild, 'Restarting '
                                                                                                     'tasks...'))

        pickleData = await self.db.loadPickleData('announcements.pk')
        if pickleData is not None:
            Buf = io.BytesIO(pickleData)
            Buf.seek(0)
            self._announcements = pk.load(Buf)
        if self._announcements:
            for announcement in self._announcements:
                self._announcements[int(announcement)] = asyncio.create_task(self.runAnnouncement(announcement))


async def setup(bot):
    await bot.add_cog(Schedule(bot=bot))
