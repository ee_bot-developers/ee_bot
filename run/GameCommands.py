# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import discord
from discord import app_commands
from discord.ext import commands
from discord.ext import tasks
from typing import Literal, Optional, Dict, Union
from EE_Bot_DB import EE_DB, cogEnum
from quizLogic import QuizLogic
import pickle as pk
from copy import deepcopy
import io


class Games(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db
        self._changed: bool = False
        self._games: Optional[Dict[int, Union[QuizLogic]]] = None

    async def loadGames(self):
        pickleData = await self.db.loadPickleData('ongoingGames.pickle')
        if pickleData is None:
            self._games = {}
            return
        Buf = io.BytesIO(pickleData)
        Buf.seek(0)
        self._games = pk.load(Buf)

    # Function that can be called anytime to save the active games.
    # This should only be called at the end of execution
    async def saveGames(self):
        if self._changed:
            # Save the games
            Buf = io.BytesIO()
            pk.dump(self._games, Buf)
            Buf.seek(0)
            await self.db.dumpPickleData('ongoingGames.pickle', Buf.read())

    @tasks.loop(minutes=5)
    async def backupGames(self):
        await self.saveGames()
        self._changed = False

    async def cog_load(self) -> None:
        await self.loadGames()
        self.backupGames.start()
        print('Games Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        self.backupGames.cancel()
        await self.saveGames()
        print('Games Cog Unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction) -> bool:
        if ctx.guild is None:
            return True
        enabled = await self.db.checkCog(ctx.guild.id, cogEnum.Games)
        if not enabled:
            await ctx.response.send_message('This command has been disabled.')
        return enabled

    # Command to create a quiz
    @app_commands.command(name='quiz', description='Start a quiz')
    @app_commands.describe(difficulty='Choose the difficulty')
    async def quiz(self,
                   ctx: discord.Interaction,
                   difficulty: Literal['very easy', 'easy', 'normal', 'hard', 'very hard']):
        # Check if in server or DM
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()

        # check if user has game ongoing...
        if ctx.user.id in self._games.keys():
            await ctx.followup.send('Cannot create a new game! Please finish or quit current game.')
            return

        # Check difficulty and create new QuizLogic object and append it to list
        if difficulty == 'normal' or difficulty == 'hard' or difficulty == 'very hard':
            await ctx.followup.send(f'{difficulty} difficulty is not implemented yet')
            return
        newQuiz = QuizLogic(difficulty, ctx.user.id)
        self._games[ctx.user.id] = deepcopy(newQuiz)
        await ctx.followup.send(f'Starting quiz on {difficulty} difficulty')
        self._changed = True

        # Get the first question, file, and index
        question, file, index = self._games[ctx.user.id].nextQuestion()

        # Send question
        if file is not None:
            # Get file extension
            extension = file.split('.')[-1]
            with open(file, 'rb') as f:
                await ctx.followup.send(f'<@{ctx.user.id}> Question {index}: {question}',
                                        file=discord.File(f, f'Question{index}.{extension}'))
        else:
            await ctx.followup.send(f'<@{ctx.user.id}> Question {index}: {question}')

    # Answer command
    @app_commands.command(name='answer', description='Your answer to the game.')
    @app_commands.describe(ans="Your answer to the question. Don't forget units!")
    async def answer(self,
                     ctx: discord.Interaction,
                     ans: str):
        # Check if in server or DM
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()

        if ctx.user.id in self._games.keys():
            # Register answer
            accepted, response = self._games[ctx.user.id].ans(ans)
            if not accepted:
                await ctx.followup.send(f'<@{ctx.user.id}> {response}')
                return
            # Fetch next question
            self._changed = True
            question, file, i = self._games[ctx.user.id].nextQuestion()

            # Send next question
            if file is not None:
                # Get file extension
                extension = file.split('.')[-1]
                with open(file, 'rb') as f:
                    try:
                        await ctx.followup.send(f'<@{ctx.user.id}> Question {i}/5: {question}',
                                                file=discord.File(f, f'Question{i}.{extension}'))
                    except Exception as err:
                        print(err)
                        return
            elif question is not None:
                await ctx.followup.send(f'<@{ctx.user.id}> Question {i}/5: {question}')

            # Check results
            if self._games[ctx.user.id].showResults():
                # Get score and answers
                score, fig = self._games[ctx.user.id].render_table()

                await ctx.followup.send(f'<@{ctx.user.id}> Your score was {score}/5')

                with io.BytesIO() as buf:
                    fig.savefig(buf, format='png')
                    buf.seek(0)
                    await ctx.followup.send(file=discord.File(buf, 'results.png'))
                del self._games[ctx.user.id]
                fig.close()
        else:
            await ctx.followup.send(f'<@{ctx.user.id}> Oops, it looks like you do not have any '
                                    f'ongoing games.')
        return

    @app_commands.command(name='quit', description='Quit your current game.')
    async def quit(self,
                   ctx: discord.Interaction):
        # Check if in server or DM
        if not await self._isEnabled(ctx):
            return

        if ctx.user.id in self._games.keys():
            # Set modified flag
            self._changed = True
            # delete game with corresponding ID
            del self._games[ctx.user.id]
            await ctx.response.send_message(f'<@{ctx.user.id}> You have quit your ongoing game.')
        else:
            await ctx.response.send_message(f'<@{ctx.user.id}> You do not have an ongoing game.')
        return

    @app_commands.command(name='game', description='Repeats prompt of ongoing game.')
    async def game(self, ctx: discord.Interaction):
        # Check if in server or DM
        if not await self._isEnabled(ctx):
            return

        if ctx.user.id in self._games.keys():
            # Get current status of game
            question, file, i = self._games[ctx.user.id].nextQuestion()

            # Display status of game
            if file is not None:
                extension = file.split('.')[-1]
                with open(file, 'rb') as f:
                    await ctx.response.send_message(f'<@{ctx.user.id}> Question {i}/5: {question}',
                                                    file=discord.File(f, f'Question{i}.{extension}'))
            else:
                await ctx.response.send_message(f'<@{ctx.user.id}> Question {i}/5: {question}')
        else:
            await ctx.response.send_message(f'<@{ctx.user.id}> Oops, it looks like you do not have an ongoing game.')
        return


async def setup(bot):
    await bot.add_cog(Games(bot=bot))
