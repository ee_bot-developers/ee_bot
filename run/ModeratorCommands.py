# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.ext import commands
from discord import app_commands, File
from Modals.warningAndBanModal import warningAndBanModal
from Modals.reportView import reportView
from Modals.spamView import spamView
from typing import Literal, Optional, Dict
from EE_Bot_DB import EE_DB, cogEnum, channelType, updatePollAction
from datetime import datetime, timezone, timedelta
import emoji as Emoji
import asyncio
import csv
import io
import re
import globals as GL

# Initialize and get globals
GL.InitLaunch()
originGuild = GL.originGuild


@app_commands.guild_only
class Moderator(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db
        self.csvHeader = ['guildID', 'channel', 'userID', 'userName', 'adminID', 'adminName', 'ruleViolated', 'reason',
                          'date', 'url']
        self._termDict = {
            1: 'Spring',
            2: 'Spring',
            3: 'Spring',
            4: 'Spring',
            5: 'Spring',
            6: 'Summer',
            7: 'Summer',
            8: 'Summer',
            9: 'Fall',
            10: 'Fall',
            11: 'Fall',
            12: 'Fall'
        }
        self.warn_cmd = app_commands.ContextMenu(
            name='warn',
            callback=self.warn
        )
        self.ban_cmd = app_commands.ContextMenu(
            name='ban',
            callback=self.ban
        )
        self.report_cmd = app_commands.ContextMenu(
            name='report_message',
            callback=self.report_message
        )
        self.ban_cmd2 = app_commands.ContextMenu(
            name='ban',
            callback=self.ban_message
        )
        self.bot.tree.add_command(self.warn_cmd)
        self.bot.tree.add_command(self.ban_cmd)
        self.bot.tree.add_command(self.report_cmd)
        self.bot.tree.add_command(self.ban_cmd2)
        self.reported_messages: Dict[str, reportView] = {}
        return

    async def cog_load(self) -> None:
        print('Moderator Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        self.bot.tree.remove_command(self.warn_cmd.name, type=self.warn_cmd.type)
        self.bot.tree.remove_command(self.ban_cmd.name, type=self.ban_cmd.type)
        self.bot.tree.remove_command(self.report_cmd.name, type=self.report_cmd.type)
        for view in self.reported_messages.values():
            await view.terminate()
        print('Moderator Cog Unloaded!')
        return

    async def _generate_spam_report(self, message: discord.Message):
        admin_channel = message.guild.get_channel(await self.bot.db.getLoggingChannel(message.guild.id,
                                                                                      channelType.admin))
        embed = discord.Embed(title='Detected Spam Messages', colour=GL.embedColor)
        if message.content:
            embed.description = message.content

        embed.set_author(name=message.author.display_name, icon_url=message.author.display_avatar.url)
        embed.timestamp = message.created_at

        view = spamView(message, embed, self.warn, self.ban)
        view.spam_notification = await admin_channel.send(embed=embed, view=view)
        return

    @commands.Cog.listener(name='on_message')
    async def on_message(self, message: discord.Message):
        try:
            bucket = self.bot.anti_spam[message.guild.id].get_bucket(message)
        except KeyError:
            return
        spamming = bucket.update_rate_limit()
        if spamming:
            # Timeout user to give admins opportunity to evaluate their activity
            await message.author.timeout(timedelta(hours=6))
            await self._generate_spam_report(message)
            return
        return

    async def _checkPermissions(self, ctx: discord.Interaction, adminOnly: bool = True) -> bool:
        if not await self.db.checkCog(ctx.guild_id, cogEnum.Moderator):
            await ctx.response.send_message('This command has been disabled!', ephemeral=True)
            return False
        if not ctx.user.guild_permissions.administrator and adminOnly:
            await ctx.response.send_message('You need admin permissions to do this.', ephemeral=True)
            return False
        return True

    async def runArchive(self, ctx: discord.Interaction):
        LOG = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        logExists = True
        if LOG is None:
            LOG = ctx.channel
            logExists = False

        channel = ctx.channel
        guild = ctx.guild
        prefix = await self.db.getChannelPrefix(ctx.guild_id)

        # Clone new channel first
        newChannel = await channel.clone()
        await newChannel.edit(position=channel.position)
        # Get name for archive channel
        archiveChannelName = f'{channel.name}-archive'
        # Check if there is an archive category, if not, create one...
        category = discord.utils.get(guild.categories, name='Archive')

        if category is None:
            category = await guild.create_category('Archive')
            await category.set_permissions(guild.default_role, add_reactions=False, attach_files=False,
                                           manage_channels=False, manage_permissions=False, read_message_history=True,
                                           read_messages=False, send_messages=False, send_tts_messages=False,
                                           use_application_commands=False)

        # Get archive channel
        archiveChannel = discord.utils.get(category.text_channels, name=archiveChannelName)
        if archiveChannel is None:
            # Calculate position of new channel
            position = 0
            end = True
            channels = category.text_channels
            try:
                channel_code = int(channel.name.lstrip(prefix))
                for x in channels:
                    try:
                        if int(x.name.lstrip(prefix).rstrip('-archive')) > channel_code:
                            position = x.position
                            end = False
                            break
                    except ValueError:
                        await LOG.send(f'<#{x.id}> channel name does not conform to naming standards of this server!')
            except ValueError:
                await LOG.send('Non-standard prefix detected. Please manually move the channel to the desired '
                               'position within the archive category')
            archiveChannel = await category.create_text_channel(archiveChannelName)

            if not end:
                await archiveChannel.edit(position=position)

        # Get roles of channel being archived and update their permissions
        roles = channel.changed_roles
        overwrite = discord.PermissionOverwrite(
            send_messages=False,
            read_messages=False,
            read_message_history=False
        )
        for x in roles:
            await channel.set_permissions(x, overwrite=overwrite)

        # Get roles from archive channel, save their permissions for later, and update them
        roles = archiveChannel.changed_roles
        role_permissions = []
        for x in roles:
            role_permissions.append([x, archiveChannel.overwrites_for(x)])
            await archiveChannel.set_permissions(x, overwrite=overwrite)

        # Create archive header
        date = datetime.today()
        month = date.month
        year = date.year
        term = f'{self._termDict[month]} {year}'

        await archiveChannel.send(f'__**Start of the {term} semester**__')

        msgRef = {}
        MsgRef = None
        refDeleted = False
        newMsg = None

        async for message in channel.history(limit=None, oldest_first=True):
            embed = discord.Embed(timestamp=message.created_at, color=GL.embedColor)
            embed.set_author(name=message.author.display_name, icon_url=message.author.display_avatar.url)

            if message.reference is not None:
                if message.type == discord.MessageType.pins_add:
                    try:
                        MsgRef = msgRef[message.reference.message_id]
                        await MsgRef.pin()
                    except KeyError:
                        pass
                    continue
                elif message.reference.guild_id == message.guild.id and \
                    (message.reference.channel_id == message.channel.id):
                    try:
                        MsgRef = msgRef[message.reference.message_id]
                    except KeyError:
                        refDeleted = True
                else:
                    MsgRef = message.reference

            files = []
            if message.attachments:
                for f in message.attachments:
                    file = await f.to_file(spoiler=f.is_spoiler())
                    files.append(file)

            if message.content:
                embed.description = message.content
            if refDeleted:
                MsgRef = await archiveChannel.send('Placeholder, To be deleted')

            try:
                newMsg = await archiveChannel.send(embed=embed, files=files, reference=MsgRef,
                                                   stickers=message.stickers)
            except discord.HTTPException as err:
                newMsg = await archiveChannel.send(content=str(err), embed=embed, reference=MsgRef)

            if refDeleted:
                await MsgRef.delete()

            msgRef[message.id] = newMsg
            MsgRef = None
            refDeleted = False

            await asyncio.sleep(3)

        await newMsg.delete()

        await archiveChannel.send(f'__**End of the {term} semester**__')

        # Reset permissions from earlier
        for x in role_permissions:
            await archiveChannel.set_permissions(x[0], overwrite=x[1])

        # Delete the old channel
        await channel.delete()

        if logExists:
            await LOG.send(f'Finished archiving {channel.name}')

        # Release the archive flag
        await self.bot.ArchiveTasks(guild.id, False)
        return

    @app_commands.command(description='Run this command inside a class channel to archive it.')
    async def archive(self, ctx: discord.Interaction):
        if not await self._checkPermissions(ctx):
            return
        # Check if channel is a class channel
        # TODO: See create_channel()
        if ctx.channel.category.name != 'Classes':
            await ctx.response.send_message('This action can only be performed in class channels.')
            return
        if await self.bot.ArchiveTasks(ctx.guild_id, True):
            # Create task
            asyncio.create_task(self.runArchive(ctx))
            await ctx.response.send_message(f'{ctx.channel.name} is now getting archived.')
        else:
            await ctx.response.send_message('Please wait until current archive task is done')
        return

    # Command to create a new channel
    @app_commands.command(description='Create a new class channel')
    @app_commands.describe(
        channel='The class number'
    )
    async def create_channel(self,
                             ctx: discord.Interaction,
                             channel: int):
        if not await self._checkPermissions(ctx, False):
            return
        log = await self.db.getLoggingChannel(ctx.guild_id, channelType.log)
        logSet = False
        if log != 0:
            log = ctx.guild.get_channel(log)
            logSet = True
        # Convert integer to string
        channel = str(channel)

        # Attempt to add new entry to database
        level, _ = await self.db.checkUserLevel(ctx.user.id, ctx.guild_id)
        roles = [int(role.name) for role in ctx.guild.roles if role.name.isdigit()]

        reqLevel = await self.db.getCreateChannelRep(ctx.guild_id)
        if reqLevel is None:
            reqLevel = float('inf')

        if channel in roles:
            await ctx.response.send_message("Channel already exists! If you can't see it, please contact the server "
                                            "staff.")
            return
        elif level < reqLevel and not ctx.user.guild_permissions.administrator:  # Not enough reputation
            await ctx.response.send_message('Sorry, you are not a high enough level to create a new channel. Please '
                                            'contact the server staff for channel creation.')
            return
        # Respond to command
        await ctx.response.send_message('Creating new channel...')
        prefix = await self.db.getChannelPrefix(ctx.guild_id)
        name = 'Classes'  # TODO: Make this configurable
        # Get category from guild
        category = discord.utils.get(ctx.guild.categories, name=name)

        if category is None:
            category = await ctx.guild.create_category(name=name)
            for x in ctx.guild.roles:
                await category.set_permissions(x, view_channel=False)

        textCategory = category

        # Calculate position of new channel
        position = 0
        end = True
        channels = category.text_channels
        for x in channels:
            try:
                if int(x.name.lstrip(prefix)) > int(channel):
                    position = x.position
                    end = False
                    break
            except Exception as err:
                print(err)
                if logSet:
                    await log.send(f'<#{x.id}> channel name does not conform to naming standards of this server!')

        # Create new channel
        newChannel = await ctx.guild.create_text_channel(f'{prefix}{channel}', category=textCategory)
        newTextChannel = newChannel

        # Set position of new channel
        if not end:
            await newTextChannel.edit(position=position)

        # Get voice channel category
        name = 'Voice Channels'  # TODO: Make this configurable
        category = discord.utils.get(ctx.guild.categories, name=name)

        if category is None:
            category = await ctx.guild.create_category(name=name)
            for x in ctx.guild.roles:
                await category.set_permissions(x, view_channel=False)

        prefix = prefix.upper()
        # Calculate position
        channels = category.voice_channels
        if not end:
            for x in channels:
                try:
                    if str(x)[:len(prefix)] != prefix:
                        continue
                    elif int(str(x).lstrip(prefix)) > int(channel):
                        position = x.position
                        break
                except Exception as err:
                    print(err)

        # Create new voice channel
        newChannel = await ctx.guild.create_voice_channel(f'{prefix}{channel}', category=category)
        newVoiceChannel = newChannel

        # Set position of new channel
        if not end:
            await newChannel.edit(position=position)

        # Get role color
        RGB = await self.db.getRoleColor(ctx.guild_id)
        if RGB is None:
            color = discord.Colour.dark_teal()
        else:
            color = discord.Colour.from_rgb(RGB[0], RGB[1], RGB[2])

        # Create new role
        newRole = await ctx.guild.create_role(name=channel, colour=color, mentionable=True)

        for x in ctx.guild.roles:
            if not x.permissions.administrator:
                await newTextChannel.set_permissions(x, view_channel=False)
                await newVoiceChannel.set_permissions(x, view_channel=False)

        await newTextChannel.set_permissions(newRole, view_channel=True)
        await newVoiceChannel.set_permissions(newRole, view_channel=True)
        await newVoiceChannel.set_permissions(ctx.guild.default_role, view_channel=False)
        await textCategory.set_permissions(newRole, view_channel=True)

        await ctx.edit_original_response(content=f'Created following channels and role:\n<#{newTextChannel.id}>\n'
                                                 f'<#{newVoiceChannel.id}>\n<@&{newRole.id}>')
        return

    @app_commands.command(description='Delete a class channel')
    @app_commands.describe(
        channel='The channel that will get deleted'
    )
    async def delete_channel(self,
                             ctx: discord.Interaction,
                             channel: discord.TextChannel):
        prefix = await self.db.getChannelPrefix(ctx.guild_id)
        if not channel.name.lstrip(prefix).isdigit():
            await ctx.response.send_message(f'Invalid channel input. Expecting a channel name whose name follows this '
                                            f'format: {prefix}###...', ephemeral=True)
            return
        if not await self._checkPermissions(ctx):
            return

        # Get channels and roles
        text_channels = ctx.guild.text_channels
        voice_channels = ctx.guild.voice_channels
        roles = ctx.guild.roles
        channel_name = channel.name.lower()

        channel = discord.utils.get(text_channels, name=channel_name)
        role_name = channel.name.replace(prefix, '')
        if channel is None:
            await ctx.response.send_message(f"{channel_name} not found in server's text channels", ephemeral=True)
            return
        await channel.delete()

        channel_name = channel.name.upper()

        channel = discord.utils.get(voice_channels, name=channel_name)
        if channel is None:
            await ctx.response.send_message(f"{channel_name} not found in server's voice channels", ephemeral=True)
            return
        await channel.delete()

        role = discord.utils.get(roles, name=role_name)
        if role is None:
            await ctx.response.send_message(f"{role_name} not found in server's roles", ephemeral=True)
            return
        await role.delete()
        await ctx.response.send_message('Channel has been deleted.', ephemeral=True)
        return

    # command that allows users assign themselves roles
    @app_commands.command(description='Assign yourself a class role. If the role is non-standard, please refer to the '
                                      'role poll')
    @app_commands.describe(
        role='The course number'
    )
    async def assign_role(self,
                          ctx: discord.Interaction,
                          role: int):
        if not await self._checkPermissions(ctx, False):
            return
        selectedRole = discord.utils.get(ctx.guild.roles, name=str(role))
        log_id = await self.db.getLoggingChannel(ctx.guild_id, channelType.log)
        if selectedRole is None:
            if log_id != 0:
                await ctx.guild.get_channel(log_id).send(f'<@{ctx.user.id}> tried assigning themselves a '
                                                         f'non-existent role ({role})')
            await ctx.response.send_message(f'Role does not exist or you cannot assign yourself the {role} role!')
            return
        await ctx.user.add_roles(selectedRole)
        await ctx.response.send_message(f'You have been assigned the {role} role')
        if log_id != 0:
            await ctx.guild.get_channel(log_id).send(f'<@{ctx.user.id}> invoked assign_role to assign themselves the '
                                                     f'{role} role')
        return

    @app_commands.command(description='Remove a class role from yourself. If the role is non-standard, please refer '
                                      'to the role poll.')
    @app_commands.describe(
        role='The course number'
    )
    async def remove_role(self,
                          ctx: discord.Interaction,
                          role: int):
        if not await self._checkPermissions(ctx, False):
            return
        selectedRole = discord.utils.get(ctx.user.roles, name=str(role))
        log_id = await self.db.getLoggingChannel(ctx.guild_id, channelType.log)
        if selectedRole is None:
            if log_id != 0:
                await ctx.guild.get_channel(log_id).send(f'Failed to remove the {role} role from <@{ctx.user.id}>')
            await ctx.response.send_message(f'Role does not exist or you cannot remove the {role} role from yourself!')
            return
        await ctx.user.remove_roles(selectedRole)
        await ctx.response.send_message('Successfully removed role!')
        return

    @app_commands.command(description='Create a poll')
    @app_commands.describe(
        for_roles='True if for roles, False for other polls',
        emojis='Emojis for the poll. There must be spaces between emojis.',
        arguments='The arguments for the poll',
        custom_message='The custom message for the poll. If it is a role poll, this parameter will be ignored.'
    )
    async def create_poll(self,
                          ctx: discord.Interaction,
                          for_roles: bool,
                          emojis: str,
                          arguments: str,
                          custom_message: str = ''):
        if not await self._checkPermissions(ctx, False):
            return
        # Parse emojis
        emojis = Emoji.demojize(emojis)
        emojis = re.sub(r'([:>])([:<])', r'\1 \2', emojis)
        emojis = emojis.split(' ')
        # Remove any duplicate emojis
        emojis = [i for n, i in enumerate(emojis) if i not in emojis[:n]]
        # Check how many emojis there are
        if len(emojis) > 20:
            await ctx.response.send_message('Cannot have more than 20 arguments!')
            return
        # Parse arguments
        args = next(csv.reader(io.StringIO(arguments), delimiter=' '))
        if len(args) != len(emojis):
            await ctx.response.send_message(f'Error: There are {len(args)} arguments and {len(emojis)} emojis.')
            return
        admin = ctx.user.guild_permissions.administrator
        Poll = {}
        Roles = []
        if for_roles:
            if not admin:
                await ctx.response.send_message('You do not have permission to create a poll for roles!')
                return
            channel = await self.db.getLoggingChannel(ctx.guild_id, channelType.role)
            channel = ctx.guild.get_channel(channel)
            if channel is None:
                await ctx.response.send_message('Error: Please configure your server by using `/configure`')
            msg = "React with the following Emojis to assign yourself to roles:\n\n"
            roles = ctx.guild.roles
            for x in args:
                role = discord.utils.get(roles, name=x)
                if role is None:
                    await ctx.response.send_message(f'Error: The {x} role does not exist!')
                    return
                elif role in Roles:
                    await ctx.response.send_message(f'Error: The {x} role is listed 2 or more times!')
                    return
                else:
                    Roles.append(role)
        else:
            channel = ctx.channel
            msg = f'**{ctx.user.name}**\n\n'
            if custom_message != '':
                msg += f'{custom_message}\n\n'
            else:
                msg += 'React with the following: \n\n'
        for i, x in enumerate(emojis):
            msg += f'{Emoji.emojize(x)} :   `{args[i]}`\n\n'
        await ctx.response.send_message('Sending poll...')
        # Post message
        Msg = await channel.send(msg)
        # Add reactions
        for i, x in enumerate(emojis):
            await Msg.add_reaction(Emoji.emojize(x))
            if for_roles:
                Poll[x] = Roles[i].id
        if for_roles:
            # Save matches to Database
            await self.db.createPoll(Msg.id, ctx.guild_id, Poll)
        return

    # Command to delete a poll
    @app_commands.command(description='Delete a poll by providing a url')
    @app_commands.describe(
        url="The url of the poll that you're trying to delete."
    )
    async def delete_poll(self,
                          ctx: discord.Interaction,
                          url: str):
        if not await self._checkPermissions(ctx):
            return
        channel = await self.db.getLoggingChannel(ctx.guild_id, channelType.role)
        channel = ctx.guild.get_channel(channel)
        if channel is None:
            await ctx.response.send_message('ERROR: Unable to find poll channel! Please configure your server first '
                                            'by running `/configure`.')
            return
        msgID = int(url.split('/')[-1])
        msg = await channel.fetch_message(msgID)
        if msg is None:
            await ctx.response.send_message('Could not find message. Please ensure that it exists and it is for '
                                            'assigning roles.')
            return
        await ctx.response.send_message('Poll deleted')
        await self.db.deletePoll(msgID, ctx.guild_id)
        await msg.delete()
        return

    @app_commands.command(description='Edit a poll given the url')
    @app_commands.describe(
        url='The url of the poll that gets edited',
        mode='Add or remove pair?',
        role='The role name',
        emoji='The emoji used for the reaction. Ignored for removal.'
    )
    async def edit_poll(self,
                        ctx: discord.Interaction,
                        url: str,
                        mode: Literal['add', 'remove'],
                        role: str,
                        emoji: Optional[str] = ''):
        if not await self._checkPermissions(ctx):
            return
        role_channel = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.role))
        Role = discord.utils.get(ctx.guild.roles, name=role)
        if Role is None:
            await ctx.response.send_message('Given role does not exist')
            return
        if role_channel is None:
            await ctx.response.send_message('Please run `/configure` before using this command.')
            return
        msgID = int(url.split('/')[-1])
        poll = await self.db.retrievePoll(msgID, ctx.guild_id)
        if not poll:
            await ctx.response.send_message('Poll does not exist!')
            return

        if mode == 'add':
            # Check if emoji was passed in
            if not emoji:
                await ctx.response.send_message('Error: No emoji passed in!')
                return
            # Check if poll is at max length
            if len(poll) == 20:
                await ctx.response.send_message('Cannot add new pair! There are already 20 pairs.')
                return

            # Check for duplicates
            if Role.id in poll.values() or Emoji.demojize(emoji) is poll.keys():
                await ctx.response.send_message('Cannot duplicate role or emoji')
                return
            # Add new pair
            await self.db.updatePoll(msgID, ctx.guild_id, {Emoji.demojize(emoji): Role.id}, updatePollAction.ADD)
            # Fetch the message
            msg = await role_channel.fetch_message(msgID)
            content = f'{msg.content}\n\n{emoji} :   `{role}`\n\n'
            await msg.edit(content=content)
            # Add new reaction
            await msg.add_reaction(emoji)
            await ctx.response.send_message('Done')
        else:
            try:
                emoji = list(poll.keys())[list(poll.values()).index(Role.id)]
            except ValueError:
                await ctx.response.send_message('Please check if the role entered is correct')
                return
            delPoll = False
            if len(poll) == 1:
                delPoll = True

            # Delete pair from database
            await self.db.updatePoll(msgID, ctx.guild_id, emoji, updatePollAction.REMOVE)
            # Fetch message and edit it
            msg = await role_channel.fetch_message(msgID)
            if delPoll:
                await msg.delete()
                return
            content = msg.content
            content = content.replace(f'\n\n{Emoji.emojize(emoji)} :   `{role}`', '')
            await msg.edit(content=content)
            # Remove all reactions to that entry
            await msg.clear_reaction(Emoji.emojize(emoji))
            # Report that operation finished
            await ctx.response.send_message('Done')
        return

    @commands.command()
    async def fetch_db(self, ctx: commands.Context):
        # TODO: Figure out how to replace this command
        if ctx.author.id == self.MASTER:
            # Create UTC timestamp
            timestamp = datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()
            with open(self.database, 'rb') as f:
                await ctx.author.send(file=File(f, f'EE_Bot{timestamp}.db'))
        return

    @commands.command()
    async def fetch_logs(self, ctx: commands.Context):
        if ctx.author.id == self.MASTER:
            # Create UTC timestamp
            timestamp = datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()
            with open('./ee_bot.log', 'rb') as f:
                await ctx.author.send(file=File(f, f'ee_bot{timestamp}.log'))
            with open('./errors.log', 'rb') as f:
                await ctx.author.send(file=File(f, f'errors{timestamp}.log'))
        return

    async def runBan(self,
                     ctx: discord.Interaction,
                     member: discord.Member,
                     ruleViolated: int,
                     reason: str):
        if (member.id == self.MASTER) or member.id == self.bot.application_id:
            with open('./images/EasterEggs/banVideo.mp4', 'rb') as f:
                await ctx.followup.send(file=File(f, 'ban.mp4'))
            return
        elif member.id == ctx.user.id:  # Keep people from banning themselves
            await ctx.followup.send('You cannot ban yourself!')
            return
        elif member.guild_permissions.administrator:
            await ctx.followup.send('You cannot ban a administrator! Ask your server owner for a manual ban!')
            return
        msg = await self.db.getBanMsg(ctx.guild_id)
        await member.send(f'You have been banned for breaking rule {ruleViolated}. Reason: {reason}. {msg}')
        shame = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.shame))
        if shame is not None:
            await shame.send(f'<@&{ctx.guild.default_role.id}> <@{member.id}> has been banned for {reason}. Let this '
                             f'serve as a reminder not to break the server rules.')
        monitor = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.admin))
        if monitor is not None:
            embed = discord.Embed(title='Banned User', description=f'Found in violation of rule {ruleViolated}. '
                                                                   f'Reason: {reason}', color=GL.embedColor)
            embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
            await monitor.send(embed=embed)
        # TODO: Make delete message days configurable
        await member.ban(reason=reason)
        return

    async def ban(self,
                  ctx: discord.Interaction,
                  member: discord.Member):
        if not await self._checkPermissions(ctx):
            return
        modal = warningAndBanModal('Ban Form', f'Banning <@{member.id}>')
        await ctx.response.send_modal(modal)
        await modal.wait()
        await self.runBan(ctx, member, int(modal.ruleViolated), modal.reason)
        return

    async def ban_message(self,
                          ctx: discord.Interaction,
                          msg: discord.Message):
        if not await self._checkPermissions(ctx):
            return
        modal = warningAndBanModal('Ban Form', f'Banning <@{msg.author.id}>')
        await ctx.response.send_modal(modal)
        await modal.wait()
        await self.runBan(ctx, msg.author, int(modal.ruleViolated), modal.reason)
        return

    async def warn(self,
                   ctx: discord.Interaction,
                   msg: discord.Message):
        if not await self._checkPermissions(ctx):
            return
        modal = warningAndBanModal('Warning Form', 'Sending the author a warning...')
        await ctx.response.send_modal(modal)
        await modal.wait()
        warnings = await self.db.addWarning(ctx.guild_id, msg.channel.name, msg.author.id, msg.author.name,
                                            ctx.user.id, ctx.user.name, int(modal.ruleViolated), modal.reason)
        monitoring = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.admin))
        if monitoring is not None:
            embed = discord.Embed(title='Warned Author', color=GL.embedColor)
            if msg.content:
                embed.description = msg.content
            embed.set_author(name=msg.author.display_name, icon_url=msg.author.display_avatar.url)
            embed.timestamp = msg.created_at

            url_view = discord.ui.View()
            url_view.add_item(discord.ui.Button(label='Go to Channel', style=discord.ButtonStyle.url,
                                                url=msg.channel.jump_url))
            await monitoring.send(embed=embed, view=url_view)
        await msg.delete()
        if warnings > 5:
            await self.runBan(ctx, msg.author, int(modal.ruleViolated), modal.reason)
            punishment_message = f'<@{msg.author.id}> has been banned for acquiring too many warnings'
        else:
            if warnings == 5:
                await msg.author.send(f'You have received warning #{warnings} for {modal.reason}. This is your final '
                                      f'warning before a ban.')
            else:
                await msg.author.send(f'You have received warning #{warnings} for {modal.reason}. You have '
                                      f'{5 - warnings} left.')
            if not msg.author.is_timed_out():
                timeout = int((193.54 * (warnings ** 4)) - (1721.2 * (warnings ** 3)) + (5511.5 * (warnings ** 2)) -
                              (7383.7 * warnings) + 3405 - ((warnings == 5) * 5))
                try:
                    await msg.author.timeout(timedelta(minutes=timeout), reason=modal.reason)
                except discord.HTTPException:
                    pass
            punishment_message = f'<@{msg.author.id}> has been warned. The timeout punishment has been applied.'
            rules = await self.db.getPoliRules(ctx.guild_id)
            if modal.ruleViolated in rules:
                await self.db.updatePoliBan(ctx.guild_id, ctx.user.id, True)
                # Remove politics role from user
                role = discord.utils.get(msg.author.roles, name='politics')
                if role is not None:
                    await msg.author.remove_roles(role)
        embed = discord.Embed(description=punishment_message, color=GL.embedColor)
        await msg.channel.send(embed=embed)

    @app_commands.command(description='Remove 1 warning from user')
    @app_commands.describe(
        user='User who gets a warning removed from their record.'
    )
    async def remove_warning(self,
                             ctx: discord.Interaction,
                             user: discord.Member):
        if not await self._checkPermissions(ctx):
            return
        warnings = await self.db.removeWarning(ctx.guild_id, user.id)
        await ctx.response.send_message(f'<@{user.id}> now has {warnings} warnings', ephemeral=True)
        return

    @app_commands.command(description='Remove all warnings from a user.')
    async def clear_warnings(self,
                             ctx: discord.Interaction,
                             user: discord.Member):
        if not await self._checkPermissions(ctx):
            return
        await self.db.clearWarnings(ctx.guild_id, user.id)
        await ctx.response.send_message(f'Cleared warnings for <@{user.id}>', ephemeral=True)
        return

    # Function that runs clear roles command as a task
    async def runClearRoles(self,
                            ctx: discord.Interaction,
                            role: discord.Role) -> None:
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        if log is not None:
            await log.send('Starting class role purge...')

        # Check if role was given
        if role is not None:
            members = role.members
            for member in members:
                await member.remove_roles(role)
        else:
            members = ctx.guild.members
            # Purge all members from roles whose name is numeric
            for member in members:
                roles = member.roles
                roles = tuple([x for x in roles if str(x).isdigit()])
                if roles:
                    await member.remove_roles(*roles, atomic=True)
        # Finished purge roles
        await self.bot.PurgeTasks(ctx.guild_id, False)
        if log is not None:
            await log.send('Finished role purge.')
        await ctx.followup.send('Finished role purge', ephemeral=True)
        return

    @app_commands.command(description='Clears roles whose name is only numeric from everyone')
    @app_commands.describe(
        role='Specific role you want to clear'
    )
    async def clear_roles(self,
                          ctx: discord.Interaction,
                          role: Optional[discord.Role]):
        if not await self._checkPermissions(ctx):
            return
        # Check flag
        if not await self.bot.PurgeTasks(ctx.guild_id, True):
            await ctx.response.send_message('Please wait until current role purge is finished.')
            return
        await ctx.response.defer(ephemeral=True)
        asyncio.create_task(self.runClearRoles(ctx, role))
        return

    @app_commands.command(description='Adds user to a watchlist')
    @app_commands.describe(
        user='The user to watch',
        watch_status="Watch or don't watch user"
    )
    async def watch(self,
                    ctx: discord.Interaction,
                    user: discord.Member,
                    watch_status: Literal['on', 'off']):
        if not await self._checkPermissions(ctx):
            return
        if user.bot:
            await ctx.response.send_message('Cannot watch bot', ephemeral=True)
            return
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        await self.db.updateMonitor(ctx.guild_id, user.id, watch_status == 'on')
        # Update override flag
        await self.db.updateOverride(ctx.guild_id, user.id, True)
        # TODO: Fix database so this doesn't need to be called twice
        await self.db.updateMonitor(ctx.guild_id, user.id, watch_status == 'on')
        if log is not None:
            await log.send(f'Monitoring was set up for <@{user.id}>')
        await ctx.response.send_message('Status updated', ephemeral=True)
        return

    @app_commands.command(description='Disables the watchlist override after invoking the watch command')
    @app_commands.describe(
        user='This is self-explanatory'
    )
    async def disable_watchlist_override(self,
                                         ctx: discord.Interaction,
                                         user: discord.Member):
        if not await self._checkPermissions(ctx):
            return
        if user.bot:
            await ctx.response.send_message('Cannot update flags for a bot', ephemeral=True)
            return
        await self.db.updateOverride(ctx.guild_id, user.id, False)
        await ctx.response.send_message('Override disabled')
        return

    @app_commands.command(description='Lifts a channel specific ban from a member')
    @app_commands.describe(
        user='The member that is banned from the politics channels'
    )
    async def lift_poli_ban(self,
                            ctx: discord.Interaction,
                            user: discord.Member):
        if not await self._checkPermissions(ctx):
            return
        if user.bot:
            await ctx.response.send_message('Cannot update flags for a bot account', ephemeral=True)
            return
        await self.db.updatePoliBan(ctx.guild_id, user.id, False)
        await ctx.response.send_message('User flag updated')
        return

    @app_commands.command(description='Outputs csv file of warning.')
    @app_commands.describe(
        args='The arguments provided. See /help for more information'
    )
    async def fetch_warnings(self,
                             ctx: discord.Interaction,
                             args: Optional[str]):
        if not await self._checkPermissions(ctx):
            return
        if args is None:
            args = ''
        args = tuple(filter(lambda a: a != '', args.split(' ')))
        search = {}
        if '--guild' in args and ctx.user.id == self.MASTER:
            search['guild'] = args[args.index('--guild') + 1]
        if '--channel' in args:
            search['channel'] = args[args.index('--channel') + 1]
        if '--userid' in args:
            search['user_id'] = args[args.index('--userid') + 1]
        if '--username' in args:
            search['user_name'] = args[args.index('--username') + 1]
        if '--adminid' in args:
            search['admin_id'] = args[args.index('--adminid') + 1]
        if '--adminname' in args:
            search['admin_name'] = args[args.index('--adminname') + 1]
        if '--rule' in args:
            search['rule'] = args[args.index('--rule') + 1]
        if '--reason' in args:
            search['reason'] = args[args.index('--reason') + 1]
        await ctx.response.defer()
        data = await self.db.getWarningData(ctx.guild_id, ctx.user.id == self.MASTER, kwargs=search)
        buffer = io.StringIO()
        writer = csv.writer(buffer)
        writer.writerow(self.csvHeader)
        writer.writerows(data)
        buffer.seek(0)
        await ctx.followup.send(file=File(buffer, f'{ctx.guild.name}-warning-data.csv'))
        return

    @app_commands.command(description='Demote a member')
    @app_commands.describe(
        user='User who gets demoted',
        levels='The number of levels they get demoted by',
        reason='The reason why they are getting demoted'
    )
    async def demote(self,
                     ctx: discord.Interaction,
                     user: discord.Member,
                     levels: int,
                     reason: Optional[str]):
        if not await self._checkPermissions(ctx):
            return
        if user.bot:
            await ctx.response.send_message('Cannot demote a bot', ephemeral=True)
            return
        if levels < 0:
            await ctx.response.send_message('The number of levels must be 0 or higher', ephemeral=True)
            return
        if reason is None:
            reason = 'no specific reason'
        await self.db.demoteUser(user.id, ctx.guild_id, levels)
        level, _ = await self.db.checkUserLevel(user.id, ctx.guild_id)
        await user.send(f'You have been demoted to level {level} in {ctx.guild.name} for {reason}')
        await ctx.response.send_message(f'<@{user.id}> has been demoted to level {level}', ephemeral=True)
        return

    async def report_message(self,
                             ctx: discord.Interaction,
                             msg: discord.Message):
        if not await self._checkPermissions(ctx, False):
            return

        # Respond to command
        await ctx.response.send_message(f'Thank you for reporting this message by {msg.author.mention} to our '
                                        f'admins.', ephemeral=True)

        # Check if message was reported within the last 10 minutes. If it has, it will not generate a new report
        if msg.jump_url in self.reported_messages.keys():
            return

        # Save message url to avoid a 403 error
        report_url = msg.jump_url

        admin_channel = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.admin))

        # Construct embed
        embed = discord.Embed(title='Reported Message', colour=GL.embedColor)
        if msg.content:
            embed.description = msg.content

        embed.set_footer(text=f'Reported by @{ctx.user.display_name}')

        embed.set_author(name=msg.author.display_name, icon_url=msg.author.display_avatar.url)
        embed.timestamp = msg.created_at

        # Construct view and add it to the reported messages dictionary
        view = reportView(msg, embed, self.warn, self.ban, ctx.followup)
        self.reported_messages[msg.jump_url] = view

        # Send report to admins and save the report message in the view
        view.report_msg = await admin_channel.send(embed=embed, view=view)

        # Wait for view to finish or timeout
        await view.wait()

        # Remove reported message
        del self.reported_messages[report_url]
        return


async def setup(bot):
    await bot.add_cog(Moderator(bot))
