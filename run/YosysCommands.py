# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import discord
from discord.ext import commands
from discord import app_commands, File, HTTPException
from typing import Literal, Optional
from EE_Bot_DB import EE_DB, cogEnum
from subprocess import Popen, PIPE, TimeoutExpired
import threading
import asyncio
import globals as GL
import contextlib
import sys

YS = False
try:
    import yosys.yosys as ys

    YS = True
except ImportError:
    YS = False

embedColor = GL.embedColor


class Yosys(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db: EE_DB = bot.db
        self.MASTER = bot.MASTER
        return

    async def cog_load(self) -> None:
        print('Yosys Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        print('Yosys Cog Unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction):
        if ctx.guild_id is None:
            return True
        if not YS or not await self.db.checkCog(ctx.guild_id, cogEnum.Yosys):
            await ctx.response.send('This command has been disabled.')
            return False
        return True

    @staticmethod
    def exec_yosys_cmd(cmd, filename, mutex, outputRef):
        modules, errors, top = ys.run_command(cmd, f'yosys/{filename}')

        error = ''
        L = ['yosys', '-s', 'yosys/yosys.sh', f'yosys/{filename}']
        process = Popen(L, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        try:
            output, error = process.communicate(timeout=60)
        except TimeoutExpired:
            output = ''
            error += '\nTimeout error: Process took too long.'
        finally:
            process.kill()

        # Optimize image files
        if cmd != 'spice' and cmd != 'json':
            if len(modules) == 1:
                process = Popen(['jpegoptim', f'./yosys/{filename}'], universal_newlines=True)
                process.communicate()
            else:
                for x in modules:
                    process = Popen(['jpegoptim', f'./yosys/{filename}_{x}'])
                    process.communicate()

        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = error
        outputRef['top'] = top
        outputRef['modules'] = modules
        outputRef['done'] = True
        mutex.release()
        return

    @app_commands.command(description='Generates a block diagram of your HDL design')
    @app_commands.describe(
        attachment='Verilog file that gets analyzed',
        flags='Custom flags for the analyzation'
    )
    async def elaborate_design(self,
                               ctx: discord.Interaction,
                               attachment: discord.Attachment,
                               flags: Optional[str]):
        if not await self._isEnabled(ctx):
            return

        filename = attachment.filename
        if not filename.endswith('.v'):
            await ctx.response.send_message('Please send 1 Verilog file')
            return

        if flags is None:
            flags = ''

        await attachment.save(fp=f'yosys/{filename}')

        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': [], 'done': False, 'top': ''}
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=('pre-synth', filename, mutex, outputDict))
        yosys_thread.start()

        await ctx.response.defer()
        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']
        modules = outputDict['modules']

        await ctx.followup.send('Sending payload...', ephemeral=True)

        n = 1500
        try:
            if output:
                if '-print' in flags:
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for x in out:
                        await ctx.channel.send(f'```{x}```')

                if len(modules) == 1:
                    with open(f'./yosys/{filename}.jpg', 'rb') as f:
                        try:
                            await ctx.channel.send(file=File(f, f'{filename}.jpg'))
                        except HTTPException as err:
                            await ctx.channel.send(f'{err.status}: {err.text}')
                elif len(modules) > 1:
                    for x in modules:
                        with open(f'./yosys/{filename}_{x}.jpg') as f:
                            try:
                                await ctx.channel.send(file=File(f, f'{filename}_{x}.jpg'))
                            except HTTPException as err:
                                await ctx.channel.send(f'{err.status}: {err.text}')
            if error:
                out = [error[i:i + n] for i in range(0, len(error), n)]
                for x in out:
                    await ctx.channel.send(f'```{x}```')
        except HTTPException as err:
            with contextlib.redirect_stdout(sys.stderr):
                print(err)
        # Clean up
        L = ['make', 'cleanYosys']
        process = Popen(L)
        process.communicate()
        return

    @app_commands.command(description='Generates an optimized block diagram of your HDL design')
    @app_commands.describe(
        attachment='Your Verilog file',
        synth_process='Desired synthesis process. Leave blank for generic synthesis.',
        flags='flags'
    )
    async def synth(self,
                    ctx: discord.Interaction,
                    attachment: discord.Attachment,
                    synth_process: Optional[Literal['achronix', 'anlogic', 'coolrunner2', 'easic', 'ecp5', 'gowin',
                    'greenpak4', 'ice40', 'intel', 'sf2', 'xilinx']],
                    flags: Optional[str]):
        if not await self._isEnabled(ctx):
            return

        if flags is None:
            flags = ''

        filename = attachment.filename
        if not filename.endswith('.v'):
            await ctx.response.send_message('Please send 1 Verilog file')
            return
        await attachment.save(fp=f'./yosys/{filename}')

        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': [], 'done': False, 'top': ''}

        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=(synth_process, filename, mutex, outputDict))
        yosys_thread.start()

        await ctx.response.defer()

        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']
        modules = outputDict['modules']

        await ctx.followup.send('Sending payload...', ephemeral=True)

        n = 1500
        try:
            if output:
                if '-print' in flags:
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for x in out:
                        await ctx.channel.send(f'```{x}```')

                if len(modules) == 1:
                    with open(f'./yosys/{filename}.jpg', 'rb') as f:
                        try:
                            await ctx.channel.send(file=File(f, f'{filename}.jpg'))
                        except HTTPException as err:
                            await ctx.channel.send(f'{err.status}: {err.text}')
                elif len(modules) > 1:
                    for x in modules:
                        with open(f'./yosys/{filename}_{x}.jpg') as f:
                            try:
                                await ctx.channel.send(file=File(f, f'{filename}_{x}.jpg'))
                            except HTTPException as err:
                                await ctx.channel.send(f'{err.status}: {err.text}')
            if error:
                out = [error[i:i + n] for i in range(0, len(error), n)]
                for x in out:
                    await ctx.channel.send(f'```{x}```')
        except HTTPException as err:
            with contextlib.redirect_stdout(sys.stderr):
                print(err)
        # Clean up
        L = ['make', 'cleanYosys']
        process = Popen(L)
        process.communicate()
        return

    @app_commands.command(description='Analyzes the given Verilog file and generates a spice subcircuit')
    @app_commands.describe(
        attachment='Your Verilog file',
        flags='flags'
    )
    async def gen_spice(self,
                        ctx: discord.Interaction,
                        attachment: discord.Attachment,
                        flags: Optional[str]):
        if not await self._isEnabled(ctx):
            return

        filename = attachment.filename
        if not filename.endswith('.v'):
            await ctx.response.send_message('Please send 1 Verilog file')
            return

        if flags is None:
            flags = ''

        await attachment.save(fp=f'yosys/{filename}')

        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': [], 'done': False, 'top': ''}
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=('spice', filename, mutex, outputDict))
        yosys_thread.start()

        await ctx.response.defer()
        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']
        modules = outputDict['modules']

        await ctx.followup.send('Sending payload...', ephemeral=True)

        n = 1500
        try:
            if output:
                if '-print' in flags:
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for x in out:
                        await ctx.channel.send(f'```{x}```')

                if len(modules) == 1:
                    with open(f'./yosys/{filename}.jpg', 'rb') as f:
                        try:
                            await ctx.channel.send(file=File(f, f'{filename}.jpg'))
                        except HTTPException as err:
                            await ctx.channel.send(f'{err.status}: {err.text}')
                elif len(modules) > 1:
                    for x in modules:
                        with open(f'./yosys/{filename}_{x}.jpg') as f:
                            try:
                                await ctx.channel.send(file=File(f, f'{filename}_{x}.jpg'))
                            except HTTPException as err:
                                await ctx.channel.send(f'{err.status}: {err.text}')
            if error:
                out = [error[i:i + n] for i in range(0, len(error), n)]
                for x in out:
                    await ctx.channel.send(f'```{x}```')
        except HTTPException as err:
            with contextlib.redirect_stdout(sys.stderr):
                print(err)
        # Clean up
        L = ['make', 'cleanYosys']
        process = Popen(L)
        process.communicate()
        return

    @app_commands.command(description='Analyzes the given Verilog file and generates a json file')
    @app_commands.describe(
        attachment='Your Verilog file',
        flags='flags'
    )
    async def gen_json(self,
                       ctx: discord.Interaction,
                       attachment: discord.Attachment,
                       flags: Optional[str]):
        if not await self._isEnabled(ctx):
            return

        filename = attachment.filename
        if not filename.endswith('.v'):
            await ctx.response.send_message('Please send 1 Verilog file')
            return

        if flags is None:
            flags = ''

        await attachment.save(fp=f'yosys/{filename}')

        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': [], 'done': False, 'top': ''}
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=('json', filename, mutex, outputDict))
        yosys_thread.start()

        await ctx.response.defer()
        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']
        modules = outputDict['modules']

        await ctx.followup.send('Sending payload...', ephemeral=True)

        n = 1500
        try:
            if output:
                if '-print' in flags:
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for x in out:
                        await ctx.channel.send(f'```{x}```')

                if len(modules) == 1:
                    with open(f'./yosys/{filename}.jpg', 'rb') as f:
                        try:
                            await ctx.channel.send(file=File(f, f'{filename}.jpg'))
                        except HTTPException as err:
                            await ctx.channel.send(f'{err.status}: {err.text}')
                elif len(modules) > 1:
                    for x in modules:
                        with open(f'./yosys/{filename}_{x}.jpg') as f:
                            try:
                                await ctx.channel.send(file=File(f, f'{filename}_{x}.jpg'))
                            except HTTPException as err:
                                await ctx.channel.send(f'{err.status}: {err.text}')
            if error:
                out = [error[i:i + n] for i in range(0, len(error), n)]
                for x in out:
                    await ctx.channel.send(f'```{x}```')
        except HTTPException as err:
            with contextlib.redirect_stdout(sys.stderr):
                print(err)
        # Clean up
        L = ['make', 'cleanYosys']
        process = Popen(L)
        process.communicate()
        return


async def setup(bot):
    if YS:
        await bot.add_cog(Yosys(bot=bot))
