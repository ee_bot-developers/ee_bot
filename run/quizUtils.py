# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from aenum import MultiValueEnum, no_arg
from dataclasses import dataclass
from typing import Optional, Union, Sequence


class Units(MultiValueEnum):
    AMPS = 'A'
    VOLTS = 'V'
    OHMS = 'ohms', 'ohm', 'Ω'
    WATTS = 'W'
    FARADS = 'F'
    HERTZ = 'Hz'
    HENRYS = 'H'
    SECONDS = 's'
    GAIN = 'V/V'
    NO_UNITS = None, ''
    INVALID = 'invalid'

    @classmethod
    def _missing_(cls, value):
        if value is no_arg:
            return cls.NO_UNITS
        return cls.INVALID


@dataclass
class QuestionMetaData:
    question: str
    file: Optional[str]
    answer: Union[complex, str, int, float, Sequence[str]]
    margin: Union[float, complex]
    units: Units
    UserAnswer: Optional[str] = None
    correct: bool = False


# Function that converts a str to a complex number
def str2complex(num: str) -> Optional[complex]:
    # Python uses j for imaginary numbers and will throw an error if i is used
    num = num.replace('i', 'j')
    num = num.replace(' ', '')
    # Remove leading '+'
    if num == '+':
        num = num[1:]
    # Remove unnecessary '+'
    i = 0
    while i < len(num):
        if num[i] == '+' and num[i - 1] == 'e':
            num = num[:i] + num[i + 1:]
        i += 1
    # Standardize input
    i = 1
    while i < len(num):
        if num[i] == '-' and num[i - 1] != 'e':
            num = num[:i] + '+' + num[i:]
            i += 1
        i += 1
    # Split into 2 numbers, guarantees that one number is returned
    num = num.split('+')
    Num = {'real': 0.0, 'imag': 0.0}
    try:
        for x in num:
            if 'j' in x:
                if x.count('j') > 1:
                    return None
                val = x.replace('j', '')
                if not val:
                    val = '1'
                Num['imag'] += float(val)
            else:
                if not x:
                    return None
                Num['real'] += float(x)
    except ValueError:
        return None
    return complex(real=Num['real'], imag=Num['imag'])
