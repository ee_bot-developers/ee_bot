# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import app_commands, File
from typing import Literal, Optional
from discord.ext import commands
from EE_Bot_DB import EE_DB, itoa
import EE_Bot_DB as DB
from subprocess import Popen, PIPE
import EE_Bot_help as help
import threading
import asyncio
import os
import io
from globals import embedColor, selectCompiler, architectures, compilers, supportedFileTypes
import globals as GL
from scipy.signal import ss2tf
import numpy as np
from imageGeneration import latex_engine
import openai
from inspect import currentframe, getframeinfo
from imageGeneration.genRankImage import createRankImage


class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db
        self._version = bot.version
        GL.InitLaunch()
        self._released = GL.launchFlag
        openai.api_key = bot.openai

    async def cog_load(self) -> None:
        print('General Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        print('General Cog Unloaded!')
        return

    # Function that keeps the compile command from blocking the bot
    @staticmethod
    def runCompile(args, mutex, outputDict):
        # Create new process
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        try:
            # Start process with 5 minute timeout
            output, _error = process.communicate(timeout=300)
        except TimeoutError:
            # Process timeout error
            output = ''
            _error = '\n Timeout error!'
        finally:
            # Kill the process
            process.kill()

        mutex.acquire()
        outputDict['output'] = output
        outputDict['error'] = _error
        outputDict['done'] = True
        mutex.release()

    # Function that keeps kicost command from blocking bot
    @staticmethod
    def execKiCost(args, mutex, outputRef):
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        output, _error = process.communicate()

        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = _error
        outputRef['done'] = True
        mutex.release()

    # Status command
    @app_commands.command(name='status', description='Checks the status of the bot')
    async def status(self, ctx: discord.Interaction):
        # TODO: Consider setting status to something else when errors arise
        await ctx.response.send_message(f'EE_Bot {self._version}\nEE_Bot_DB {DB.__version__}\n'
                                        f'Latency: {self.bot.latency * 1000: .2f} ms\nStatus: OK')
        return

    # Filter command (Easter Egg)
    @app_commands.command(name='filter')
    @app_commands.guild_only()
    @app_commands.describe(
        toggle='Toggle status of filter.'
    )
    async def filter(self,
                     ctx: discord.Interaction,
                     toggle: Literal['on', 'off']):
        # Check rank
        rank, _ = await self.db.checkUserLevel(ctx.user.id, ctx.guild_id)

        reqLevel = await self.db.getFilterRep(ctx.guild_id)
        if reqLevel is None:
            reqLevel = float('inf')

        # Early exit if rank < 5 or there was a database error
        if rank < reqLevel:
            await ctx.response.send_message('Cannot update filter. Insufficient level.')
            return
        newFilter = False
        if toggle == 'on':
            newFilter = True
        # Update flag in database
        await self.db.updateUserFilter(ctx.user.id, ctx.guild_id, newFilter)
        await ctx.response.send_message('Filter updated')

    @app_commands.command(name='rank', description='Displays level and progress')
    @app_commands.guild_only()
    async def rank(self, ctx: discord.Interaction):
        await ctx.response.defer()
        # Check rank and progress
        Rank, progress = await self.db.checkUserLevel(ctx.user.id, ctx.guild_id)
        users = await self.db.getAllUsers(ctx.guild_id)
        reqXP = self.db.calcXP(Rank + 1)
        currentXP = self.db.reverseProgressCalc(Rank, progress)
        # Generate card
        buf = io.BytesIO()
        await ctx.user.display_avatar.save(buf)
        status = ctx.guild.get_member(ctx.user.id).status
        img = createRankImage(ctx.user.display_name,
                              buf,
                              status,
                              Rank,
                              progress,
                              users.getGuildPlace(ctx.user.id),
                              currentXP,
                              reqXP,
                              (35, 38, 42, 255),
                              (0, 0, 0, 255),
                              (0x0E, 0x82, 0xC4, 255),
                              (72, 75, 78, 255),
                              (255, 255, 255, 255))
        # Display rank and progress
        with img as f:
            await ctx.followup.send(file=discord.File(f, f'{ctx.user.display_name}-{ctx.created_at}.png'))
        return

    # Command to turn on/off rank up notifications
    @app_commands.command(name='notif', description='Toggles level up notifications')
    @app_commands.guild_only()
    @app_commands.describe(
        notifications='Turn on or off level notifications.'
    )
    async def notif(self,
                    ctx: discord.Interaction,
                    notifications: Literal['on', 'off']):
        await self.db.updateNotifications(ctx.user.id, ctx.guild_id, notifications == 'on')
        await ctx.response.send_message(f'Level up notifications are now turned {notifications} for you. '
                                        f'To see current rank, use the rank command.')
        return

    # Compile command
    @app_commands.command(name='compile', description='See /help for information')
    @app_commands.describe(
        source='Your C or C++ file',
        compiler='Compiler used',
        architecture='The target architecture',
        endianness='The endianness of the system',
        flags='Compiler flags'
    )
    async def compile(self,
                      ctx: discord.Interaction,
                      source: discord.Attachment,
                      compiler: compilers,
                      architecture: Optional[architectures] = '',
                      endianness: Optional[Literal['little', 'big']] = '',
                      flags: str = ''):
        preprocess = False

        if selectCompiler is None:
            await ctx.response.send_message('Not supported')
            return

        # Parse filename and check if it is a .c or .cpp file
        parsedFileName = source.filename.split('.')
        if parsedFileName[-1] not in supportedFileTypes:
            await ctx.response.send_message(f'Please attach a file with one of the following extensions: '
                                            f'{supportedFileTypes}')
            return

        # Run the function to select compiler
        Valid, compiler = await selectCompiler(ctx, compiler, architecture, endianness)

        # Check if a valid compiler was selected
        if not Valid:
            return

        # Parse flags
        if flags:
            # TODO: Find better way to do this
            flagsList = flags.split(' ')
            sFlagFound = False
            for x in flagsList:
                if x[0] == '-' and x[1] != '-':
                    if 'E' in x:
                        preprocess = True
                    if 'S' in x:
                        sFlagFound = True
            if not preprocess and not sFlagFound:
                flags += ' -S'
        else:
            flags = '-S'

        # Save file and generate assembly filename
        filename = source.filename
        await source.save(fp=os.PathLike[f'./Compile/{filename}'])
        parsedFileName.pop()
        parsedFileName.append('s')
        assembly = '.'.join(parsedFileName)
        # Arguments for command line
        L = ['./compile.out', f'"{compiler}"', f'"{filename}"', f'"{flags}"']

        # Create thread and run
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        thread = threading.Thread(target=self.runCompile, args=(L, mutex, outputDict))
        thread.run()

        # Wait until thread has finished
        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        error = outputDict['error']

        # Try sending output
        n = 1500
        try:
            if output != '':
                out = [output[i:i + n] for i in range(0, len(output), n)]
                for x in out:
                    await ctx.followup.send(f'```{x}```')
                if not preprocess:
                    try:
                        with open(f'Compile/{assembly}', 'rb') as f:
                            await ctx.followup.send(file=File(f, assembly))
                    except Exception as err:  # TODO: Catch specific exception
                        await ctx.followup.send('Error: Assembly file too large')
                        frameinfo = getframeinfo(currentframe())
                        print(f'{frameinfo.filename}:{frameinfo.lineno}: {err}')
            if error != '':
                await ctx.followup.send('--- Errors ---')
                out = [error[i:i + n] for i in range(0, len(error), n)]
                for x in out:
                    await ctx.followup.send(f'```{x}```')
        except Exception as err:
            frameinfo = getframeinfo(currentframe())
            print(f'{frameinfo.filename}:{frameinfo.lineno}: {err}')
        mutex.release()
        # Clean up
        L = ["bash", "Compilecleanup.sh"]
        process = Popen(L)
        process.communicate()
        return

    # noinspection PyUnreachableCode
    @app_commands.command(name='kicost', description='Run kicost on BOM')
    async def kicost(self,
                     ctx: discord.Interaction,
                     bom: discord.Attachment,
                     flags: str):
        if __debug__:
            file = await bom.to_file()
            await ctx.response.send_message(file=file)
            return
        parsedFileName = bom.filename.split('.')
        if parsedFileName in ['xml', 'csv']:
            # TODO: Update this to be more robust
            if '-i' in flags or '--input' in flags:
                await ctx.response.send_message('Error: Unrecognized flag. The bot automatically uses the input flag!')
                return
            if '-o' in flags or '--output' in flags:
                await ctx.response.send_message('Error: Unrecognized flag. The bot automatically uses the output flag!')
                return
            if '-h' in flags or '--help' in flags:
                await help.help_cmd(ctx, self.bot.user.display_avatar.url, 'kicost', self.db)
                return
            if '-v' in flags or '--version' in flags:
                await ctx.response.send_message('Error: Version flag is not supported!')
                return
            if '--info' in flags:
                await ctx.response.send_message('Error: Info flag is not supported!')
                return
            if '--gui' in flags:
                await ctx.response.send_message('Error: GUI flag is not supported!')
                return
            if '--user' in flags:
                await ctx.response.send_message('Error: User flag is not supported!')
                return
            if '--setup' in flags:
                await ctx.response.send_message('Error: Setup flag is not supported!')
                return
            if '--unsetup' in flags:
                await ctx.response.send_message('Error: Unsetup flag is not supported!')
                return
            filename = bom.filename
            await bom.save(fp=os.PathLike[f'./kicost/{filename}'])
            parsedFileName.pop()
            parsedFileName.append('xlsx')
            outFile = '.'.join(parsedFileName)
            L = ['kicost', '-i', f'kicost/{filename}', '-o', f'kicost/{outFile}']
            if flags:
                L += flags
            outputDict = {'output': '', 'error': '', 'done': False}
            mutex = threading.Lock()
            kicostCmd = threading.Thread(target=self.execKiCost, args=(L, mutex, outputDict))
            kicostCmd.start()
            await ctx.response.send_message('Processing request. This may take a while...')

            mutex.acquire()
            while not outputDict['done']:
                mutex.release()
                await asyncio.sleep(1)
                mutex.acquire()

            output = outputDict['output']
            error = outputDict['error']
            # Try sending output
            n = 1500
            try:
                if output != '':
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for x in out:
                        await ctx.followup.send(f'```{x}```')
                if error != '':
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    await ctx.followup.send('--- Errors ---')
                    for x in out:
                        await ctx.followup.send(f'```{x}```')
                with open(f'kicost/{outFile}', 'rb') as f:
                    await ctx.followup.send("KiCost is powered by the Kitspace PartInfo API. Partinfo hooks into "
                                            "paid-for 3rd party services. If you find KiCost useful please donate to "
                                            "the Kitspace Open Collective. If Kitspace doesn't receive enough "
                                            "donations then this free service will have to be shut down. "
                                            "https://opencollective.com/kitspace", file=File(f, outFile))
            except Exception as err:
                frameinfo = getframeinfo(currentframe())
                print(f'{frameinfo.filename}:{frameinfo.lineno}: {err}')
            L = ['rm', '-f', f'kicost/{filename}', f'kicost/{outFile}']
            process = Popen(L)
            process.communicate()
            return

    @app_commands.command(name='radix', description='Change radix of given number')
    @app_commands.describe(
        original_base='Current radix',
        new_base='Desired radix',
        number='...'
    )
    async def radix(self,
                    ctx: discord.Interaction,
                    original_base: int,
                    new_base: int,
                    number: str):
        try:
            dec_number = int(number, base=original_base)
            new_number = itoa(dec_number, new_base)
        except ValueError as err:
            await ctx.response.send_message(str(err))
            return
        embed = discord.Embed(description=f'Base {original_base} {number} = Base {new_base} {new_number}',
                              colour=embedColor)
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        await ctx.response.send_message(embed=embed)
        return

    @app_commands.command(name='mils_mm', description='Converts between mils and mm.')
    @app_commands.describe(
        new_units='Convert to this unit',
        value='The value that gets converted'
    )
    async def mils_mm(self,
                      ctx: discord.Interaction,
                      new_units: Literal['mm', 'mils'],
                      value: float):
        # Select the proper case
        if new_units == 'mils':
            # 1 mm == 39.3701 mils
            newValue = value * 39.3701
            oldUnits = 'mm'
        else:
            # 1 mil == 0.0254 mm
            newValue = value * 0.0254
            oldUnits = 'mils'
        await ctx.response.send_message(f'{value} {oldUnits} == {newValue} {new_units}')
        return

    @app_commands.command(name='leaderboard', description='Shows xp leaderboard for server')
    async def leaderboard(self, ctx: discord.Interaction):
        users = await self.db.getAllUsers(ctx.guild_id)
        embed = discord.Embed(title='Leaderboard', description='These are the top members',
                              color=embedColor)
        if ctx.guild is None:
            top10 = users.getGlobalTop10()
            position = users.getGlobalPlace(ctx.user.id)
        else:
            top10 = users.getTop10()
            position = users.getGuildPlace(ctx.user.id)
        i = 1
        for User in top10:
            user = discord.utils.get(ctx.guild.members, id=User['user_id'])
            if ctx.guild is not None:
                embed.add_field(name=f'#{i}', value=f'{user.display_name}\nXP: {User["xp"]}', inline=False)
            else:
                embed.add_field(name=f'#{i}', value=f'{user.display_name}\nLevel {User["total_posts"]}', inline=False)
            i += 1
        embed.set_footer(text=f'You are #{position} in the leaderboard.')
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        await ctx.response.send_message(embed=embed)
        return

    @staticmethod
    def standardize_matrix(matrix_str: str) -> np.ndarray:
        matrix_str = ''.join(matrix_str.split())  # Remove all whitespace
        if "[[" in matrix_str:
            matrix_str.replace("],", "];")
        matrix = np.matrix(matrix_str, dtype=float, copy=True)
        return matrix

    @staticmethod
    def generate_latex_polynomial(array: np.ndarray) -> str:
        array = array.flatten()
        order = array.size
        polynomial = ''
        for x in range(order):
            if array[x] < 0:
                polynomial += f'{array[x]:.3f}'
            else:
                polynomial += f'+{array[x]:.3f}'
            if (x + 1) != order:
                if (order - x - 1) != 1:
                    polynomial += f's^{order - x - 1}'
                else:
                    polynomial += 's'
        if polynomial.startswith('+'):
            polynomial = polynomial[1:]
        return polynomial

    @app_commands.command(name='ss2tf', description='Converts state space into a transfer function')
    @app_commands.describe(
        a='State (or system) matrix of shape (n, n)',
        b='Input matrix of shape (n, p)',
        c='Output matrix of shape (q, n)',
        d='Feedthrough (or feedforward) matrix of shape (q, p)'
    )
    async def ss2tf(self,
                    ctx: discord.Interaction,
                    a: str,
                    b: str,
                    c: str,
                    d: str):
        # Sanitize user input and check for errors
        try:
            A_mat = self.standardize_matrix(a)
        except ValueError:
            await ctx.response.send_message('Error: Cannot convert A to a matrix! Requested matrix has an '
                                            'inhomogeneous shape')
            return
        dim = A_mat.shape
        if dim[0] != dim[1]:
            await ctx.response.send_message('Error: A matrix has bad dimensions. Must be an n x n matrix.')
            return
        n = dim[0]
        try:
            B_mat = self.standardize_matrix(b)
        except ValueError:
            await ctx.response.send_message('Error: Cannot convert B to a matrix! Requested matrix has an '
                                            'inhomogeneous shape')
            return
        dim = B_mat.shape
        if dim[0] != n:
            await ctx.response.send_message('Error: B matrix has bad dimensions. Must be an n x p matrix.')
            return
        p = dim[1]
        try:
            C_mat = self.standardize_matrix(c)
        except ValueError:
            await ctx.response.send_message('Error: Cannot convert C to a matrix! Requested matrix has an '
                                            'inhomogeneous shape')
            return
        dim = C_mat.shape
        if dim[1] != n:
            await ctx.response.send_message('Error: C matrix has bad dimensions. Must be a q x n matrix.')
            return
        q = dim[0]
        try:
            D_mat = self.standardize_matrix(d)
        except ValueError:
            await ctx.response.send_message('Error: Cannot convert D to a matrix! Requested matrix has an '
                                            'inhomogeneous shape')
            return
        dim = D_mat.shape
        if dim[0] != q or dim[1] != p:
            await ctx.response.send_message('Error: D matrix has bad dimensions. Must be a q x n matrix.')
            return

        # User input sanitized and verified, defer response since code below runs a while
        await ctx.response.defer()

        # Attempt to convert state space into transfer function. Send failure info if operation failed
        try:
            num, den = ss2tf(A_mat, B_mat, C_mat, D_mat)
        except ValueError as err:
            await ctx.followup.send(str(err))
            return

        # Generate latex string and render it in an image
        latex_str = f'$H(s) = \\frac{{{self.generate_latex_polynomial(num)}}}{{{self.generate_latex_polynomial(den)}}}$'

        image, filename = latex_engine.render_latex(latex_str)

        # Handle error
        if image is None:
            await ctx.followup.send(f'Error rendering latex image. Please paste this string into your favorite latex '
                                    f'editor: `{latex_str}`')
            return

        # Reset iterator to beginning
        image.seek(0)

        # Followup with result
        await ctx.followup.send(file=discord.File(image, filename))

        return

    @app_commands.command()
    @app_commands.guild_only
    async def query(self,
                    ctx: discord.Interaction,
                    query: str):
        try:
            query = await self.db.getQuery(ctx.guild_id, query)
        except DB.EE_DB_Exception:
            if not await self.db.checkAI(ctx.guild_id) or True:
                await ctx.response.send_message('Not a valid query.', ephemeral=True)
                return
            # Could not find query in database. Defer response and generate response with openAI instead
            await ctx.response.defer()
            print('Generating response')
            Query = await openai.Completion.acreate(
                engine=GL.OPENAI_MODEL_ENGINE,
                prompt=query,
                max_tokens=GL.OPENAI_MAX_TOKENS,
                n=GL.OPENAI_N,
                stop=GL.OPENAI_STOP,
                temperature=GL.OPENAI_TEMPERATURE
            )
            title = await openai.Completion.acreate(
                engine=GL.OPENAI_MODEL_ENGINE,
                prompt=f'Create simple title for "{query}". Do not include any other words outside of prompt',
                max_tokens=GL.OPENAI_MAX_TOKENS,
                n=GL.OPENAI_N,
                stop=GL.OPENAI_STOP,
                temperature=GL.OPENAI_TEMPERATURE
            )
            print('Done')
            embed = discord.Embed(title=title.choices[0].text.strip('\n\"'), description=Query.choices[0].text,
                                  colour=embedColor)
            embed.set_thumbnail(url=self.bot.user.display_avatar.url)
            embed.set_footer(text='This response was generated with openAI. Please take it with a grain of salt.')
            await ctx.followup.send(embed=embed)
            return
        # Construct response and send it
        embed = discord.Embed(title=query['formula'], url=query['url'], description=query['description'],
                              colour=embedColor)
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        if query['image'] is not None:
            embed.set_image(url=f'attachment://{query["filename"]}')
            with io.BytesIO(query['image']) as fp:
                fp.seek(0)
                file = discord.File(fp, filename=query['filename'])
                await ctx.response.send_message(embed=embed, file=file)
        else:
            await ctx.response.send_message(embed=embed)

    @query.autocomplete('query')
    async def query_autocomplete(self, ctx: discord.Interaction, current: str):
        entries = await self.db.getValidQueries(ctx.guild_id)
        entries = [discord.app_commands.Choice(name=entry, value=entry) for entry in entries
                   if entry.lower().startswith(current)]
        if len(entries) > 25:
            return entries[:25]
        return entries


async def setup(bot):
    await bot.add_cog(General(bot))
