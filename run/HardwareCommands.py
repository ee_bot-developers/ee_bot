# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import app_commands
from discord.ext import commands
from typing import Optional
from EE_Bot_DB import EE_DB, cogEnum
from subprocess import Popen, PIPE
import threading
import asyncio
import globals as GL


# Initialize and get globals
GL.InitLaunch()
originGuild = GL.originGuild
launch = GL.launchFlag


class Hardware(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db

    async def cog_load(self) -> None:
        print(f'{self.__class__.__name__} loaded!')
        return

    async def cog_unload(self) -> None:
        print(f'{self.__class__.__name__} unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction) -> bool:
        if ctx.guild is not None:
            if self.db.checkCog(ctx.guild_id, cogEnum.Hardware):
                return True
        await ctx.response.send('This command has been disabled')
        return False

    @app_commands.command(name='temp')
    @app_commands.guilds(originGuild)
    async def temp(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        await ctx.response.send_message('Not implemented yet')
        return

    @app_commands.command(name='cool')
    @app_commands.default_permissions(administrator=True)
    @app_commands.guilds(originGuild)
    async def cool(self,
                   ctx: discord.Interaction,
                   time: Optional[float] = 10.0):
        await ctx.response.send_message('This command is not functional')

    @staticmethod
    def executeCmd(args, mutex, outputRef):
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        output, _error = process.communicate()
        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = _error
        outputRef['done'] = True
        mutex.release()
        return

    @app_commands.command(name='cmd')
    @app_commands.default_permissions(administrator=True)
    async def cmd(self,
                  ctx: discord.Interaction,
                  command: str):
        if not await self._isEnabled(ctx):
            return
        developer = False
        guild = discord.utils.find(lambda g: g.id == 758600193070858251, ctx.user.mutual_guilds)
        if guild is not None:
            mem = discord.utils.find(lambda m: m.id == ctx.user.id, guild.members)
            role = discord.utils.find(lambda r: r.name == 'Bot Developer', mem.roles)
            if role is not None:
                developer = True

            # Make sure rm command is not ran
            if command[:2] == 'rm':
                developer = False
                ctx.user.id = 0
            # Execute command on certain conditions
            if (ctx.user.id == self.MASTER) or developer:
                await ctx.response.send_message('Running command...')

                # Convert args into list
                L = command.split(' ')

                cmdOutputDict = {'output': '', 'error': '', 'done': False}
                mutex = threading.Lock()

                # Create and start thread
                cmdThread = threading.Thread(target=self.executeCmd, args=(L, mutex, cmdOutputDict))
                cmdThread.start()

                # Wait for thread to finish without blocking other commands
                mutex.acquire()
                while not cmdOutputDict['done']:
                    mutex.release()
                    await asyncio.sleep(1)
                    mutex.acquire()

                output = cmdOutputDict['output']
                error = cmdOutputDict['error']

                # Try sending output
                n = 1500
                try:
                    if output != '':
                        out = [output[i:i + n] for i in range(0, len(output), n)]
                        for x in out:
                            await ctx.followup.send(f'```{x}```')
                    if error != '':
                        out = [error[i:i + n] for i in range(0, len(error), n)]
                        for x in out:
                            await ctx.followup.send(f'```{x}```')
                except Exception:
                    pass

                mutex.release()
            else:
                await ctx.response.send_message('Nice try')
        return


async def setup(bot):
    await bot.add_cog(Hardware(bot=bot))
