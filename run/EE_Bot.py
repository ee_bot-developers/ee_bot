# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import File
from discord.ext import commands
from typing import Optional, Union, Dict
import urllib.request
import urllib.error
import emoji as Emoji
import re
import io
from EE_Bot_DB import EE_DB, channelType, EE_DB_Exception
import asyncio
import globals as GL
from bot_utils import load_login, run_once, UNUSED
import logging
import logging.handlers
import os

# Current version of the bot.
# The bot version is different from
# the database version.
__version__ = 'v3.0.4'

# Initialize shared global constants
GL.InitLaunch()
LAUNCH = GL.launchFlag
ORIGIN = GL.originGuild

MASTER = 751265080963891230

KEY_FILE_LOCATION = './client_secrets.json'

DISABLE_DATABASE = False

# Two possible database locations
LAUNCH_DATABASE = 'ee_bot_production'
TEST_DATABASE = 'ee_bot_test'


class EE_Bot_Command_Tree(discord.app_commands.CommandTree):
    _before_invoke: Optional[discord.ext.commands.bot.CFT] = None
    _after_invoke: Optional[discord.ext.commands.bot.CFT] = None

    def before_invoke(self, coro: discord.ext.commands.bot.CFT):
        if not asyncio.iscoroutinefunction(coro):
            raise TypeError('The pre-invoke hook must be a coroutine.')
        self._before_invoke = coro
        return coro

    def after_invoke(self, coro: discord.ext.commands.bot.CFT):
        if not asyncio.iscoroutinefunction(coro):
            raise TypeError('The post-invoke hook must be a coroutine.')
        self._after_invoke = coro
        return coro

    async def _call(self, interaction: discord.Interaction) -> None:
        if interaction.type is not discord.InteractionType.autocomplete:
            if self._before_invoke is not None:
                await self._before_invoke(interaction)

        await super()._call(interaction)

        if interaction.type is not discord.InteractionType.autocomplete:
            if self._after_invoke is not None:
                await self._after_invoke(interaction)
        return


class EE_Bot(commands.Bot):
    def __init__(self,
                 database: EE_DB,
                 MASTER_ID: int,
                 key_file: str,
                 version: str,
                 disable_database: bool,
                 openai: str,
                 command_prefix: str = '!',
                 **kwargs
                 ):
        super().__init__(command_prefix=command_prefix,
                         help_command=None,
                         **kwargs)
        self.db = database
        self.MASTER = MASTER_ID
        self.key_file = key_file
        self.version = version
        self.disableDB = disable_database
        self._retries = 0
        self._archiveLocks: Dict[int, asyncio.Lock] = {}
        self._purgeRoleLocks: Dict[int, asyncio.Lock] = {}
        self._asyncArchiveLock = asyncio.Lock()
        self._asyncPurgeLock = asyncio.Lock()
        self.openai = openai
        self._reboot = True
        self._rebootLock = asyncio.Lock()
        self._heartbeat = True
        self._heartbeatLock = asyncio.Lock()
        asyncio.create_task(self._heartbeat_update())
        self._referenceCount: int = 0
        self._referenceCountLock = asyncio.Lock()
        self.before_invoke(self._increaseReferenceCount)
        self.after_invoke(self._decreaseReferenceCount)
        self.tree.before_invoke(self._increaseReferenceCount)
        self.tree.after_invoke(self._decreaseReferenceCount)
        self.anti_spam: Dict[int, commands.CooldownMapping] = {}
        return

    async def ArchiveTasks(self, guild_id: int, acquire_lock: bool) -> Optional[bool]:
        """Locks the archive task
        :param[in] guild_id The guild id the lock belongs to
        :param[in] acquire_lock False if release lock, True if acquire lock
        :return bool True if lock was acquired, False if lock was not acquired, None if released"""
        async with self._asyncArchiveLock:
            if acquire_lock:
                if self._archiveLocks[guild_id].locked():
                    return False
                await self._archiveLocks[guild_id].acquire()
                return True
            if self._archiveLocks[guild_id].locked():
                self._archiveLocks[guild_id].release()
            return None

    async def PurgeTasks(self, guild_id: int, acquire_lock: bool) -> Optional[bool]:
        """Locks the purge roles task
        :param[in] guild_id The guild id the lock belongs to
        :param[in] acquire_lock False if release lock, True if acquire lock
        :return bool True if lock was acquired, False if lock was not acquired, None if released"""
        async with self._asyncPurgeLock:
            if acquire_lock:
                if self._purgeRoleLocks[guild_id].locked():
                    return False
                await self._purgeRoleLocks[guild_id].acquire()
                return True
            if self._purgeRoleLocks[guild_id].locked():
                self._purgeRoleLocks[guild_id].release()
            return None

    async def setup_hook(self) -> None:
        await self.load_extension('GeneralCommands')
        await self.load_extension('GameCommands')
        await self.load_extension('HardwareCommands')
        await self.load_extension('ModeratorCommands')
        await self.load_extension('PowerCommands')
        await self.load_extension('RFCommands')
        await self.load_extension('ScheduleCommands')
        await self.load_extension('serverConfigCommands')
        await self.load_extension('YosysCommands')
        await self.load_extension('UtilityCommands')
        return

    async def _increaseReferenceCount(self, ctx: Union[discord.ext.commands.Context, discord.Interaction]):
        UNUSED(ctx)
        await self._referenceCountLock.acquire()
        self._referenceCount += 1
        self._referenceCountLock.release()

    async def _decreaseReferenceCount(self, ctx: Union[discord.ext.commands.Context, discord.Interaction]):
        UNUSED(ctx)
        await self._referenceCountLock.acquire()
        self._referenceCount -= 1
        self._referenceCountLock.release()

    async def reboot(self) -> bool:
        await self._rebootLock.acquire()
        reboot = self._reboot
        self._rebootLock.release()
        return reboot

    async def kill(self, reboot: bool = True):
        await self._heartbeatLock.acquire()
        self._heartbeat = False
        self._heartbeatLock.release()
        await self._rebootLock.acquire()
        self._reboot = reboot
        self._rebootLock.release()
        return

    async def _heartbeat_update(self):
        while not self.is_closed():
            await self._heartbeatLock.acquire()
            if not self._heartbeat:
                await self._referenceCountLock.acquire()
                while self._referenceCount:
                    self._referenceCountLock.release()
                    await asyncio.sleep(0.1)
                    await self._referenceCountLock.acquire()
                await self.close()
                self._referenceCountLock.release()
            self._heartbeatLock.release()
            await asyncio.sleep(1)

    async def _updateFlags(self, guild_id: int):
        async with self._asyncArchiveLock:
            if guild_id not in self._archiveLocks.keys():
                self._archiveLocks[guild_id] = asyncio.Lock()
        async with self._asyncPurgeLock:
            if guild_id not in self._purgeRoleLocks.keys():
                self._purgeRoleLocks[guild_id] = asyncio.Lock()

    async def _initialize_spam_detection(self, guild: int) -> None:
        parameters = await self.db.getSpamProtectionParameters(guild)
        self.anti_spam[guild] = commands.CooldownMapping.from_cooldown(parameters[0], parameters[1],
                                                                       commands.BucketType.member)
        return

    @staticmethod
    def isPhraseIn(phrase, msg):
        return re.search(r"\b{}\b".format(phrase), msg, re.IGNORECASE)

    async def on_ready(self):
        await self.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name='Type /help for '
                                                                                                     'usage'))
        for guild in self.guilds:
            await self._updateFlags(guild.id)
            await self.db.addGuild(guild.id)
            users = [user.id for user in guild.members if not user.bot]
            await self.db.updateGuildUsers(guild.id, users)
            await self._initialize_spam_detection(guild.id)
        await self.db.adjustRanks()
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')
        return

    async def on_message(self, message: discord.Message) -> None:
        if message.author.bot:
            return
        self._retries = 0

        if not message.content.startswith(self.command_prefix):
            if message.guild is None:
                return

            rank: Optional[int] = None
            rankUp: Optional[bool] = None

            if not self.disableDB:
                rankUp, rank = await self.db.updateUser(message.author.id, message.guild.id)

            monitor = await self.db.checkMonitor(message.guild.id, message.author.id)

            if monitor:
                watchChannel = self.get_channel(await self.db.getLoggingChannel(message.guild.id, channelType.admin))
                if watchChannel is not None:
                    embed = discord.Embed(title='Watched Author', color=GL.embedColor)
                    if message.content:
                        embed.description = message.content

                    embed.set_author(name=message.author.display_name, icon_url=message.author.display_avatar.url)
                    embed.timestamp = message.created_at

                    url_view = discord.ui.View()
                    url_view.add_item(discord.ui.Button(label='Go to Message', style=discord.ButtonStyle.url,
                                                        url=message.jump_url))

                    await watchChannel.send(embed=embed, view=url_view)

            msg = re.sub(r'[^\w\s]', '', message.content)

            if self.isPhraseIn('railgun', msg):
                files = []
                with open('images/EasterEggs/railgun1.jpg', 'rb') as f:
                    files.append(File(f, 'railgun1.jpg'))
                with open('images/EasterEggs/railgun2.jpg', 'rb') as f:
                    files.append(File(f, 'railgun2.jpg'))
                with open('images/EasterEggs/railgun3.jpg', 'rb') as f:
                    files.append(File(f, 'railgun3.jpg'))
                with open('images/EasterEggs/railgun4.jpg', 'rb') as f:
                    files.append(File(f, 'railgun4.jpg'))
                with open('images/EasterEggs/railgun1.mp4', 'rb') as f:
                    files.append(File(f, 'railgun1.mp4'))
                with open('images/EasterEggs/railgun2.mp4', 'rb') as f:
                    files.append(File(f, 'railgun2.mp4'))
                with open('images/EasterEggs/railgun3.mp4', 'rb') as f:
                    files.append(File(f, 'railgun3.mp4'))

                await message.reply('Did someone say railgun?', files=files)

            eggs = await self.db.fetchEasterEggs(message.guild.id)
            userFilter = await self.db.checkUserFilter(message.author.id, message.guild.id)

            for egg in eggs:
                if self.isPhraseIn(egg['phrase'], msg):
                    if egg['limited'] and userFilter:
                        continue
                    with io.BytesIO(egg['file']) as fp:
                        fp.seek(0)
                        f = File(fp, egg['filename'], spoiler=bool(egg['limited']))
                        await message.channel.send(file=f)

            if rankUp:
                levelUpChannel = self.get_channel(await self.db.getLoggingChannel(message.guild.id, channelType.level))
                if levelUpChannel is None:
                    return
                if rank != 1:
                    await levelUpChannel.send(f'Congratulations <@{message.author.id}>, you just advanced to level '
                                              f'{rank}!')
                else:
                    await levelUpChannel.send(f'Congratulations <@{message.author.id}>, you just advanced to level '
                                              f'{rank}! To disable level notifications for yourself, '
                                              f'use the /notification command.')
        else:
            await self.process_commands(message)

    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        if payload.member.bot:
            return
        roleChannel = await self.db.getLoggingChannel(payload.guild_id, channelType.role)
        poliRulesChannel = await self.db.getPoliChannel(payload.guild_id)
        if payload.channel_id != roleChannel and payload.channel_id != poliRulesChannel:
            return
        guildID = payload.guild_id
        guild = discord.utils.find(lambda g: g.id == guildID, self.guilds)
        logChannel = guild.get_channel(await self.db.getLoggingChannel(guildID, channelType.log))
        member = guild.get_member(payload.user_id)
        if member is None:
            if logChannel is not None:
                await logChannel.send(f'Unable to find <@{payload.user_id}> in the server')
            return
        if payload.channel_id == roleChannel:
            poll = await self.db.retrievePoll(payload.message_id, guildID)
            try:
                roleID = poll[Emoji.demojize(str(payload.emoji))]
            except KeyError:
                if logChannel is not None:
                    await logChannel.send('Code error in on_raw_reaction_add')
                return
            role = discord.utils.get(guild.roles, id=roleID)
            if role is None:
                if logChannel is not None:
                    await logChannel.send('Unable to find role associated with role poll!')
                return
            await member.add_roles(role)
        if payload.message_id == await self.db.getPoliMsg(payload.guild_id):
            role = discord.utils.get(guild.roles, name='politics')
            if role is None:
                return
            try:
                banned = await self.db.checkPoliBan(payload.guild_id, payload.user_id)
            except EE_DB_Exception:
                banned = False
            rank, _ = await self.db.checkUserLevel(payload.user_id, payload.guild_id)
            if banned or rank <= 3:
                return
            await member.add_roles(role)

    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        guildID = payload.guild_id
        if guildID is None:
            return
        guild = discord.utils.get(self.guilds, id=guildID)
        member = discord.utils.get(guild.members, id=payload.user_id)
        logChannel = guild.get_channel(await self.db.getLoggingChannel(guildID, channelType.log))
        roleChannel = await self.db.getLoggingChannel(guildID, channelType.role)
        poliRulesChannel = await self.db.getPoliChannel(guildID)
        if member is None:
            if logChannel is not None:
                await logChannel.send(f'Unable to find <@{payload.user_id} in the server!')
            return
        if member.bot:
            return
        if payload.channel_id != roleChannel and payload.channel_id != poliRulesChannel:
            return
        if payload.channel_id == roleChannel:
            poll = await self.db.retrievePoll(payload.message_id, guildID)
            try:
                roleID = poll[Emoji.demojize(str(payload.emoji))]
            except KeyError:
                if logChannel is not None:
                    await logChannel.send('Code error in on_raw_reaction_remove')
                return
            role = discord.utils.get(guild.roles, id=roleID)
            if role is None:
                if logChannel is not None:
                    await logChannel.send('Unable to find role associated with role poll!')
                return
            await member.remove_roles(role)
        if payload.message_id == await self.db.getPoliMsg(guildID):
            role = discord.utils.get(guild.roles, name='politics')
            if role is None:
                return
            await member.remove_roles(role)

    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        guildID = payload.guild_id
        if guildID is None:
            return
        guild = self.get_guild(guildID)
        channelID = payload.channel_id
        channel = discord.utils.get(guild.channels, id=channelID)
        if channel is None:
            return
        if await self.db.getLoggingChannel(guildID, channelType.role) == channel.id:
            await self.db.deletePoll(payload.message_id, guildID)
            return

    async def on_member_join(self, member: discord.Member):
        if member.bot:
            return
        log = self.get_channel(await self.db.getLoggingChannel(member.guild.id, channelType.log))
        if log is None:
            return
        await log.send(f'<@{member.id}> just joined the server')

    async def on_member_remove(self, member: discord.Member):
        if member.bot:
            return
        log = self.get_channel(await self.db.getLoggingChannel(member.guild.id, channelType.log))
        if log is None:
            return
        await log.send(f'<@{member.id}> was removed from the server.')

    async def on_guild_join(self, guild: discord.Guild):
        await self.db.addGuild(guild.id)
        await self._updateFlags(guild.id)
        users = [user.id for user in guild.members if not user.bot]
        await self.db.updateGuildUsers(guild.id, users)
        await guild.owner.send(f'Thank you for adding me to {guild.name}. Please use the `/configure` command to set '
                               f'up the log, role opt in, level notification, and monitoring channels. By failing to '
                               f'do so, you will miss out on important messages. This is should be the only direct '
                               f'message that you will receive from me as long as you do not get banned from any '
                               f'servers that I am also part of.')

    @staticmethod
    async def on_thread_create(thread: discord.Thread):
        await thread.join()

    async def on_disconnect(self):
        if self.is_closed():
            return
        if self._retries > 5:
            await self.close()
            return
        self._retries += 1

    async def on_invite_create(self, invite: discord.Invite):
        creator = invite.inviter
        guild = invite.guild
        if guild is None:
            return

        if await self.db.checkMonitor(guild.id, creator.id):
            await invite.delete(reason='Keeping malicious user from creating invites')
        return


async def run_bot() -> bool:
    login = load_login('./login.json')
    if LAUNCH:
        active_db = LAUNCH_DATABASE
        botToken = os.getenv('BOT_TOKEN')
    else:
        active_db = TEST_DATABASE
        botToken = login['test_bot_key']

    openaiKey = login['openai_key']

    intents = discord.Intents.all()

    internet = False
    while not internet:
        try:
            urllib.request.urlopen('http://google.com')
            internet = True
        except urllib.error.URLError:
            print('No internet!')
            await asyncio.sleep(15)

    if LAUNCH:
        run_once()

    logger = logging.getLogger('discord')
    logger.setLevel(logging.ERROR)

    handler = logging.handlers.RotatingFileHandler(
        filename='discord.log',
        encoding='utf-8',
        maxBytes=32 * 1024 * 1024,  # 32 MiB
        backupCount=5,  # Rotate through 5 files
    )
    dt_fmt = '%Y-%m-%d %H:%M:%S'
    formatter = logging.Formatter('[{asctime}] [{levelname:<8}] {name}: {message}', dt_fmt, style='{')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    async with EE_DB(login['database_username'], login['database_password'], active_db) as db:
        await db.initialize_tables()
        async with EE_Bot(database=db, MASTER_ID=MASTER, key_file=KEY_FILE_LOCATION, version=__version__,
                          disable_database=DISABLE_DATABASE, intents=intents, openai=openaiKey,
                          tree_cls=EE_Bot_Command_Tree) as bot:
            await bot.start(botToken)
            reconnect = await bot.reboot()
            return reconnect


def run() -> bool:
    return asyncio.run(run_bot())


if __name__ == '__main__':
    run()
