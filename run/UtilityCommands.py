# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import File, app_commands
from discord.ext import commands, tasks
from typing import Literal, Optional
from EE_Bot_DB import EE_DB, channelType, cogEnum
import EE_Bot_help as bot_help
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseUpload
from oauth2client.service_account import ServiceAccountCredentials
import pyAesCrypt as AES
from inspect import currentframe, getframeinfo
import traceback
import re
import io
import datetime
from subprocess import Popen, PIPE
import globals as GL

utc = datetime.timezone.utc

times = [
    datetime.time(hour=0, minute=0, second=0, tzinfo=utc),
    datetime.time(hour=12, minute=0, second=0, tzinfo=utc)
]


class Utility(commands.Cog):
    def __init__(self, bot, launchFlag: bool):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db
        self.launched = launchFlag
        self.key_file_location = bot.key_file
        GL.InitLaunch()
        self._released = GL.launchFlag
        return

    def run_db_backup(self):
        if self._released:
            scopes = ['https://www.googleapis.com/auth/drive']

            # Load credentials
            try:
                credentials = ServiceAccountCredentials.from_json_keyfile_name(self.key_file_location, scopes=scopes)
            except Exception as err:
                frameinfo = getframeinfo(currentframe())
                print(f'{frameinfo.filename}:{frameinfo.lineno}: {err}')
                return

            # Log into account
            service = build('drive', 'v3', credentials=credentials)

            # Get files from Google Drive
            results = service.files().list(fields="nextPageToken, files(id, name)").execute()
            items = results.get('files', [])
            index = -1
            minIndex = 0
            minIndexID = ''
            firstEntry = True

            if not items:
                pass
            else:
                for item in items:
                    if 'ee_bot_production' in item['name']:
                        result = re.sub('[^0-9]', '', item['name'])
                        result = int(result)
                        if result < minIndex or firstEntry:
                            minIndex = result
                            minIndexID = item['id']
                            firstEntry = False
                        if result > index:
                            index = result
            index += 1

            # Create file metadata
            file_meta = {'name': f'ee_bot_production_{index}.sql.aes',
                         'parents': ['1J9tehWK-WtSOQyz3lxpHtBR7YQKQW3VE']}

            process = Popen('pg_dump ee_bot_production', stdout=PIPE, shell=True)
            data_dump, _ = process.communicate()

            with open('./encryption.txt', 'r') as f:
                password = f.readline()

            backup = io.BytesIO(bytes(data_dump))
            backupEncrypted = io.BytesIO()

            bufferSize = 64 * 1024

            del data_dump

            AES.encryptStream(backup, backupEncrypted, password.strip(), bufferSize)
            backupEncrypted.seek(0)

            del backup
            del password

            # Upload file
            media = MediaIoBaseUpload(backupEncrypted, mimetype='application/octet-stream', resumable=True)
            file = service.files().create(body=file_meta, media_body=media, fields='id').execute()
            # Print ID to log file
            print(f"Uploaded Database Backup; File ID: {file.get('id')}")

            # Delete old backup
            index -= 20
            if minIndex < index:
                # Delete file by ID
                service.files().delete(fileId=minIndexID).execute()
                print(f'Deleted Database Backup; File ID: {minIndexID}')
            del backupEncrypted

    @tasks.loop(time=times)
    async def backupDB(self):
        self.run_db_backup()
        return

    async def cog_load(self) -> None:
        print('Utility Cog Loaded!')
        self.backupDB.start()
        return

    async def cog_unload(self) -> None:
        print('Utility Cog Unloaded!')
        self.backupDB.cancel()
        return

    @commands.command(description='Terminates execution of bot.')
    async def kill(self, ctx: commands.Context):
        if ctx.guild is None:
            return
        if ctx.author.id == self.MASTER:
            log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild.id, channelType.log))
            if log is not None:
                await log.send('Killing bot...')
            await self.bot.kill(False)
        else:
            with open('./images/EasterEggs/kill.jpg', 'rb') as f:
                await ctx.channel.send(file=File(f, 'kill.jpg'))

    @commands.command(description='Reboots bot')
    async def reboot(self, ctx: commands.Context):
        if ctx.guild is None:
            return
        if ctx.author.id == self.MASTER:
            log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild.id, channelType.log))
            if log is not None:
                await log.send('Rebooting bot...')
            await self.bot.kill()
        return

    @commands.command()
    @commands.guild_only()
    @commands.is_owner()
    async def sync(self,
                   ctx: commands.Context,
                   guilds: commands.Greedy[discord.Object],
                   spec: Optional[Literal['update', 'add', 'clear']] = None) -> None:
        if not guilds:
            if spec == 'update':
                synced = await ctx.bot.tree.sync(guild=ctx.guild)
            elif spec == 'add':
                ctx.bot.tree.copy_global_to(guild=ctx.guild)
                synced = await ctx.bot.tree.sync(guild=ctx.guild)
            elif spec == 'clear':
                ctx.bot.tree.clear_commands(guild=ctx.guild)
                await ctx.bot.tree.sync(guild=ctx.guild)
                synced = []
            else:
                synced = await ctx.bot.tree.sync()
            await ctx.send(f"Synced {len(synced)} commands {'globally' if spec is None else 'to the current guild.'}")
            return

        ret = 0
        for guild in guilds:
            try:
                await ctx.bot.tree.sync(guild=guild)
            except discord.HTTPException:
                pass
            else:
                ret += 1

        await ctx.send(f'Synced the tree to {ret}/{len(guilds)} guilds.')
        return

    @commands.command()
    @commands.is_owner()
    async def force_backup(self,
                           ctx: commands.Context) -> None:
        if ctx.author.id != self.MASTER:
            return
        try:
            self.run_db_backup()
            await ctx.send('Database forcefully backed up')
        except Exception as err:
            await ctx.send(f'Backup failed: {str(err)}, Traceback: {traceback.format_exc()}')
        return

    @commands.hybrid_command(description='Mark a forum thread as solved')
    @commands.guild_only()
    async def solved(self, ctx: commands.Context):
        # Check if channel is a thread and if the parent channel is a forum
        if (ctx.channel.type != discord.ChannelType.public_thread) or \
            (ctx.channel.parent.type != discord.ChannelType.forum):
            await ctx.send('You need to be in a forum channel to run this command!', ephemeral=True)
            return
        # Check if author is the original poster or an admin
        if ctx.channel.owner_id != ctx.author.id and not ctx.author.guild_permissions.administrator:
            if ctx.interaction is None:
                await ctx.message.delete()
            else:
                await ctx.send('You do not have permission to do this!', ephemeral=True)
            return
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild.id, channelType.log))
        tags = ctx.channel.parent.available_tags
        # Get the tag names into a list
        tag_names = [tag.name.lower() for tag in tags]
        # Check for tags named solved or resolved
        if 'solved' not in tag_names and 'resolved' not in tag_names:
            await ctx.send('Forum does not contain solved or resolved tag!', ephemeral=True)
            if log is not None:
                await log.send(f'Missing resolved or solved tag in <#{ctx.channel.parent.id}>')
            return
        # Get solved tags and apply them to thread
        solvedTags = [tag for tag in tags if tag.name.lower() == 'solved' or tag.name.lower() == 'resolved']
        await ctx.channel.add_tags(*solvedTags)
        # Respond to interaction if slash command was used
        if ctx.interaction is not None:
            await ctx.send('Marked thread as solved')
        return

    @app_commands.command(name='help', description='Get more information for commands.')
    async def Help(self, ctx: discord.Interaction, spec: Optional[str] = None):
        await bot_help.help_cmd(ctx, self.bot.user.display_avatar.url, spec, self.db)

    @Help.autocomplete('spec')
    async def help_autocomplete_callback(self, ctx: discord.Interaction, current: str):
        if ctx.guild is None:
            options = []
            for category in bot_help.CATEGORIES:
                if category.guild_only:
                    continue
                options.append(category)
                options += category.subcommands
        else:
            options = []
            for category in bot_help.CATEGORIES:
                if category.command_group != cogEnum.NONE:
                    if not await self.db.checkCog(ctx.guild_id, category.command_group):
                        continue
                    if not category.enabled:
                        continue
                    if ctx.guild.owner_id != ctx.user.id and category.owner_only:
                        continue
                options.append(category)
                options += [subcommand for subcommand in category.subcommands
                            if ((type(subcommand) is app_commands.Choice) or
                                (not subcommand.admin) or ctx.user.guild_permissions.administrator)]

        return [cmd for cmd in options if cmd.name.startswith(current)][:25]


async def setup(bot):
    GL.InitLaunch()
    await bot.add_cog(Utility(bot=bot, launchFlag=GL.launchFlag))
