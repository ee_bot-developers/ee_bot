# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import random
from typing import Optional, Tuple
import _veryEasyQuiz as Ve
import _EasyQuiz as Ez
from quizUtils import QuestionMetaData, Units, str2complex
import re
import globals
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import six


SI_PREFIX = {
    'p': 1e-12,
    'n': 1e-9,
    'u': 1e-6,
    'm': 1e-3,
    'k': 1e3,
    'M': 1e6,
    'G': 1e9
}


_VeryEasy = {'load': Ve.load_very_easy, 'questions': Ve.Questions, 'free': Ve.freeVeryEasy}
_Easy = {'load': Ez.load_easy, 'questions': Ez.Questions, 'free': Ez.freeEasy}

_QUESTION_DATA = {
    'very easy': _VeryEasy,
    'easy': _Easy,
    'normal': None,
    'hard': None,
    'very hard': None
}


class QuizLogic:
    def __init__(self, difficulty, userID: int):
        # Initialize data members
        self.userID = userID
        self._difficulty = difficulty
        self._questions = []
        self._score = 0
        self._index = 0

        # load questions, answers, and margins based on difficulty...
        if difficulty not in _QUESTION_DATA.keys():
            raise ValueError(f'Invalid difficulty: {difficulty}')
        try:
            _QUESTION_DATA[difficulty]['load']()
            Questions = _QUESTION_DATA[difficulty]['questions']
        except TypeError:
            raise NotImplementedError(f'The {difficulty} difficulty is still under construction')
        keys = list(Questions.keys())

        while len(self._questions) < 5:
            key = random.choice(keys)
            question, file, answer, margin, units = Questions[key]()
            self._questions.append(QuestionMetaData(question=question,
                                                    file=file,
                                                    answer=answer,
                                                    margin=margin,
                                                    units=units))
            keys.remove(key)

        _QUESTION_DATA[difficulty]['free']()

    # Function that processes user's answer
    def ans(self, userAnswer: str) -> Tuple[bool, Optional[str]]:
        # Check answer type
        if isinstance(self._questions[self._index].answer, (int, float)):
            # Create regex statements to sort out numbers and units
            dec = re.compile(r'[^\d.-]+')
            units = re.compile(r'[0-9. ]+')
            # Get number as a string
            num = dec.sub('', userAnswer)

            # Convert number into float or integer
            if isinstance(self._questions[self._index].answer, int):
                try:
                    num = int(num)
                except ValueError:
                    return False, 'Cannot process answer. Please try a different answer.'
            else:
                try:
                    num = float(num)
                except ValueError:
                    return False, "Unable to process answer, multiple instances of '.' or '-' occurred."

            # Retrieve units from string
            userUnits = units.sub('', userAnswer)
            if userUnits == '-':
                # Get rid of '-' symbol
                userUnits = userUnits.strip('-')

            # Check for prefix
            try:
                multiple = SI_PREFIX[userUnits[0]]
                userUnits = userUnits[1:]
            except KeyError:
                multiple = 1
            except IndexError:
                multiple = 1

            units = Units(userUnits)

            # Check if units are correct
            if units == self._questions[self._index].units:
                num *= multiple  # Adjust numerical part to fit the base
                # Check if numerical answer is in range
                if (self._questions[self._index].answer - self._questions[self._index].margin) <= num <= \
                        (self._questions[self._index].answer + self._questions[self._index].margin):
                    self._score += 1  # Add to score
                    self._questions[self._index].correct = True
        # Check for string type answer
        elif isinstance(self._questions[self._index].answer, str):
            # Check string lengths
            if userAnswer.lower() == self._questions[self._index].answer.lower():
                self._score += 1
                self._questions[self._index].correct = True
        # Check for complex answer
        elif isinstance(self._questions[self._index].answer, complex):
            units = re.compile(r'[0-9.i\+j\- ]+')
            userUnits = units.sub('', userAnswer)
            userAnswer = userAnswer.replace(userUnits, '')
            num = str2complex(userAnswer)
            if num is None:
                return False, 'Unable to process answer, please try reformatting your answer.'

            # Check for prefix
            try:
                multiple = SI_PREFIX[userUnits[0]]
                userUnits = userUnits[1:]
            except KeyError:
                multiple = 1

            units = Units(userUnits)

            # Check if units are correct
            if units == self._questions[self._index].units:
                num *= multiple  # Adjust numerical part to fit the base
                # Check if numerical answer is in range
                if (self._questions[self._index].answer.real - self._questions[self._index].margin.real) <= num.real \
                        <= (self._questions[self._index].answer.real + self._questions[self._index].margin.real):
                    if (self._questions[self._index].answer.imag - self._questions[self._index].margin.imag) <= \
                            num.imag <= (self._questions[self._index].answer +
                                         self._questions[self._index].margin.imag):
                        self._score += 1  # Add to score
                        self._questions[self._index].correct = True
        # Check if user answer is in an answer bank
        else:
            # Check if exact user answer is in list or if lowercase user answer is in the list
            if (userAnswer in self._questions[self._index].answer) or \
                    (userAnswer.lower() in self._questions[self._index].answer):
                # Add to score
                self._score += 1
                self._questions[self._index].correct = True
            # Set first item as correct answer
            self._questions[self._index].answer = self._questions[self._index].answer[0]

        # append user answer to list
        self._questions[self._index].UserAnswer = userAnswer
        # adjust index
        self._index += 1
        # return success
        return True, None

    # Function that retrieves the current question
    def nextQuestion(self) -> Tuple[Optional[str], Optional[str], Optional[int]]:
        # Check if index is in range
        if self._index < 5:
            # return the question, file, and question number
            return self._questions[self._index].question, self._questions[self._index].file, (self._index + 1)
        else:
            return None, None, None

    # Function that checks if results are ready to be shown
    def showResults(self) -> bool:
        return self._index == 5

    def render_table(self, col_width=3.0, row_height=0.625, font_size=14,
                     header_color=globals.sEmbedColor, row_colors=('#f1f1f2', 'w'),
                     edge_color='w', bbox=(0, 0, 1, 1), header_columns=0, ax=None,
                     **kwargs):
        # Loop through answers and limit them to 6 decimal places
        for i, question in enumerate(self._questions):
            if isinstance(question.answer, float):
                self._questions[i].answer = f'{self._questions[i].answer:.6f}'.rstrip('0')
        # Create dataframe for image generation
        data = pd.DataFrame()
        data['Question'] = list(range(1, 6))
        data['User Answer'] = [question.UserAnswer for question in self._questions]
        data['Answer'] = [question.answer for question in self._questions]
        data['Units'] = [question.units.value for question in self._questions]
        data['Error Allowed'] = [question.margin for question in self._questions]
        data['Correct'] = [question.correct for question in self._questions]
        fig = None

        # Generate the image
        if ax is None:
            size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
            fig, ax = plt.subplots(figsize=size)
            ax.axis('off')

        mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)

        mpl_table.auto_set_font_size(False)
        mpl_table.set_fontsize(font_size)

        for k, cell in six.iteritems(mpl_table._cells):
            cell.set_edgecolor(edge_color)
            if k[0] == 0 or k[1] < header_columns:
                cell.set_text_props(weight='bold', color='w')
                cell.set_facecolor(header_color)
            else:
                cell.set_facecolor(row_colors[k[0] % len(row_colors)])

        return self._score, fig
