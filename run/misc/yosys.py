# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from subprocess import Popen
from subprocess import PIPE


def parse(filename: str = ''):
    modules = []
    modDef = False
    if filename != '':
        with open(filename, 'r') as f:
            for line in f:
                Line = line.split(' ')
                for x in Line:
                    if x == 'module':
                        modDef = True
                    elif modDef:
                        modDef = False
                        modules.append(remover(x, list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")))
    return modules


def remover(my_string="", values=[]):
    for item in my_string:
        if item not in values:
            my_string = my_string.replace(item, "")
    return my_string


def gen_script_preSynth(modules, filename):
    commands = ['hierarchy -check\n', 'proc\n', 'opt\n', 'fsm\n', 'opt\n']
    if len(modules) == 1:
        commands.append('show -prefix %s -format jpg\n' % filename)
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    elif len(modules) > 1:
        for x in modules:
            commands.append('select -module %s\n' % x)
            commands.append('show -prefix %s_%s -format jpg\n' % (filename, x))
            commands.append('select -clear\n')
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    else:
        return 'No modules defined'
    return ''


def gen_script_synth(modules, filename):
    commands = ['synth\n']
    if len(modules) == 1:
        commands.append('show -prefix %s -format jpg\n' % filename)
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    elif len(modules) > 1:
        for x in modules:
            commands.append('select -module %s\n' % x)
            commands.append('show -prefix %s_%s -format jpg\n' % (filename, x))
            commands.append('select -clear\n')
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    else:
        return 'No modules defined'
    return ''


def gen_script_tech(modules, filename, tech):
    commands = None
    topModule = ''
    if tech == 'xilinx':
        commands = ['synth_xilinx -flatten\n']
    else:
        commands = ['synth_%s\n' % tech]
    if len(modules) == 1:
        commands.append('show -prefix %s -format jpg\n' % filename)
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    elif len(modules) > 1:
        #for x in modules:
        #    commands.append('select -module %s\n' % x)
        #    commands.append('show -prefix %s_%s -format jpg\n' % (filename, x))
        #    commands.append('select -clear\n')
        topModule = getTopModule(filename)
        if topModule == '':
            return 'An error occured'
        commands.append('select -module %s\n' % topModule)
        commands.append('show -prefix %s_%s -format jpg\n' % (filename, topModule))
        commands.append('select -clear\n')
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    else:
        return 'No modules defined'
    return '', topModule


def getTopModule(filename):
    L = ['yosys', '-s', './yosys/getTop.sh', './%s' % filename]
    process = Popen(L, stdout=PIPE, universal_newlines=True)
    output, errors = process.communicate()

    if errors is None:
        output = str(output)
        output = output.replace('\n', ' ')
        output = output.split(' ')

        topModule = ''

        for i in range(0, len(output)):
            if output[i] == '1':
                if output[i+1] == 'modules:':
                    k = 2
                    while output[i+k] == '':
                        k += 1
                    topModule = output[i+k]
                    break
        return topModule
    return ''


def genSpiceScript(filename, modules):
    commands = ['hierarchy -check\n', 'proc\n', 'opt\n', 'fsm\n', 'opt\n']
    if len(modules) >= 1:
        commands.append('write_spice %s.sp\n' % filename)
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    else:
        return 'No modules defined'
    return ''


def genJSON_script(filename, modules):
    commands = ['hierarchy -check\n', 'proc\n', 'opt\n', 'fsm\n', 'opt\n']
    if len(modules) >= 1:
        commands.append('write_json %s.json\n' % filename)
        with open('yosys/yosys.sh', 'w') as f:
            f.writelines(commands)
    else:
        return 'No modules defined'
    return ''


def run_command(command, filename):
    modules = parse(filename)
    Errors = ''
    top = ''

    if command == 'pre-synth':
        Errors = gen_script_preSynth(modules, filename)
    elif command == 'synth':
        Errors = gen_script_synth(modules, filename)
    elif command == 'achronix' or command == 'anlogic' or command == 'coolrunner2' or command == 'easic' or command == 'ecp5' or command == 'gowin' or command == 'greenpak4' or command == 'ice40' or command == 'intel' or command == 'sf2' or command == 'xilinx':
        Errors, top = gen_script_tech(modules, filename, command)
    elif command == 'spice':
        Errors = genSpiceScript(filename, modules)
    elif command == 'json':
        Errors = genJSON_script(filename, modules)
    else:
        Errors = 'Not a valid command'
    return modules, Errors, top
