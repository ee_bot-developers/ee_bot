# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import discord
from discord import app_commands
from discord.ext import commands
from typing import Literal, Optional
from EE_Bot_DB import EE_DB, cogEnum
from subprocess import Popen, PIPE
import threading
import asyncio
import os


powerExists = os.path.isdir('power')


class Power(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db: EE_DB = bot.db
        self.MASTER = bot.MASTER
        return

    async def cog_load(self) -> None:
        print('Power Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        print('Power Cog Unloaded!')
        return

    async def _isEnabled(self, ctx: discord.Interaction) -> bool:
        if ctx.guild_id is None:
            return True
        if not await self.db.checkCog(ctx.guild_id, cogEnum.Power):
            await ctx.response.send_message('This command has been disabled.')
            return False
        return True

    @staticmethod
    def runCommand(args, mutex, outputDict):
        process = Popen(args, stderr=PIPE, stdout=PIPE, universal_newlines=True)
        output, error = process.communicate()

        mutex.acquire()
        outputDict['output'] = output
        outputDict['error'] = error
        outputDict['done'] = True
        mutex.release()
        return

    @app_commands.command(description='Calculates power')
    @app_commands.describe(
        calculation='Type of power calculation',
        voltage='The voltage supplied to the circuit in Volts',
        current='The current supplied to the circuit in Amps',
        resistance="The circuit's resistance in Ohms",
        reactance="The circuit's reactance in Ohms"
    )
    async def power(self,
                    ctx: discord.Interaction,
                    calculation: Literal['Real', 'Reactive', 'Apparent', 'Power'],
                    voltage: Optional[float],
                    current: Optional[float],
                    resistance: Optional[float],
                    reactance: Optional[float]):
        if not await self._isEnabled(ctx):
            return

        L = ['power/power.out', calculation.lower()]

        if voltage is not None:
            L.append('--voltage')
            L.append(str(voltage))
        if current is not None:
            L.append('--current')
            L.append(str(current))
        if resistance is not None:
            L.append('--resistance')
            L.append(str(resistance))
        if reactance is not None:
            L.append('--reactance')
            L.append(str(reactance))

        await ctx.response.defer()
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        powerThread = threading.Thread(target=self.runCommand, args=(L, mutex, outputDict))
        powerThread.start()

        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        msg = ''
        if outputDict['output']:
            msg += f"```{outputDict['output']}```"
        if outputDict['error']:
            msg += f"```diff\n- {outputDict['error']}```"
        await ctx.followup.send(msg)
        return

    @commands.command(description='Calculates line to phase and vice versa')
    @app_commands.describe(
        configuration='The current circuit configuration',
        line_voltage='The line voltage in Volts',
        phase_voltage='The phase voltage in Volts',
        line_current='The line current in Amps',
        phase_current='The phase current in Amps'
    )
    async def line_phase(self,
                         ctx: discord.Interaction,
                         configuration: Literal['delta', 'star', 'wye'],
                         line_voltage: Optional[float],
                         phase_voltage: Optional[float],
                         line_current: Optional[float],
                         phase_current: Optional[float]):
        if not await self._isEnabled(ctx):
            return

        L = ['power/power.out', 'line-phase', f'--{configuration}']

        if line_voltage is not None:
            L.append('--line-voltage')
            L.append(str(line_voltage))
        if phase_voltage is not None:
            L.append('--phase-voltage')
            L.append(str(phase_voltage))
        if line_current is not None:
            L.append('--line-current')
            L.append(str(line_current))
        if phase_current is not None:
            L.append('--phase-current')
            L.append(str(phase_current))

        await ctx.followup.defer()
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        powerThread = threading.Thread(target=self.runCommand, args=(L, mutex, outputDict))
        powerThread.start()

        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        msg = ''
        if outputDict['output']:
            msg += f"```{outputDict['output']}```"
        if outputDict['error']:
            msg += f"```diff\n- {outputDict['error']}```"
        await ctx.followup.send(msg)
        return

    @app_commands.command(description='Calculates the reactance')
    @app_commands.describe(
        frequency='The frequency that the AC circuit is operating at. Uses Hz as default',
        frequency_units='The units that the frequency is in',
        inductance='The inductance of the circuit in Henries',
        capacitance='The capacitance of the circuit in Farads'
    )
    async def reactance(self,
                        ctx: discord.Interaction,
                        frequency: float,
                        frequency_units: Optional[Literal['Hz', 'Rad/s']],
                        inductance: Optional[float],
                        capacitance: Optional[float]):
        if not await self._isEnabled(ctx):
            return

        L = ['power/power.out', 'reactance', '--frequency', str(frequency)]

        if frequency_units is not None:
            if frequency_units == 'Rad/s':
                frequency_units = 'rads'
            L.append(frequency_units)
        if inductance is not None:
            L.append('--inductance')
            L.append(str(inductance))
        if capacitance is not None:
            L.append('--capacitance')
            L.append(str(capacitance))

        await ctx.response.defer()
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        powerThread = threading.Thread(target=self.runCommand, args=(L, mutex, outputDict))
        powerThread.start()

        mutex.acquire()
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        msg = ''
        if outputDict['output']:
            msg += f"```{outputDict['output']}```"
        if outputDict['error']:
            msg += f"```diff\n- {outputDict['error']}```"
        await ctx.followup.send(msg)
        return


async def setup(bot):
    if powerExists:
        await bot.add_cog(Power(bot=bot))
