# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import app_commands, File, HTTPException, Forbidden, NotFound, Embed
from discord.ext import commands
from typing import Literal, Optional, List, Union
import emoji as Emoji
from EE_Bot_DB import EE_DB, cogEnum, channelType, EE_DB_Exception
from YosysCommands import YS
from Modals.banMsgModal import BanMsgModal
from Modals.guildSettings import guildSettingsModal
import re
import io
import csv
from PIL import Image
import globals as GL
from imageGeneration import latex_engine
import validators
import enum

embedColor = GL.embedColor
cogs = tuple([x.name for x in cogEnum][1:-2])


class editRemovalActions(enum.Enum):
    IMAGE = 'Remove image'
    DESCRIPTION = 'Remove description'
    URL = 'Remove URL'
    NOTHING = None


@app_commands.guild_only()
class serverConfig(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.MASTER = bot.MASTER
        self.db: EE_DB = bot.db
        return

    async def cog_load(self) -> None:
        print('Server Configs Cog Loaded!')
        return

    async def cog_unload(self) -> None:
        print('Server Configs Cog Unloaded!')
        return

    async def _isEnabled(self,
                         ctx: discord.Interaction,
                         configured: bool = True) -> bool:
        if ctx.user.id != ctx.guild.owner_id:
            with open('./images/EasterEggs/adminPermissions.png', 'rb') as f:
                await ctx.response.send_message(file=File(f, 'adminPermissions.png'))
            return False
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        if log is None and configured:
            await ctx.response.send_message('Error: Please set your logging channels first! See `/help configure`.')
            return False
        return True

    @app_commands.command(description='Configure server channels')
    @app_commands.describe(
        log='The bot logging channel',
        role='The role opt in channel',
        level='The level notification channel',
        monitor='The user monitoring channel',
        shaming='The server shaming channel'
    )
    async def configure(self,
                        ctx: discord.Interaction,
                        log: discord.TextChannel,
                        role: discord.TextChannel,
                        level: discord.TextChannel,
                        monitor: discord.TextChannel,
                        shaming: Optional[discord.TextChannel]) -> None:
        if not await self._isEnabled(ctx, False):
            return

        log = log.id
        role = role.id
        level = level.id
        monitor = monitor.id
        if shaming is not None:
            shaming = shaming.id
        else:
            shaming = 0
        await self.db.configureGuildChannels(ctx.guild_id, log, role, level, monitor, shaming)
        log = ctx.guild.get_channel(log)
        await log.send('Configuration successful')
        await ctx.response.send_message('Done', ephemeral=True)
        return

    @staticmethod
    async def test_channel(channel: discord.TextChannel,
                           channel_type: str,
                           results: dict) -> None:
        try:
            msg = await channel.send('Pinging channel')
            results[f'{channel_type} channel send message'] = 'Success'
            try:
                await msg.delete()
                results[f'{channel_type} channel delete message'] = 'Success'
            except Forbidden as err:
                results[f'{channel_type} channel delete message'] = err.text
            except NotFound as err:
                results[f'{channel_type} channel delete message'] = err.text
            except HTTPException as err:
                results[f'{channel_type} channel delete message'] = err.text
        except Forbidden as err:
            results[f'{channel_type} channel send message'] = err.text
        except HTTPException as err:
            results[f'{channel_type} channel send message'] = err.text
        except AttributeError:
            results[f'{channel_type} channel send message'] = 'Channel not found'
        return

    @app_commands.command(description='Test the server configurations')
    async def config_test(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()
        Actions = {
            'Log channel send message': None,
            'Log channel delete message': None,
            'Level channel send message': None,
            'Level channel delete message': None,
            'Role channel send message': None,
            'Role channel delete message': None,
            'Monitor channel send message': None,
            'Monitor channel delete message': None,
            'Shame channel send message': None,
            'Shame channel delete message': None
        }
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        level = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.level))
        role = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.role))
        monitor = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.admin))
        shame = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.shame))
        await self.test_channel(log, 'Log', Actions)
        await self.test_channel(level, 'Level', Actions)
        await self.test_channel(role, 'Role', Actions)
        await self.test_channel(monitor, 'Monitor', Actions)
        await self.test_channel(shame, 'Shame', Actions)
        embed = Embed(color=embedColor, title='Logging Channel tests')
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        for x in Actions.keys():
            if Actions[x] is not None:
                embed.add_field(name=x, value=Actions[x], inline=False)
        await ctx.followup.send(embed=embed, ephemeral=True)
        return

    @app_commands.command(description='Turn on or off the level system')
    async def level_system(self,
                           ctx: discord.Interaction,
                           status: Literal['on', 'off']):
        if not await self._isEnabled(ctx, False):
            return
        await self.db.toggleLevelSystem(ctx.guild_id, status == 'on')
        await ctx.response.send_message(f'Server level system is now turned {status}.', ephemeral=True)
        return

    @app_commands.command(description='Check status of level system')
    async def check_level_system(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx, False):
            return
        status = await self.db.checkLevelSystem(ctx.guild_id)
        if status:
            status = 'on'
        else:
            status = 'off'
        await ctx.response.send_message(f'The level system is {status}', ephemeral=True)
        return

    @app_commands.command(description='Enable or disable groups of commands')
    async def toggle_cog(self,
                         ctx: discord.Interaction,
                         action: Literal['enable', 'disable'],
                         category: Literal[cogs]):
        if not await self._isEnabled(ctx):
            return
        if action == 'enable':
            await self.db.enableCog(ctx.guild_id, cogEnum[category])
        else:
            await self.db.disableCog(ctx.guild_id, cogEnum[category])
        await ctx.response.send_message(f'The {category} category is now {action}', ephemeral=True)
        return

    @app_commands.command(description='Check the cogs that are enabled and disabled')
    async def check_cog_status(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx, False):
            return
        embed = discord.Embed(title='Command Category Status', description='', color=embedColor)
        embed.set_thumbnail(url=self.bot.user.display_avatar.url)
        value = {True: 'ENABLED', False: 'DISABLED'}
        for x in cogs:
            enabled = await self.db.checkCog(ctx.guild_id, cogEnum[x])
            if not YS and cogEnum[x] == cogEnum.Yosys:
                enabled = False
            embed.add_field(name=x, value=f'Status: **{value[enabled]}**')
        await ctx.response.send_message(embed=embed, ephemeral=True)
        return

    @app_commands.command(description='Set a custom ban message. It is recommended you include a link to an appeal '
                                      'form.')
    async def ban_msg(self,
                      ctx: discord.Interaction,
                      action: Literal['set', 'clear']):
        if not await self._isEnabled(ctx):
            return
        if action == 'set':
            banMsg = await self.db.getBanMsg(ctx.guild_id)
            modal = BanMsgModal(self.db, ctx.guild_id, banMsg)
            await ctx.response.send_modal(modal)
            await modal.wait()
        else:
            await self.db.clearBanMsg(ctx.guild_id)
            await ctx.response.send_message('Successfully cleared ban message')
        return

    @app_commands.command(description='Creates an easter egg for the server')
    @app_commands.describe(
        phrase='Activation phrase',
        file='The attachment that gets displayed',
        limited='Limit the Easter egg'
    )
    async def create_easter_egg(self,
                                ctx: discord.Interaction,
                                phrase: str,
                                file: discord.Attachment,
                                limited: Literal['yes', 'no']):
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()
        if file.size > GL.MAX_FILE_SIZE:
            await ctx.followup.send(f'Error: File must be less than 4 MiB! Given file size: '
                                    f'{file.size / (1024 * 1024)} MiB', ephemeral=True)
            return
        if len(await self.db.fetchEasterEggs(ctx.guild_id)) == 20:
            await ctx.followup.send('Error: The server cannot have more than 20 Easter eggs!', ephemeral=True)
            return
        content = await file.read()
        try:
            await self.db.createEasterEgg(ctx.guild_id, phrase, limited == 'yes', file.filename, content)
        except EE_DB_Exception:
            await ctx.followup.send('Error: Easter Egg already exists!', ephemeral=True)
            return
        await ctx.followup.send('Easter Egg added!', ephemeral=True)
        return

    @app_commands.command(description='Removes an Easter egg from the server')
    @app_commands.describe(
        phrase='The Easter egg activation phrase'
    )
    async def remove_easter_egg(self,
                                ctx: discord.Interaction,
                                phrase: str):
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()
        try:
            await self.db.removeEasterEgg(ctx.guild_id, phrase)
        except EE_DB_Exception:
            await ctx.followup.send('Error: Easter Egg does not exist!', ephemeral=True)
            return
        await ctx.followup.send('Easter Egg removed!', ephemeral=True)
        return

    @app_commands.command(description='View Easter eggs in the log channel')
    async def view_easter_eggs(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        log = ctx.guild.get_channel(await self.db.getLoggingChannel(ctx.guild_id, channelType.log))
        await ctx.response.send_message(f'Check <#{log.id}> for Easter Eggs', ephemeral=True)
        eggs = await self.db.fetchEasterEggs(ctx.guild_id)
        if not eggs:
            await log.send('No Easter Eggs found!')
            return
        for egg in eggs:
            msg = f"{egg['phrase']}\nLimited: {bool(egg['limited'])}"
            with io.BytesIO(egg['file']) as fp:
                fp.seek(0)
                await log.send(msg, file=File(fp, egg['filename']))
        return

    @app_commands.command(description='Set the default role color for creating class roles')
    @app_commands.describe(
        red='The red component of the color',
        green='The green component of the color',
        blue='The blue component of the color'
    )
    async def set_color(self,
                        ctx: discord.Interaction,
                        red: int,
                        green: int,
                        blue: int):
        if not await self._isEnabled(ctx):
            return
        await ctx.response.defer()
        await self.db.setRoleColor(ctx.guild_id, red, green, blue)
        with io.BytesIO() as f:
            img = Image.new('RGB', (128, 128), (red, green, blue))
            img.save(f, format='JPEG')
            f.seek(0)
            await ctx.followup.send('Role color set successfully', file=File(f, 'roleColor.jpg'), ephemeral=True)
        return

    @app_commands.command(description='See role color')
    async def role_color(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        try:
            RGB = await self.db.getRoleColor(ctx.guild_id)
        except EE_DB_Exception:
            RGB = discord.Color.dark_teal().to_rgb()
        with io.BytesIO() as f:
            img = Image.new('RGB', (128, 128), RGB)
            img.save(f, format='JPEG')
            f.seek(0)
            await ctx.response.send_message(file=File(f, 'roleColor.jpg'), ephemeral=True)
        return

    @app_commands.command(description='Clear the role color')
    async def clear_color(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        await self.db.clearRoleColor(ctx.guild_id)
        await ctx.response.send_message('Role color cleared', ephemeral=True)
        return

    @app_commands.command(description='Set class prefix for class specific channels')
    @app_commands.describe(
        prefix='Cannot exceed 5 characters'
    )
    async def set_channel_prefix(self,
                                 ctx: discord.Interaction,
                                 prefix: str):
        if not await self._isEnabled(ctx):
            return
        if not prefix:
            await ctx.response.send_message('Prefix cannot be empty')
            return
        if len(prefix) > 5:
            await ctx.response.send_message('The prefix must be at most 5 characters')
            return
        await self.db.setChannelPrefix(ctx.guild_id, prefix)
        await ctx.response.send_message('Prefix set!', ephemeral=True)
        return

    @app_commands.command(description='Resets prefix back to default prefix: EE')
    async def clear_prefix(self, ctx: discord.Interaction):
        if not await self._isEnabled(ctx):
            return
        await self.db.clearChannelPrefix(ctx.guild_id)
        await ctx.response.send_message('Prefix cleared!', ephemeral=True)
        return

    async def set_poli_rules(self,
                             ctx: discord.Interaction,
                             rules: str):
        parsedRules: List[Union[str, int]] = next(csv.reader(io.StringIO(rules), delimiter=' '))
        for i in range(0, len(parsedRules)):
            try:
                parsedRules[i] = int(parsedRules[i])
            except ValueError:
                await ctx.response.send_message('The given rules must be numeric', ephemeral=True)
                return
        await self.db.setPoliRules(ctx.guild_id, parsedRules)
        await ctx.response.send_message('Political rules set', ephemeral=True)
        return

    async def clear_poli_rules(self, ctx: discord.Interaction):
        await self.db.clearPoliRules(ctx.guild_id)
        await ctx.response.send_message('Cleared Political Rules', ephemeral=True)
        return

    async def view_poli_rules(self, ctx: discord.Interaction):
        rules = await self.db.getPoliRules(ctx.guild_id)
        response = str(rules)[1:-1]
        if not response:
            response = 'No political rules set'
        await ctx.response.send_message(response, ephemeral=True)
        return

    @app_commands.command(description='Set, clear, or view the political channel rules of the server.')
    @app_commands.describe(
        rules='The rules that gets a user banned from the political channels. Must be numeric.'
    )
    async def poli_rules(self,
                         ctx: discord.Interaction,
                         action: Literal['set', 'clear', 'view'],
                         rules: Optional[str]):
        if not await self._isEnabled(ctx):
            return
        if action == 'set':
            if rules is None or not rules:
                await ctx.response.send_message('Error: Must include a list of rules to set!', ephemeral=True)
                return
            await self.set_poli_rules(ctx, rules)
        elif action == 'clear':
            await self.clear_poli_rules(ctx)
        else:
            await self.view_poli_rules(ctx)
        return

    async def set_poli_msg(self, ctx: discord.Interaction, url: str, reaction: str):
        # Check emoji
        if not Emoji.is_emoji(reaction):
            if not re.sub(r'<:\w+(\d+)>', '', reaction):
                await ctx.response.send_message('Please use an emoji')
                return

        # Get old message, if any
        oldChannel = await self.db.getPoliChannel(ctx.guild_id)
        oldMessage = await self.db.getPoliMsg(ctx.guild_id)

        # Try removing the old reactions
        if oldChannel is not None and oldMessage is not None:
            try:
                msg = await ctx.guild.get_channel(oldChannel).fetch_message(oldMessage)
                await msg.clear_reactions()
            except HTTPException:
                pass
            except AttributeError:
                pass

        # Get channel and msg id from url
        parsedURL = url.split('/')

        # Set channel and message
        await self.db.setPoliChannel(ctx.guild_id, int(parsedURL[-2]))
        await self.db.setPoliMsg(ctx.guild_id, int(parsedURL[-1]))

        # Fetch message
        msg = await ctx.guild.get_channel(int(parsedURL[-2])).fetch_message(int(parsedURL[-1]))
        await msg.add_reaction(reaction)
        await ctx.response.send_message('Political message set!', ephemeral=True)
        return

    async def clear_poli_msg(self, ctx: discord.Interaction):
        msgID = await self.db.getPoliMsg(ctx.guild_id)
        channelID = await self.db.getPoliChannel(ctx.guild_id)
        await self.db.clearPoliChannel(ctx.guild_id)
        await self.db.clearPoliMsg(ctx.guild_id)
        try:
            msg = await ctx.guild.get_channel(channelID).fetch_message(msgID)
            await msg.clear_reactions()
        except discord.HTTPException:
            # If the message has been deleted
            pass
        except AttributeError:
            # If the message was already wiped from the database
            pass
        await ctx.response.send_message('Political rules message cleared', ephemeral=True)
        return

    @app_commands.command(description="Sets or clears the political role's opt-in/out message")
    @app_commands.describe(
        url='URL of the message',
        reaction='The emoji for opting in or out of the political role'
    )
    async def poli_msg(self,
                       ctx: discord.Interaction,
                       action: Literal['set', 'clear'],
                       url: Optional[str],
                       reaction: Optional[str]):
        if not await self._isEnabled(ctx):
            return

        if action == 'set':
            if url is None or not url:
                await ctx.response.send_message('Error: URL must be provided for this action!', ephemeral=True)
                return
            if reaction is None or not reaction:
                await ctx.response.send_message('Error: Reaction must be provided for this action!', ephemeral=True)
                return
            await self.set_poli_msg(ctx, url, reaction)
        else:
            await self.clear_poli_msg(ctx)
        return

    async def add_query(self,
                        ctx: discord.Interaction,
                        query: str,
                        description: Optional[str],
                        tex: Optional[str],
                        image: Optional[discord.Attachment],
                        url: Optional[str]):
        await ctx.response.defer()
        # Ensure entry does not have both tex and image
        if tex is not None and image is not None:
            await ctx.followup.send('Query cannot have an image and tex')
            return

        # Verify files
        if tex is not None:
            img, filename = latex_engine.render_latex(tex)
            if img is None:
                await ctx.followup.send('Could not generate image from TEX code')
                return
            img.seek(0)
            if img.getbuffer().nbytes > GL.MAX_FILE_SIZE:
                await ctx.followup.send(f'TEX code rejected. File size too large! '
                                        f'{img.getbuffer().nbytes / (1024 * 1024)}MiB > 4 MiB')
                return
            img = img.read()
        elif image is not None:
            if image.size > GL.MAX_FILE_SIZE:
                await ctx.followup.send(f'File too large! {image.size / (1024 * 1024)}MiB > 4 MiB')
                return
            img = await image.read()
            filename = image.filename
        else:
            img = None
            filename = None

        # Verify URL
        if url is not None:
            if not validators.url(url):
                await ctx.followup.send('Provided URL is invalid')
                return

        # Ensure there is at least one content item
        if description is None and url is None and img is None:
            await ctx.followup.send('Cannot create query with no material')
            return

        try:
            await self.db.addNewQuery(ctx.guild_id, query, description, filename, img, url)
            await ctx.followup.send('New query created')
        except EE_DB_Exception:
            await ctx.followup.send('Can not create duplicate query')
        return

    async def edit_query(self,
                         ctx: discord.Interaction,
                         query: str,
                         description: Optional[str],
                         tex: Optional[str],
                         image: Optional[discord.Attachment],
                         url: Optional[str],
                         edit_action: editRemovalActions):
        await ctx.response.defer()

        # Make sure there are no conflicts
        if description is not None and edit_action == editRemovalActions.DESCRIPTION:
            await ctx.followup.send('Cannot edit description and remove description')
            return
        if (tex is not None or image is not None) and editRemovalActions == editRemovalActions.IMAGE:
            await ctx.followup.send('Cannot edit image and remove image')
            return
        if url is not None and editRemovalActions == editRemovalActions.URL:
            await ctx.followup.send('Cannot edit url and remove url')
            return

        # Get old data
        try:
            oldData = await self.db.getQuery(ctx.guild_id, query)
        except EE_DB_Exception:
            await ctx.followup.send('Query not found')
            return

        # Ensure entry does not have both tex and image
        if tex is not None and image is not None:
            await ctx.followup.send('Query cannot have an image and tex')
            return

        # Verify files
        if tex is not None:
            img, filename = latex_engine.render_latex(tex)
            if img is None:
                await ctx.followup.send('Could not generate image from TEX code')
                return
            img.seek(0)
            if img.getbuffer().nbytes > GL.MAX_FILE_SIZE:
                await ctx.followup.send(f'TEX code rejected. File size too large! '
                                        f'{img.getbuffer().nbytes / (1024 * 1024)}MiB > 4 MiB')
                return
            img = img.read()
        elif image is not None:
            if image.size > GL.MAX_FILE_SIZE:
                await ctx.followup.send(f'File too large! {image.size / (1024 * 1024)}MiB > 4 MiB')
                return
            img = await image.read()
            filename = image.filename
        else:
            img = None
            filename = None

        # Verify URL
        if url is not None:
            if not validators.url(url):
                await ctx.followup.send('Provided URL is invalid')
                return

        if edit_action == editRemovalActions.IMAGE:
            oldData['filename'] = None
            oldData['image'] = None
        elif edit_action == editRemovalActions.DESCRIPTION:
            oldData['description'] = None
        elif edit_action == editRemovalActions.URL:
            oldData['url'] = None

        # Ensure there is at least one content item to edit
        if description is None and url is None and img is None and editRemovalActions != editRemovalActions.NOTHING:
            await ctx.followup.send('Cannot edit query with no material removed, edited, or added')
            return

        # Preserve old data
        if description is None:
            description = oldData['description']
        if tex is None and image is None:
            img = oldData['image']
            filename = oldData['filename']
        if url is None:
            url = oldData['url']

        try:
            await self.db.editQuery(ctx.guild_id, query, description, filename, img, url)
            await ctx.followup.send('Query edited')
        except EE_DB_Exception:
            await ctx.followup.send('Could not edit non-existent query')
        return

    async def remove_query(self,
                           ctx: discord.Interaction,
                           query: str):
        await self.db.deleteQuery(ctx.guild_id, query)
        await ctx.response.send_message('Query removed')
        return

    @app_commands.command(description='Updates server queries')
    async def update_query(self,
                           ctx: discord.Interaction,
                           action: Literal['add', 'edit', 'remove'],
                           query: str,
                           description: Optional[str],
                           tex: Optional[str],
                           image: Optional[discord.Attachment],
                           url: Optional[str],
                           edit_action: Optional[Literal['Remove image', 'Remove description', 'Remove URL']]):
        if not await self._isEnabled(ctx, False):
            return
        if action == 'add':
            await self.add_query(ctx, query, description, tex, image, url)
        elif action == 'edit':
            await self.edit_query(ctx, query, description, tex, image, url, editRemovalActions(edit_action))
        elif action == 'remove':
            await self.remove_query(ctx, query)
        return

    @app_commands.command()
    async def update_server_settings(self,
                                     ctx: discord.Interaction,
                                     use_ai: Literal['yes', 'no']):
        if not await self._isEnabled(ctx, False):
            return
        # Get the current settings and load them into modal
        channelRep = await self.db.getCreateChannelRep(ctx.guild_id)
        filterRep = await self.db.getFilterRep(ctx.guild_id)
        modal = guildSettingsModal(self.db, ctx.guild_id, channelRep, filterRep)
        await ctx.response.send_modal(modal)
        # If modal was submitted, update the AI setting
        await self.db.updateAI(ctx.guild_id, use_ai == 'yes')

    @app_commands.command()
    async def set_spam_parameters(self,
                                  ctx: discord.Interaction,
                                  messages: int,
                                  period: int):
        if not await self._isEnabled(ctx, False):
            return

        if messages <= 0:
            await ctx.response.send_message('messages parameter must be positive')
            return
        if period <= 0:
            await ctx.response.send_message('period parameter must be positive')
            return
        try:
            await self.db.setSpamProtectionParameters(ctx.guild_id, messages, period)
        except EE_DB_Exception:
            await ctx.response.send_message('Error while updating database')
            return
        await ctx.response.send_message('Spam parameters successfully updated')
        self.bot.anti_spam[ctx.guild_id] = commands.CooldownMapping.from_cooldown(messages, period,
                                                                                  commands.BucketType.member)
        return


async def setup(bot):
    await bot.add_cog(serverConfig(bot=bot))
