# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import ui
from EE_Bot_DB import EE_DB
from typing import Optional


class guildSettingsModal(ui.Modal):
    _createChannelRep = ui.TextInput(label='Level required for channel creation', max_length=3, required=False)
    _filterRep = ui.TextInput(label='Level required for turning off the filter', max_length=3,
                              required=False)
    # Uncomment when discord finally gets their crap together
    # _enableAI = ui.Select(placeholder='AI Settings', options=[
    #     discord.SelectOption(label='Use AI to generate query responses', value='True'),
    #     discord.SelectOption(label="Don't use AI to generate query responses", value='False')], disabled=True)

    def __init__(self, database: EE_DB, guild: int, channelRep: Optional[int], filterRep: Optional[int]):
        super().__init__(title='Server Settings')
        self.db = database
        self.guild = guild
        if channelRep is not None:
            self._createChannelRep.default = str(channelRep)
        if filterRep is not None:
            self._filterRep.default = str(filterRep)

    async def on_submit(self, interaction: discord.Interaction) -> None:
        # Get the user input
        filterRep = str(self._filterRep)
        channelRep = str(self._createChannelRep)
        if not filterRep:
            filterRep = None
        else:
            try:
                # Try converting user input into integer
                filterRep = int(filterRep)
                if filterRep < 0:
                    raise ValueError('Must be unsigned')
            except ValueError:
                await interaction.response.send_message('Reputation must be entered as a positive integer',
                                                        ephmeral=True)
                return
        if not channelRep:
            channelRep = None
        else:
            try:
                # Try converting user input into integer
                channelRep = int(channelRep)
                if channelRep < 0:
                    raise ValueError('Must be unsigned')
            except ValueError:
                await interaction.response.send_message('Reputation must be entered as a positive integer',
                                                        ephemeral=True)
                return
        # Everything got converted correctly. Publish updates to database
        await interaction.response.send_message('Saving settings', ephemeral=True)
        await self.db.updateFilterRep(self.guild, filterRep)
        await self.db.updateCreateChannelRep(self.guild, channelRep)
        return
