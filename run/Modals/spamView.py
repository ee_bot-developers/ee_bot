# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import discord
from typing import Optional, Callable, Awaitable


class spamView(discord.ui.View):
    def __init__(self,
                 msg: discord.Message,
                 embed: discord.Embed,
                 warn_callback: Callable[[discord.Interaction, discord.Message], Awaitable[None]],
                 ban_callback: Callable[[discord.Interaction, discord.Member], Awaitable[None]],
                 timeout: Optional[float] = 600.0):
        super().__init__(timeout=timeout)
        self.add_item(discord.ui.Button(label='Go to last message', style=discord.ButtonStyle.url, url=msg.jump_url))
        self.message = msg
        self.msg_url = msg.jump_url
        self.embed = embed
        self.spam_notification: Optional[discord.Message] = None
        self.warn_callback = warn_callback
        self.ban_callback = ban_callback
        return

    async def update_view(self):
        if self.spam_notification is not None:
            url_view = discord.ui.View()
            url_view.add_item(discord.ui.Button(label='Go to last message', style=discord.ButtonStyle.url,
                                                url=self.msg_url))
            await self.spam_notification.edit(embed=self.embed, view=url_view)

    async def on_timeout(self) -> None:
        await self.update_view()
        return

    @discord.ui.button(label='warn', style=discord.ButtonStyle.primary)
    async def warn(self, ctx: discord.Interaction, _: discord.ui.Button):
        await self.warn_callback(ctx, self.message)
        await self.update_view()
        self.stop()

    @discord.ui.button(label='ban', style=discord.ButtonStyle.danger)
    async def ban(self, ctx: discord.Interaction, _: discord.ui.Button):
        await self.ban_callback(ctx, self.message.author)
        await self.update_view()
        self.stop()

    @discord.ui.button(label='dismiss', style=discord.ButtonStyle.success)
    async def dismiss(self, ctx: discord.Interaction, _: discord.ui.Button):
        await ctx.response.send_message('Removing timeout from user', ephemeral=True)
        await self.message.author.timeout(None)
        await self.update_view()
        self.stop()

    async def terminate(self):
        await self.update_view()
        self.stop()
