# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import ui
from EE_Bot_DB import EE_DB


class BanMsgModal(ui.Modal):
    Message = ui.TextInput(label='Message',
                           style=discord.TextStyle.paragraph,
                           max_length=1000,
                           min_length=0)

    def __init__(self, database: EE_DB, guild: int, original: str):
        super().__init__(title='Set Ban Message')
        self.db = database
        self.guild = guild
        self.Message.default = original

    async def on_submit(self, interaction: discord.Interaction) -> None:
        if str(self.Message):
            await self.db.setBanMsg(self.guild, str(self.Message))
            response = 'Successfully set ban message'
        else:
            await self.db.clearBanMsg(self.guild)
            response = 'Successfully cleared ban message'
        await interaction.response.send_message(response, ephemeral=True)
        return
