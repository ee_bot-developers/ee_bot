# BSD 3-Clause License
#
# Copyright (c) 2023, Tom Schmitz
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from typing import Optional, Callable, Awaitable, Union


class reportView(discord.ui.View):
    def __init__(self,
                 msg: discord.Message,
                 embed: discord.Embed,
                 warn_callback: Callable[[discord.Interaction, discord.Message], Awaitable[None]],
                 ban_callback: Callable[[discord.Interaction, discord.Member], Awaitable[None]],
                 followup: Union[Callable[[], discord.Webhook], discord.Webhook],
                 timeout: Optional[float] = 600.0):
        # Initialize super class
        super().__init__(timeout=timeout)
        # Add go to message button
        self.add_item(discord.ui.Button(label='Go to message', style=discord.ButtonStyle.url, url=msg.jump_url))
        # Keep message around
        self.message = msg
        # Save message url since urls cannot be accessed after a message is deleted
        self.msg_url = msg.jump_url
        # Save embed created for message
        self.embed = embed
        # Report message, set after sending the message
        self.report_msg: Optional[discord.Message] = None
        # Callbacks for warn and ban operations
        self.warn_callback = warn_callback
        self.ban_callback = ban_callback
        # Followup to let the reporter know what came of their report
        self.followup = followup

    async def update_view(self):
        # Updates view in the event of a timeout or an action taken
        if self.report_msg is not None:
            url_view = discord.ui.View()
            url_view.add_item(discord.ui.Button(label='Go to Message', style=discord.ButtonStyle.url,
                                                url=self.msg_url))
            await self.report_msg.edit(embed=self.embed, view=url_view)

    async def on_timeout(self) -> None:
        # Updates view and lets the reporter know that the admins are still reviewing the report
        await self.update_view()
        await self.followup.send('The admins are still reviewing your report.', ephemeral=True)

    @discord.ui.button(label='warn', style=discord.ButtonStyle.primary)
    async def warn(self, ctx: discord.Interaction, button: discord.ui.Button):
        # Calls the warn callback. If pop-up is canceled, then this function does not continue after the warn
        # callback call. If a warning was successfully submitted, it will update the view and send a followup
        # detailing that actions were taken against the user
        await self.warn_callback(ctx, self.message)
        await self.update_view()
        await self.followup.send('The admins have reviewed your report and took action.', ephemeral=True)
        self.stop()

    @discord.ui.button(label='ban', style=discord.ButtonStyle.danger)
    async def ban(self, ctx: discord.Interaction, button: discord.ui.Button):
        # Calls the ban callback. If pop-up is canceled, then this function does not continue after the ban
        # callback call. If a ban was successfully submitted, it will update the view and send a followup
        # detailing that actions were taken against the user
        await self.ban_callback(ctx, self.message.author)
        await self.update_view()
        await self.followup.send('The admins have reviewed your report and took action.', ephemeral=True)
        self.stop()

    @discord.ui.button(label='dismiss', style=discord.ButtonStyle.success)
    async def dismiss(self, ctx: discord.Interaction, button: discord.ui.Button):
        # Dismisses the report and updates the view. Additionally, follows up with reporter to let them know that the
        # admins decided not to take action against the user
        await ctx.response.send_message('Report dismissed', ephemeral=True)
        await self.update_view()
        await self.followup.send('The admins have decided to dismiss your report.', ephemeral=True)
        self.stop()

    async def terminate(self):
        # Stops the view and updates it. This should be called when cleaning up
        await self.update_view()
        self.stop()
