# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#!/bin/sh
#!/bin/make
error() {
	echo "$@" >> /dev/stderr
}

fail() {
	echo "$@" >> /dev/stderr
	exit 1
}

log() {
	echo "$@"
}

exit_success() {
	echo "Finished setting up directory"
	exit 0
}

cd ..
log "Setting up directory"

arch=$(arch)
cd run
mkdir -p Compile
cd Compile
mkdir -p toolchains
cd toolchains

if [[ "$arch" == "x86_64" ]]; then
	git clone git@gitlab.com:ee_bot-developers/toolchains/x86_64.git || error "You do not have permissions to download the toolchains for x64_64"
elif [[ "$arch" == "aarch64" ]]; then
	git clone git@gitlab.com:ee_bot-developers/toolchains/rpi.git || error "You do not have permissions to download the toolchains for aarch64"
else
	error "No toolchains available for $(arch). Please generate some"
fi

cd ../../..

if [[ "$OSTYPE" == "darwin"* ]]; then
	brew tap Homebrew/bundle && brew bundle
	git submodule update --init --recursive
	cd run
	pip install -U -r requirements.txt
	if [ -d "kicost" ]; then
		rm -rf kicost
	fi
	mkdir kicost
	make
	cd RF
	git submodule update --init
	cd ..
	cd power
	git submodule update --init
	cd ..
	make RF
	make RF-Interface
	make power
	make power-interface
	chmod u+x ./run.sh
	cd ../..
	exit_success
fi

if ! type pip3 &. /dev/null; then
	sudo apt update -y
	if [ $? -ne 0 ]; then
		fail "Installation failed"
	fi
	sudo apt upgrade -y
	sudo apt install -y python3
	sudo apt install -y python3-pip
	sudo apt install build_essential
fi
if ! type make &> /dev/null; then
  sudo apt install -y make
fi
if ! type clang &> /dev/null; then
  sudo apt install -y clang
fi
if ! type yosys &> /dev/null; then
  sudo apt install -y yosys
fi

if type pip3 &> /dev/null; then
  if type wget &> /dev/null; then
    if type make &> /dev/null; then
      if type unzip &> /dev/null; then
        if type yosys &> /dev/null; then
          git submodule update --init --recursive
            cd run
			pip install -U -r requirements.txt
            chmod u+x ./run.sh
            if [ -d "kicost" ]; then
              rm -rf kicost
            fi
            if [ -d "images" ]; then
              git submodule update --recursive --remote
            fi
            mkdir kicost
            make
			cd RF
			git submodule update --init
			cd ..
			cd power
			git submodule update --init
			cd ..
            make compile
            make RF
            make RF-Interface
            make power
            make power-interface
            cd ..
            exit_success
          fi
        fi
      fi
    fi
  fi
