# EE Bot
This is the repository for EE_Bot, a discord bot used for [The WSU Electrical Engineering Community](https://discord.gg/JJfVg3hjpG)

## Intalling necessary packages
**Python 3.8 or higher is required**

This Bot uses a library called discord.py and is installed automatically installs it upon setup. The documentation can be found with a simple google search, or you can use the following link: [discord.py programmers reference](https://discordpy.readthedocs.io/en/latest/index.html).

### For Windows Users
You must have [Visual Studio Build Tools](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019) installed

Run the following command to see if Python is installed:
```sh
#Checks if python is installed and opens app store if it is not installed
python3 --version
```

### For macOS Users
Run the following commands:
```sh
#This will check if you have developer tools installed and install them if you don't
xcode-select --install 
brew update
brew upgrade
```
### Installation
To install the necessary libraries, run the following command(s):

```sh
#Linux/macOS
make

#Windows
cd setup
setup.bat
```
